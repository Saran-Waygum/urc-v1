//
//  AlertNotification.swift
//  URC
//
//  Created by Saran Mahadevan on 03/01/17.
//  Copyright © 2017 Waygum. All rights reserved.
//

import UIKit

class AlertNotification: Notification {

    
    var alertState: String = ""
    var alertMessage : String = ""
    var assetName : String = ""
    var alertDescription : String = ""
    var alertLevel : String = ""
    var alertId : Int = 0
    

    
    override init(){
        super.init()
    }
    
}
