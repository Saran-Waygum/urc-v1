//
//  AlertStatusUpdateWebServiceManager.swift
//  URC
//
//  Created by Saran Mahadevan on 08/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AlertStatusUpdateWebServiceManager: URCWebServiceManager
{

    var deviceRegistrationURL = "http://indiadevres4.cloudapp.net:8080/api/alertStatuses"
    var alertsArray: Array<AssetMeasurementEvent> = []
    
    internal func loadMeasurementEvents(parameters:[String:AnyObject]!, success : @escaping (_ response: Array<AssetMeasurementEvent>?)->Void, failure :@escaping (_ error : NSError?, _ statusCode: Int) -> Void){
        
        
        
        postDataToService(url: deviceRegistrationURL, parameters: parameters, headers: nil, success: { (response) -> Void in
//            for obj in response {
//                
//                var locationName: String = ""
//                let loc = obj["geoLocationInfo"]
//                if let location = loc {
//                    locationName = location["geoLocationName"] != nil ? location["geoLocationName"] as! String : "Unknown"
//                }
//                
//                self.filteredAssetGroupsArray.append(AssetGroup(
//                    name: (obj.valueForKey("assetGrpName") as? String) == nil ? "Name" : (obj.valueForKey("assetGrpName") as! String),
//                    id: obj.valueForKey("id") == nil ? 0 : (obj.valueForKey("id")?.integerValue)!,
//                    grpDescription: (obj.valueForKey("assetGrpDesc") as? String) == nil ? " " : (obj.valueForKey("assetGrpDesc") as! String),
//                    location: locationName,
//                    siteId: (obj.valueForKey("managedSiteId") as? String) == nil ? "Id" : (obj.valueForKey("managedSiteId") as! String),
//                    imageUrl:(obj.valueForKey("assetGrpDisplayImageRef") as? String) == nil ? NSURL() : NSURL(string: (obj.valueForKey("assetGrpDisplayImageRef") as! String))!))
//                
//            }
            success (self.alertsArray)
            
        }, failure: {
            (error,code,failureReason) in
//            failure(error: error,statusCode: code,failureReason: failureReason)
        })
        
    }
    
    

    
}
