//
//  AlertsDetailCollectionViewCell.swift
//  URC
//
//  Created by Saran Mahadevan on 08/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AlertsDetailCollectionViewCell: UICollectionViewCell {
    
    
    
    let isAssetGroup: Bool = true
    
    @IBOutlet weak var assetNameLabel: UILabel!
    @IBOutlet weak var assetDesciptionLabel: UILabel!
    @IBOutlet weak var measurementsLabel: UILabel!
    @IBOutlet weak var measuredValuesLabel: UILabel!
    @IBOutlet weak var assetImageView: UIImageView!
    
    
    
    func configureAlert(asset: NSObject, isAssetGroup: Bool, alert: Alert)
    {
        assetImageView.layer.cornerRadius = 15.0
        assetNameLabel.font = UIFont(name: Styles.Style.boldStyleCustomFont, size: 13.0)
        assetDesciptionLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        measurementsLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        measuredValuesLabel.font = UIFont(name: Styles.Style.boldStyleCustomFont, size: 12.0)
        
        if isAssetGroup
        {
            let assetGroup = asset as! AssetGroup
            assetNameLabel.text = assetGroup.name
            assetDesciptionLabel.text = assetGroup.grpDescription
            measuredValuesLabel.isHidden = true
            measurementsLabel.isHidden = true
        }
        else
        {
            let asset = asset as! Asset
            assetNameLabel.text = asset.name
            assetDesciptionLabel.text = asset.deviceDesc
            measuredValuesLabel.isHidden = false
            measurementsLabel.isHidden = false
        }
        
        if alert.alertLevel == "warning"
        {
            measuredValuesLabel.textColor = Styles.Style.warningAlertColor
        }
        else
        {
            measuredValuesLabel.textColor = Styles.Style.criticalAlertColor
        }
        
    }
}
