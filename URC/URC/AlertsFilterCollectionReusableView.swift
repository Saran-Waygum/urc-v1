//
//  AlertsFilterCollectionReusableView.swift
//  URC
//
//  Created by Saran Mahadevan on 28/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AlertsFilterCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var filterOptionsButton: UIButton!
    
    @IBOutlet weak var sectionSeparatorViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var filterOptionsExpandImage: UIImageView!
}
