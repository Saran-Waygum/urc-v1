//
//  AlertsFilterCollectionViewCell.swift
//  URC
//
//  Created by Saran Mahadevan on 28/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AlertsFilterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var filterCriteriaNameLabel: UILabel!
    @IBOutlet weak var removeFilterCriteriaButton: UIButton!
    
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var filterCriteriaImageView: UIImageView!
    
    @IBOutlet weak var checkBoxWidth: NSLayoutConstraint!
    @IBOutlet weak var removeFilterCriteriaButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var filterCriteriaImageViewWidth: NSLayoutConstraint!
    
    var tapAction: ((UICollectionViewCell) -> Void)?
    
    @IBAction func removeFilterCriteriaButtonPressed(_ sender: UIButton) {
        tapAction!(self)
    }
}



/*
 
 
 
 
 @IBOutlet weak var checkBoxButton: UIButton!
 @IBOutlet weak var checkBoxWidth: NSLayoutConstraint!
 
 @IBOutlet weak var filterCriteriaImageView: UIImageView!
 @IBOutlet weak var filterCriteriaImageViewWidth: NSLayoutConstraint!
 

 */
