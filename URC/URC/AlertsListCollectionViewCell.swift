//
//  AlertsListCollectionViewCell.swift
//  URC
//
//  Created by Saran Mahadevan on 08/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AlertsListCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var alertNameLabel: UILabel!
    @IBOutlet weak var alertStatusLabel: UILabel!
    @IBOutlet weak var alertResponseLabel: UILabel!
    @IBOutlet weak var alertImageView: UIImageView!
    @IBOutlet weak var alertResponseImageView: UIImageView!
    @IBOutlet weak var alertNameLabelHeightConstraint: NSLayoutConstraint!
    
    
    func configureAlert(alert: Alert)
    {
        alertNameLabel.text = alert.alertRule.alertRuleName
        
        alertNameLabel.font = UIFont(name: Styles.Style.boldStyleCustomFont, size: 13.0)
        alertStatusLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        alertResponseLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        
        
        if alert.state == "ACKNOWLEDGED"
        {
            alertResponseLabel.text = "Acknowledged " + alert.dateUpdated
            alertStatusLabel.text = "In Progress"
        }
        else if alert.state == "GENERATED"
        {
            alertResponseLabel.text = "Generated " + alert.dateUpdated
            alertStatusLabel.text = "Waiting for action"
        }
        

        if alert.alertLevel == "critical"
        {
            alertImageView.image = UIImage.init(named: "critical")
        }
        else
        {
            alertImageView.image = UIImage.init(named: "warning")
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let screenWidth = appDelegate.window?.frame.size.width
        alertNameLabelHeightConstraint.constant = self.heightForLabel(constraintedWidth: screenWidth! - 100, font: alertNameLabel.font, text: alert.alertRule.alertRuleName)
        
        self.layoutSubviews()


        
//        if alert.alertLevel == "critical"
//        {
//            alertImageView.image = UIImage.init(named: "critical")
//        }
//        else if alert.alertLevel == "warning"
//        {
//            alertImageView.image = UIImage.init(named: "warning")
//        }
//        else
//        {
//            alertImageView.image = UIImage.init(named: "maintenance")
//        }
        
    }
    
    
    class func heightForItem(alert: Alert) -> CGFloat
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let screenWidth = appDelegate.window?.frame.size.width
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: screenWidth! - 90, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = alert.alertRule.alertRuleName
        label.sizeToFit()
        return 70 + label.frame.height
    }
    
    func heightForLabel(constraintedWidth width: CGFloat, font: UIFont, text: String) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
}
