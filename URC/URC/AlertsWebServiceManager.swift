//
//  AlertsWebServiceManager.swift
//  URC
//
//  Created by Saran Mahadevan on 07/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AlertsWebServiceManager: URCWebServiceManager {
    
    var alertServiceURL = Constants.getAlertsUrl()
    var alertsArray: Array<Alert> = []
    
    
    

    internal func loadAlerts(parameters:[String:AnyObject]!, success : @escaping (_ response: Array<Alert>?)->Void, failure :@escaping (_ error : NSError?, _ statusCode: Int) -> Void){
        

        
        
        if let status = parameters["alertStatus"] as? String
        {
            alertServiceURL = alertServiceURL + "?" + status
        }
        else
        {
            alertServiceURL = alertServiceURL + "?alertStatus=ACKNOWLEDGED,GENERATED"
        }

        
        if let alertLevel = parameters["alertLevel"] as? String
        {
            alertServiceURL = alertServiceURL + "&" + alertLevel
        }
        
        
        
        if parameters["duration"] != nil
        {
            let durationSelected = parameters["duration"] as! String
            alertServiceURL = alertServiceURL + "&secondsPrior=\(durationSelected)"
        }
        
        if let assets = parameters["assetGroupId"] as? String
        {
            alertServiceURL = alertServiceURL + "&" + assets
        }
        else
        {
            if let devices = parameters["deviceId"] as? String
            {
                alertServiceURL = alertServiceURL + "&" + devices
            }
        }
        
        
        
        
        
        

        
        loadDataFromService(url: alertServiceURL, parameters: nil , headers: nil , success: {
            (response,resHeaders) in
            
            for alertData in response{
                
                let newAlert = Alert(alertId:alertData["id"] as! Int)
                newAlert.alertUUId = alertData["alertUUId"] as! String
                newAlert.alertLevel = alertData["alertLevel"] as! String
                newAlert.alertExternalId = alertData["alertExternalId"] as! String
                newAlert.alertExtraInfo = alertData["alertExtraInfo"] != nil ? alertData["alertExtraInfo"] as! String : ""
                newAlert.alertValue = alertData["alertValue"] as! String
                newAlert.state = alertData["state"] as! String
//                newAlert.dateGenerated = NSDate.Date.formatterISO8601.dateFromString(alertData["dateUpdated"] as! String)!
                newAlert.organizationId = alertData["organizationId"] as! Int
//                newAlert.dateUpdated = NSDate.Date.formatterISO8601.dateFromString(alertData["dateUpdated"] as! String)!
                
                
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX"
                
                let dateUpdatedString = dateFormatter.date(from: alertData["dateUpdated"] as! String)!
                let dateGeneratedString = dateFormatter.date(from: alertData["dateGenerated"] as! String)!
                
                dateFormatter.dateFormat = "hh:mm a"
                dateFormatter.amSymbol = "AM"
                dateFormatter.pmSymbol = "PM"
                
                newAlert.generatedDate = dateGeneratedString as NSDate
                newAlert.updatedDate = dateUpdatedString as NSDate
            
                newAlert.dateGenerated = dateFormatter.string(from: dateGeneratedString)
                newAlert.dateUpdated = dateFormatter.string(from: dateUpdatedString)
                
                
                
                let alertRulesData =  alertData["alertRule"] as! Dictionary<String, Any>
                let newAlertRule = AlertRule()
                newAlertRule.alertRuleDesc = alertRulesData["alertRuleDesc"] as! String
                newAlertRule.alertRuleName = alertRulesData["alertRuleName"] as! String
                newAlert.alertRule = newAlertRule
                
                
                if let managedDeviceData = alertData["managedDevice"] as? NSDictionary
                {
                    let managedDevice = Asset()
                    managedDevice.id = managedDeviceData["id"] as! Int
                    
                    if let deviceUUId = managedDeviceData["deviceUUId"] as? String
                    {
                        managedDevice.assetUUId = deviceUUId
                    }
                    
                    managedDevice.name = managedDeviceData["deviceName"] as! String
                    let deviceData = managedDeviceData["deviceSpecification"] as! Dictionary<String, Any>
                    managedDevice.deviceDisplayImageRef = deviceData["deviceDisplayImageRef"] as! String
                    
                    if let deviceDesc = managedDeviceData["deviceDesc"] as? String
                    {
                        managedDevice.deviceDesc = deviceDesc
                    }
                    
                    newAlert.managedDevice = managedDevice
                }
                
                
                
                if let managedAssetGroupData = alertData["managedAssetGroup"] as? NSDictionary
                {
                    let managedAssetGroup = AssetGroup()
                    managedAssetGroup.name = managedAssetGroupData["assetGrpName"] as! String
                    managedAssetGroup.grpDescription = managedAssetGroupData["assetGrpDesc"] as! String
                    
                    
                    
                    if let locationData = managedAssetGroupData["geoLocationInfo"] as? NSDictionary
                    {
                        let geoLocationInfo = GeoLocationInfo.init(geoLocationId: locationData["id"] as! Int)
                        geoLocationInfo.geoLocationName = locationData["geoLocationName"] as! String
                        managedAssetGroup.geoLocationInfo = geoLocationInfo
                    }
                    
                    
                    newAlert.managedAssetGroup = managedAssetGroup
                }
                
                
                self.alertsArray.append(newAlert)
                
            }
            success (self.alertsArray)
            
        }, failure: { (error,code) in
            failure(error, code)
        })
        
    }
    
    
    
    internal func silenceAlertNotification(parameters:[String:String]!, success : @escaping (_ response: Bool?)->Void, failure :@escaping (_ error : NSError?, _ statusCode: Int) -> Void){
        
        
        if parameters["alertId"] != nil
        {
            alertServiceURL = alertServiceURL + "/silence/" + parameters["alertId"]!
        }
        
        
        postDataToService(url: alertServiceURL, parameters: nil, headers: nil, success:
            { (response) -> Void in
                for obj in response {
                    
                    print(obj)
                    
                }
                success (true)
                
                
        }, failure: {
            (error,code,failureReason) in
            print("failed")
        })
        
    }
    
    
    // http://indiadevmisc.cloudapp.net:8080/api/alerts/10798
    
    internal func loadAlertData(parameters:[String:String]!, success : @escaping (_ response: Alert?)->Void, failure :@escaping (_ error : NSError?, _ statusCode: Int) -> Void){
        
        
        if parameters["alertId"] != nil
        {
            alertServiceURL = alertServiceURL + "/" + parameters["alertId"]!
        }
        
        
        loadDataFromService(url: alertServiceURL, parameters: nil , headers: nil , success: {
            (response,resHeaders) in
            
            for alertData in response{
                
                let newAlert = Alert(alertId:alertData["id"] as! Int)
                newAlert.alertUUId = alertData["alertUUId"] as! String
                newAlert.alertLevel = alertData["alertLevel"] as! String
                newAlert.alertExternalId = alertData["alertExternalId"] as! String
                newAlert.alertExtraInfo = alertData["alertExtraInfo"] != nil ? alertData["alertExtraInfo"] as! String : ""
                newAlert.alertValue = alertData["alertValue"] as! String
                newAlert.state = alertData["state"] as! String
                newAlert.organizationId = alertData["organizationId"] as! Int
                
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX"
                
                let dateUpdatedString = dateFormatter.date(from: alertData["dateUpdated"] as! String)!
                let dateGeneratedString = dateFormatter.date(from: alertData["dateGenerated"] as! String)!
                
                dateFormatter.dateFormat = "hh:mm a"
                dateFormatter.amSymbol = "AM"
                dateFormatter.pmSymbol = "PM"
                
                newAlert.generatedDate = dateGeneratedString as NSDate
                newAlert.updatedDate = dateUpdatedString as NSDate
                
                newAlert.dateGenerated = dateFormatter.string(from: dateGeneratedString)
                newAlert.dateUpdated = dateFormatter.string(from: dateUpdatedString)
                
                
                
                let alertRulesData =  alertData["alertRule"] as! Dictionary<String, Any>
                let newAlertRule = AlertRule()
                newAlertRule.alertRuleDesc = alertRulesData["alertRuleDesc"] as! String
                newAlertRule.alertRuleName = alertRulesData["alertRuleName"] as! String
                newAlert.alertRule = newAlertRule
                
                
                if let managedDeviceData = alertData["managedDevice"] as? NSDictionary
                {
                    let managedDevice = Asset()
                    managedDevice.id = managedDeviceData["id"] as! Int
                    
                    if let deviceUUId = managedDeviceData["deviceUUId"] as? String
                    {
                        managedDevice.assetUUId = deviceUUId
                    }
                    
                    managedDevice.name = managedDeviceData["deviceName"] as! String
                    let deviceData = managedDeviceData["deviceSpecification"] as! Dictionary<String, Any>
                    managedDevice.deviceDisplayImageRef = deviceData["deviceDisplayImageRef"] as! String
                    
                    if let deviceDesc = managedDeviceData["deviceDesc"] as? String
                    {
                        managedDevice.deviceDesc = deviceDesc
                    }
                    
                    newAlert.managedDevice = managedDevice
                }
                
                
                
                if let managedAssetGroupData = alertData["managedAssetGroup"] as? NSDictionary
                {
                    let managedAssetGroup = AssetGroup()
                    managedAssetGroup.name = managedAssetGroupData["assetGrpName"] as! String
                    managedAssetGroup.grpDescription = managedAssetGroupData["assetGrpDesc"] as! String
                    
                    
                    
                    if let locationData = managedAssetGroupData["geoLocationInfo"] as? NSDictionary
                    {
                        let geoLocationInfo = GeoLocationInfo.init(geoLocationId: locationData["id"] as! Int)
                        geoLocationInfo.geoLocationName = locationData["geoLocationName"] as! String
                        managedAssetGroup.geoLocationInfo = geoLocationInfo
                    }
                    
                    
                    newAlert.managedAssetGroup = managedAssetGroup
                }
                
                
                self.alertsArray.append(newAlert)
                
            }
            success (self.alertsArray[0])
            
        }, failure: { (error,code) in
            failure(error, code)
        })
        
    }
    
}
