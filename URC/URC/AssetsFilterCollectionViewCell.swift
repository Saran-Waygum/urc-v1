//
//  AssetsFilterCollectionViewCell.swift
//  URC
//
//  Created by Saran Mahadevan on 28/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AssetsFilterCollectionViewCell: UICollectionViewCell{
    
    @IBOutlet weak var filterCriteriaNameLabel: UILabel!
    @IBOutlet weak var removeFilterCriteriaButton: UIButton!
    @IBOutlet weak var removeFilterCriteriaButtonWidth: NSLayoutConstraint!
    
    var tapAction: ((UICollectionViewCell) -> Void)?
    
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var checkBoxWidth: NSLayoutConstraint!
    
    @IBOutlet weak var filterCriteriaImageView: UIImageView!
    @IBOutlet weak var filterCriteriaImageViewWidth: NSLayoutConstraint!
    
    
    
    @IBAction func removeFilterCriteriaButtonPressed(_ sender: UIButton){
        tapAction!(self)
    }
    
}
