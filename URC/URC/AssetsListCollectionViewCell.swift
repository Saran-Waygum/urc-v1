//
//  AssetsListCollectionViewCell.swift
//  URC
//
//  Created by Saran Mahadevan on 24/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AssetsListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var assetImageView: UIImageView!
    @IBOutlet weak var assetNameLabel: UILabel!
    @IBOutlet weak var assetDescriptionLabel: UILabel!
    @IBOutlet weak var assetLocationLabel: UILabel!
    
    @IBOutlet weak var criticalAlertsCountLabel: UILabel!
    @IBOutlet weak var warningAlertsCountLabel: UILabel!
    @IBOutlet weak var resolvedAlertsCountLabel: UILabel!
    
    @IBOutlet weak var assetNameLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var assetDescriptionLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var assetLocationLabelHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var resolvedAlertsCountLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var warningCountLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var criticalAlertsCountLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var alertCountContainerWidthConstraint: NSLayoutConstraint!
    
}
