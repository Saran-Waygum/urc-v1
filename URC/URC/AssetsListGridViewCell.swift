//
//  AssetsListGridViewCell.swift
//  URC
//
//  Created by Saran Mahadevan on 25/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AssetsListGridViewCell: UICollectionViewCell {
    
    @IBOutlet weak var assetGroupImageView: UIImageView!
    @IBOutlet weak var assetGroupNameLabel: UILabel!
    
    @IBOutlet weak var alertCountContainerWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var criticalAlertsCountLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var warningCountLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var maintanenceAlertsCountLabelWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var criticalAlertCountLabel: UILabel!
    @IBOutlet weak var warningCountLabel: UILabel!
    @IBOutlet weak var maintanenceAlertCountLabel: UILabel!
    
    
}
