//
//  AssetsListTreeViewCell.swift
//  URC
//
//  Created by Saran Mahadevan on 29/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AssetsListTreeViewCell: UICollectionViewCell{
    
    @IBOutlet weak var assetTreeImageView: UIImageView!
    @IBOutlet weak var assetTreeNameLabel: UILabel!
    @IBOutlet weak var assetImageLeadingSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var assetImageWidthConstraint: NSLayoutConstraint!
    
    func updateAssetCell(asset: Any)
    {
        
        assetTreeImageView.layer.cornerRadius = 15
        assetTreeNameLabel.font = UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 14.0)
        
        if asset is Organization
        {
            let org = asset as! Organization
            
            assetTreeNameLabel.text = org.orgName
            assetTreeImageView.image = nil
            assetImageWidthConstraint.constant = 0
            
            if org.parentOrganisation == nil
            {
                assetImageLeadingSpaceConstraint.constant = 0
            }
            else
            {
                assetImageLeadingSpaceConstraint.constant = 10
            }
            
        }
        else if asset is Site
        {
            let site = asset as! Site
            
            assetTreeNameLabel.text = site.siteName
            assetTreeImageView.image = nil
            assetImageWidthConstraint.constant = 0
            assetImageLeadingSpaceConstraint.constant = 20
        }
        else if asset is AssetGroup
        {
            let asset = asset as! AssetGroup
            
            assetTreeNameLabel.text = asset.name
            
            if let data = NSData(contentsOf: asset.imageUrl as! URL) {
                assetTreeImageView.image = UIImage(data: data as Data)
            }
            assetImageWidthConstraint.constant = 30
            assetImageLeadingSpaceConstraint.constant = 30
            
        }
        else if asset is Asset
        {
            let asset = asset as! Asset
            
            assetTreeNameLabel.text = asset.name
            
            if asset.deviceDisplayImageRef != ""
            {
                if let data = NSData(contentsOf: URL.init(string: asset.deviceDisplayImageRef)!) {
                    assetTreeImageView.image = UIImage(data: data as Data)
                }
                else
                {
                    assetTreeImageView.image = nil
                }
            }
            else
            {
                assetTreeImageView.image = nil
            }
            
            assetImageWidthConstraint.constant = 30
            assetImageLeadingSpaceConstraint.constant = 40
            
        }
        
        
    }
    
}
