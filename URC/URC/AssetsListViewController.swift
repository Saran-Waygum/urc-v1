//
//  AssetsListViewController.swift
//  URC
//
//  Created by Saran Mahadevan on 24/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit
import QuartzCore
import MapKit

class AssetsListViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegateFlowLayout, MKMapViewDelegate {
    
    var assetGroupsArray: Array<AssetGroup>=[]
    var tempAssetGroupsArray: Array<AssetGroup>=[]
    
    var organizationsArray : Array<Organization> = []
    var treeArray : Array<Any> = []
    var expandedObjectsArray : Array<Any> = []
    
    var hierarchyArray : Array<Int> = []
    
    var treeComparisonArray : Array<String> = ["JSW","JSW_Karnataka","SMELTING FURNACE #3"]
    
    
    let regionRadius: CLLocationDistance = 1000
    
    @IBOutlet weak var assetsCollectionView: UICollectionView!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var swapViewButton: UIButton!
    @IBOutlet weak var assetsSearchBar: UISearchBar!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var filterView: UIScrollView!
    @IBOutlet weak var filterViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var filterCollectionView: UICollectionView!
    @IBOutlet weak var searchbarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var layoutOptionsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var filterOptionsContainerView: UIView!
    @IBOutlet weak var filterOptionsContainerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var alphaLayer: UIView!
    @IBOutlet weak var filterOptionsTableView: UITableView!
    @IBOutlet weak var filterCriteriaSelectedLabel: UILabel!
    @IBOutlet weak var filterViewDoneButton: UIButton!
    @IBOutlet weak var filterOptionsView: UIView!
    @IBOutlet weak var filterOptionsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var severityLabel: UILabel!
    @IBOutlet weak var alertCountContainerWidthConstraint: NSLayoutConstraint!
    
    var layoutSelected : String = "List"
    var layoutTypes : Array<String> = ["List","Grid","Tree","Map"]
    var layoutTypeImages : Array<String> = ["ic_list_view_active", "ic_tile_view_active", "ic_tree_view_active", "ic_map_view_active"]
    
    @IBOutlet weak var listLayoutButton: UIButton!
    @IBOutlet weak var gridLayoutButton: UIButton!
    @IBOutlet weak var treeLayoutButton: UIButton!
    @IBOutlet weak var mapLayoutButton: UIButton!
    
    
    var selectedFilterCriteriaTag: Int = 0
    var isMyAssetsSelected : Bool = false
    var isNotMyAssetsSelected : Bool = false
    var isWithoutAlertsSelected : Bool = false
    var isWithAlertsSelected : Bool = false
    
    var searchTextEntered: String = ""
    
    let filterTypes : Array<String> = ["LOCATION","SITE"]
    let filterTypeActiveImages : Array<String> = ["ic_filter_location_active","ic_filter_site_active"]
    let filterTypeInActiveImages : Array<String> = ["ic_filter_location","ic_filter_site"]
    
    var locationsArray : Array<Location> = []
    var sitesArray : Array<Site> = []
    var typesArray : Array<String> = ["Critical", "Warning", "Maintanance"]
    var typeImagesArray : Array<String> = ["critical", "warning", "maintenance"]
    let array : Array<String> = ["With Alerts", "Without Alerts", "My Assests","Not My Assests"]
    
    
    
    var selectedLocationsArray : Array<String> = []
    var selectedSitedArray : Array<Int> = []
    var selectedTypes : Array<String> = []
    
    var isFiltered: Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Assets"
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationController!.navigationBar.barTintColor = Styles.Style.appNavigationBarColor
        self.navigationController!.navigationBar.tintColor = Styles.Style.appNavigationContentColor
        
        self.navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(AssetsListViewController.searchAssets)), animated: true)
        
        
        setNavigationBarStyle()
        updateUI()
        
        alphaLayer.backgroundColor = UIColor.clear.withAlphaComponent(0.8)
        assetsSearchBar.delegate = self
        
    }
    
    
    
    func searchAssets()
    {
        
        if searchbarHeightConstraint.constant == 0.0
        {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                
                self.searchbarHeightConstraint.constant = 44.0
                
            }, completion: { (finished: Bool) -> Void in
                self.view.layoutSubviews()
            })
        }
        else
        {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                
                self.searchbarHeightConstraint.constant = 0.0
                
            }, completion: { (finished: Bool) -> Void in
                self.view.layoutSubviews()
            })
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadDataFromServer()
        //        loadDataFromJSON()
        
        
    }
    
    
    func updateUI()
    {
        
        //        myAssetsLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        //        notMyAssetsLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        //        withoutAlertsLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        //        withAlertsLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        //
        //
        //        locationFilterButton.titleLabel?.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        //        siteFilterButton.titleLabel?.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        //        typeFilterButton.titleLabel?.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        
        severityLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 15.0)
        severityLabel.textColor = UIColor.black
        
        listLayoutButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0)
        gridLayoutButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0)
        treeLayoutButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0)
        mapLayoutButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0)
        
    }
    
    func setNavigationBarStyle(){
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationController!.navigationBar.barTintColor = Styles.Style.appNavigationBarColor
        self.navigationController!.navigationBar.tintColor = Styles.Style.appNavigationContentColor
        
        
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 16.0)!]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        
        
        
    }
    
    
    func loadDataFromJSON()
    {
        let url = Bundle.main.url(forResource: "Assetgroups", withExtension: "json")
        let data = NSData(contentsOf: url!)
        
        do {
            let object = try JSONSerialization.jsonObject(with: data! as Data, options: .allowFragments)
            if let dictionary = object as? Array<Dictionary<String, AnyObject>> {
                
                var locationName: String = ""
                for obj in dictionary{
                    let loc = obj["geoLocationInfo"]
                    let geoLocation = GeoLocationInfo.init()
                    if let location = loc as? Dictionary<String, AnyObject> {
                        locationName = location["geoLocationName"] != nil ? location["geoLocationName"] as! String : "Unknown"
                        
                        
                        geoLocation.geoLocationCenterLongitude = location["geoLocationCenterLongitude"] as! Double
                        geoLocation.geoLocationCenterLatitude = location["geoLocationCenterLatitude"] as! Double
                    }
                    
                    
                    var imageUrl: String = ""
                    if (obj["assetGrpDisplayImageRef"] as? String) != nil
                    {
                        imageUrl = obj["assetGrpDisplayImageRef"] as! String
                        imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    }
                    
                    let assetGroupCreated = AssetGroup(
                        name: (obj["assetGrpName"] as? String) == nil ? "Name" : (obj["assetGrpName"] as! String),
                        id: obj["id"] == nil ? 0 : ((obj["id"] as AnyObject).integerValue)!,
                        grpDescription: (obj["assetGrpDesc"] as? String) == nil ? "" : (obj["assetGrpDesc"] as! String),
                        location: locationName,
                        siteId: (obj["managedSiteId"] as? Int) == nil ? 0 : (obj["managedSiteId"] as! Int),
                        imageUrl:(obj["assetGrpDisplayImageRef"] as? String) == nil ? NSURL() : NSURL(string: imageUrl)!,
                        criticalAlertsCount: (obj["criticalAlertCount"] as? Int) == 0 ? 0 : (obj["criticalAlertCount"] as! Int),
                        warningsCount: (obj["warningAlertCount"] as? Int) == 0 ? 0 : (obj["warningAlertCount"] as! Int),
                        resolvedAlertsCount: (obj["resolvedAlertCount"] as? Int) == 0 ? 0 : (obj["resolvedAlertCount"] as! Int)
                    )
                    assetGroupCreated.geoLocationInfo = geoLocation
                    
                    self.assetGroupsArray.append(assetGroupCreated)
                    
                }
                self.assetsCollectionView.reloadData()
                
            }
        } catch {
            // Handle Error
        }
        
    }
    
    
    func loadDataFromServer()
    {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        let webAction = AssetGroupWebService()
        webAction.loadAssetGroups(success: {
            (response) in
            if let response = response{
                
                self.assetGroupsArray = response
                self.tempAssetGroupsArray = response
                self.assetsCollectionView.reloadData()
                
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                self.loadFilterParametersFromServer()
                
            }
        }, failure:
            {
                (error,statusCode) in
                if(statusCode != 0){
                    
                }
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Collection View DataSource
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        
        if collectionView == filterCollectionView{
            if isWithoutAlertsSelected
            {
                return 3
            }
            else
            {
                return 4
            }
            
        }
        else
        {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == filterCollectionView
        {
            if section == 0
            {
                return selectedLocationsArray.count
            }
            else if section == 1
            {
                return selectedSitedArray.count
            }
            else if section == 3
            {
                return typesArray.count
            }
            else
            {
                return 4
            }
            
        }
        else
        {
            if layoutSelected == "Tree"
            {
                return treeArray.count
            }
            return self.assetGroupsArray.count
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if collectionView == filterCollectionView
        {
            let filterCell : AssetsFilterCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AssetsFilterCollectionViewCellID", for: indexPath as IndexPath) as! AssetsFilterCollectionViewCell
            
            if indexPath.section == 0
            {
                filterCell.filterCriteriaNameLabel.text = locationsArray[indexPath.row].name
                
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.checkBoxWidth.constant = 0
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.removeFilterCriteriaButtonWidth.constant = 15
                filterCell.filterCriteriaNameLabel.textColor = UIColor.black
            }
            else if indexPath.section == 1
            {
                filterCell.filterCriteriaNameLabel.text = sitesArray[indexPath.row].siteName
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.checkBoxWidth.constant = 0
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.removeFilterCriteriaButtonWidth.constant = 15
                filterCell.filterCriteriaNameLabel.textColor = UIColor.black
            }
            else if indexPath.section == 3
            {
                filterCell.filterCriteriaNameLabel.text = typesArray[indexPath.row]
                filterCell.filterCriteriaImageView.image = UIImage.init(named: typeImagesArray[indexPath.row])
                filterCell.filterCriteriaImageViewWidth.constant = 25
                filterCell.checkBoxWidth.constant = 25
                filterCell.removeFilterCriteriaButtonWidth.constant = 0
                
                if selectedTypes.contains(typesArray[indexPath.row])
                {
                    filterCell.checkBoxButton.setImage(UIImage.init(named: "checkbox_square_active"), for: .normal)
                    filterCell.filterCriteriaNameLabel.textColor = UIColor.black
                }
                else
                {
                    filterCell.checkBoxButton.setImage(UIImage.init(named: "checkbox_square_unactive"), for: .normal)
                    filterCell.filterCriteriaNameLabel.textColor = UIColor.gray
                }
                
            }
            else
            {
                filterCell.filterCriteriaNameLabel.text = array[indexPath.row]
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.checkBoxWidth.constant = 25
                filterCell.removeFilterCriteriaButtonWidth.constant = 0
                
                switch indexPath.row {
                case 0:
                    if isWithAlertsSelected
                    {
                        filterCell.checkBoxButton.setImage(UIImage.init(named: "checkbox_square_active"), for: .normal)
                        filterCell.filterCriteriaNameLabel.textColor = UIColor.black
                    }
                    else
                    {
                        filterCell.checkBoxButton.setImage(UIImage.init(named: "checkbox_square_unactive"), for: .normal)
                        filterCell.filterCriteriaNameLabel.textColor = UIColor.gray
                    }
                    
                case 1:
                    if isWithoutAlertsSelected
                    {
                        filterCell.checkBoxButton.setImage(UIImage.init(named: "checkbox_square_active"), for: .normal)
                        filterCell.filterCriteriaNameLabel.textColor = UIColor.black
                    }
                    else
                    {
                        filterCell.checkBoxButton.setImage(UIImage.init(named: "checkbox_square_unactive"), for: .normal)
                        filterCell.filterCriteriaNameLabel.textColor = UIColor.lightGray
                    }
                    
                case 2:
                    if isMyAssetsSelected
                    {
                        filterCell.checkBoxButton.setImage(UIImage.init(named: "checkbox_square_active"), for: .normal)
                        filterCell.filterCriteriaNameLabel.textColor = UIColor.black
                    }
                    else
                    {
                        filterCell.checkBoxButton.setImage(UIImage.init(named: "checkbox_square_unactive"), for: .normal)
                        filterCell.filterCriteriaNameLabel.textColor = UIColor.lightGray
                    }
                    
                case 3:
                    if isNotMyAssetsSelected
                    {
                        filterCell.checkBoxButton.setImage(UIImage.init(named: "checkbox_square_active"), for: .normal)
                        filterCell.filterCriteriaNameLabel.textColor = UIColor.black
                    }
                    else
                    {
                        filterCell.checkBoxButton.setImage(UIImage.init(named: "checkbox_square_unactive"), for: .normal)
                        filterCell.filterCriteriaNameLabel.textColor = UIColor.lightGray
                    }
                    
                default:
                    print("")
                }
                
            }
            
            filterCell.tapAction = { (cell) in
                self.removeFilterCondition(indexPath: collectionView.indexPath(for: cell)!)
            }
            filterCell.layoutSubviews()
            
            filterCell.filterCriteriaNameLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)
            return filterCell
        }
        else
        {
            switch layoutSelected {
            case "Grid":
                let assetCell : AssetsListGridViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AssetsListGridViewCellID", for: indexPath as IndexPath) as! AssetsListGridViewCell
                
                let assetGroup = assetGroupsArray[indexPath.row]
                
                assetCell.assetGroupNameLabel.text = assetGroup.name
                assetCell.assetGroupImageView.layer.cornerRadius = 40
                assetCell.assetGroupNameLabel.font = UIFont(name: Styles.Style.boldStyleCustomFont, size: 14.0)
                
                assetCell.criticalAlertCountLabel.layer.cornerRadius = 10
                assetCell.warningCountLabel.layer.cornerRadius = 10
                assetCell.maintanenceAlertCountLabel.layer.cornerRadius = 10
                
                
                
                
                DispatchQueue.global(qos: .userInitiated).async {
                    if let data = NSData(contentsOf: assetGroup.imageUrl as! URL) {
                        
                        DispatchQueue.main.async {
                            assetCell.assetGroupImageView.image = UIImage(data: data as Data)
                        }
                    }
                }
                
                
                assetCell.criticalAlertCountLabel.text = "\(assetGroup.criticalAlertsCount)"
                assetCell.warningCountLabel.text = "\(assetGroup.warningsCount)"
                assetCell.maintanenceAlertCountLabel.text = "\(assetGroup.maintenanceAlertsCount)"
                
                
                
                if assetGroup.criticalAlertsCount != 0
                {
                    assetCell.criticalAlertCountLabel.text = String.init(format: "%d", (assetGroup.criticalAlertsCount))
                    assetCell.criticalAlertsCountLabelWidthConstraint.constant = 20.0
                }
                else
                {
                    assetCell.criticalAlertsCountLabelWidthConstraint.constant = 0.0
                }
                
                
                if assetGroup.warningsCount != 0
                {
                    assetCell.warningCountLabel.text = String.init(format: "%d", (assetGroup.warningsCount))
                    assetCell.warningCountLabelWidthConstraint.constant = 20.0
                }
                else
                {
                    assetCell.warningCountLabelWidthConstraint.constant = 0.0
                }
                
                if assetGroup.maintenanceAlertsCount != 0
                {
                    assetCell.maintanenceAlertCountLabel.text = String.init(format: "%d", (assetGroup.maintenanceAlertsCount))
                    assetCell.maintanenceAlertsCountLabelWidthConstraint.constant = 20.0
                }
                else
                {
                    assetCell.maintanenceAlertsCountLabelWidthConstraint.constant = 0.0
                }
                
                
                let totalWidth = assetCell.maintanenceAlertsCountLabelWidthConstraint.constant + assetCell.criticalAlertsCountLabelWidthConstraint.constant + assetCell.warningCountLabelWidthConstraint.constant
                
                if totalWidth == 60
                {
                    assetCell.alertCountContainerWidthConstraint.constant = 70.0
                }
                else if totalWidth == 40
                {
                    assetCell.alertCountContainerWidthConstraint.constant = 45.0
                }
                else if totalWidth == 20
                {
                    assetCell.alertCountContainerWidthConstraint.constant = 20.0
                }
                else
                {
                    assetCell.alertCountContainerWidthConstraint.constant = 0.0
                }
                
                
                return assetCell
                
                
            case "List":
                let assetCell : AssetsListCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AssetsListCollectionViewCellID", for: indexPath as IndexPath) as! AssetsListCollectionViewCell
                
                let assetGroup = assetGroupsArray[indexPath.row]
                
                
                
                assetCell.criticalAlertsCountLabel.layer.cornerRadius = 10
                assetCell.warningAlertsCountLabel.layer.cornerRadius = 10
                assetCell.resolvedAlertsCountLabel.layer.cornerRadius = 10
                
                assetCell.criticalAlertsCountLabel.layer.masksToBounds = true
                
                assetCell.assetNameLabel.text = assetGroup.name
                assetCell.assetDescriptionLabel.text = assetGroup.grpDescription
                assetCell.assetLocationLabel.text = assetGroup.location
                
                assetCell.assetNameLabel.font = UIFont(name: Styles.Style.boldStyleCustomFont, size: 14.0)
                assetCell.assetDescriptionLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)
                assetCell.assetLocationLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
                
                assetCell.assetImageView.layer.cornerRadius = 35
                
                
                
//                if let data = NSData(contentsOf: assetGroup.imageUrl as! URL) {
//                    assetCell.assetImageView.image = UIImage(data: data as Data)
//                }
                
                let nameSize = assetCell.assetNameLabel.sizeThatFits(CGSize.init(width: assetCell.assetNameLabel.frame.size.width, height: 200))
                assetCell.assetNameLabelHeight.constant = nameSize.height  + 1
                
                let descriptionSize = assetCell.assetDescriptionLabel.sizeThatFits(CGSize.init(width: assetCell.assetDescriptionLabel.frame.size.width, height: 200))
                assetCell.assetDescriptionLabelHeight.constant = descriptionSize.height  + 1
                
                let locationSize = assetCell.assetLocationLabel.sizeThatFits(CGSize.init(width: assetCell.assetLocationLabel.frame.size.width, height: 200))
                assetCell.assetLocationLabelHeight.constant = locationSize.height + 2
                
                
                assetCell.criticalAlertsCountLabel.text = "\(assetGroup.criticalAlertsCount)"
                assetCell.warningAlertsCountLabel.text = "\(assetGroup.warningsCount)"
                assetCell.resolvedAlertsCountLabel.text = "\(assetGroup.maintenanceAlertsCount)"
                
                if assetGroup.criticalAlertsCount == 0
                {
                    assetCell.criticalAlertsCountLabelWidth.constant = 0.0
                }
                else
                {
                    assetCell.criticalAlertsCountLabelWidth.constant = 20.0
                }
                
                if assetGroup.warningsCount == 0
                {
                    assetCell.warningCountLabelWidth.constant = 0.0
                }
                else
                {
                    assetCell.warningCountLabelWidth.constant = 20.0
                }
                
                if assetGroup.maintenanceAlertsCount == 0
                {
                    assetCell.resolvedAlertsCountLabelWidth.constant = 0.0
                }
                else
                {
                    assetCell.resolvedAlertsCountLabelWidth.constant = 20.0
                }
                
                
                let totalWidth = assetCell.resolvedAlertsCountLabelWidth.constant + assetCell.criticalAlertsCountLabelWidth.constant + assetCell.warningCountLabelWidth.constant
                
                if totalWidth == 60
                {
                    assetCell.alertCountContainerWidthConstraint.constant = 70.0
                }
                else if totalWidth == 40
                {
                    assetCell.alertCountContainerWidthConstraint.constant = 45.0
                }
                else if totalWidth == 20
                {
                    assetCell.alertCountContainerWidthConstraint.constant = 20.0
                }
                else
                {
                    assetCell.alertCountContainerWidthConstraint.constant = 0.0
                }
                
                
                
                if searchTextEntered.characters.count != 0
                {
                    self.highlightSearchStringforLabel(label: assetCell.assetNameLabel)
                    self.highlightSearchStringforLabel(label: assetCell.assetDescriptionLabel)
                    self.highlightSearchStringforLabel(label: assetCell.assetLocationLabel)
                }
                return assetCell
                
            case "Tree":
                let assetCell : AssetsListTreeViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AssetsListTreeViewCellID", for: indexPath as IndexPath) as! AssetsListTreeViewCell

                let org = treeArray[indexPath.row]
                assetCell.updateAssetCell(asset: org)
                return assetCell
                
            default:
                let assetCell : AssetsListGridViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AssetsListGridViewCellID", for: indexPath as IndexPath) as! AssetsListGridViewCell
                
                let assetGroup = assetGroupsArray[indexPath.row]
                assetCell.assetGroupNameLabel.text = assetGroup.name
                
                if let data = NSData(contentsOf: assetGroup.imageUrl as! URL) {
                    assetCell.assetGroupImageView.image = UIImage(data: data as Data)
                }
                return assetCell
                
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        if collectionView == filterCollectionView
        {
            if (section == 0 || section == 1)
            {
                return CGSize.init(width: collectionView.frame.size.width, height: 40)
            }
            else
            {
                return CGSize.init(width: collectionView.frame.size.width, height: 10)
            }
            
        }
        else
        {
            return CGSize.zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        
        if indexPath.section == 0 || indexPath.section == 1
        {
            let headerView  = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AssetsFilterCollectionReusableViewID", for: indexPath) as! AssetsFilterCollectionReusableView
            
            headerView.filterOptionsButton.setTitle(filterTypes[indexPath.section], for: .normal)
            
            headerView.filterOptionsButton.tag = indexPath.section + 1
            
            headerView.filterOptionsButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0)
            headerView.filterOptionsButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0)
            headerView.filterOptionsButton.addTarget(self, action: #selector(AssetsListViewController.filterTypeSelected(sender:)), for: UIControlEvents.touchUpInside)
            
            
            
            if ((indexPath.section == 0 && selectedLocationsArray.count > 0) || (indexPath.section == 1 && selectedSitedArray.count > 0))
            {
                headerView.filterOptionsButton.setImage(UIImage.init(named: filterTypeActiveImages[indexPath.section]), for: .normal)
                headerView.filterOptionsButton.setTitleColor(UIColor.black, for: .normal)
                headerView.filterOptionsButton.titleLabel?.font = UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)
            }
            else
            {
                headerView.filterOptionsButton.setImage(UIImage.init(named: filterTypeInActiveImages[indexPath.section]), for: .normal)
                
                headerView.filterOptionsButton.setTitleColor(UIColor.gray, for: .normal)
                headerView.filterOptionsButton.titleLabel?.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)
            }
            
            
            headerView.sectionSeparatorViewHeightConstraint.constant = 0
            return headerView
            
        }
        else
        {
            let headerView  = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AssetsFilterCollectionReusableViewID", for: indexPath) as! AssetsFilterCollectionReusableView
            headerView.sectionSeparatorViewHeightConstraint.constant = 1
            
            headerView.filterOptionsButton.setTitle("", for: .normal)
            headerView.filterOptionsButton.setImage(nil, for: .normal)
            
            return headerView
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == filterCollectionView
        {
            return CGSize(width: collectionView.frame.size.width, height: 40)
        }
        else
        {
            if layoutSelected == "List"
            {
                return CGSize(width: collectionView.frame.size.width, height: 110)
            }
            else if layoutSelected == "Grid"
            {
                return CGSize(width: (collectionView.frame.size.width - 20)/3, height: 140)
            }
            else if layoutSelected == "Tree"
            {
                return CGSize(width: collectionView.frame.size.width, height: 40)
            }
            else
            {
                return CGSize(width: 1, height: 1)
            }
        }
    }
    
    func removeFilterCondition(indexPath: IndexPath)
    {
        
        if indexPath.section == 0
        {
            selectedLocationsArray.remove(at: indexPath.row)
        }
        else
        {
            selectedSitedArray.remove(at: indexPath.row)
        }
        
        self.filterCollectionView.reloadData()
        
    }
    
    
    
    func filterTypeSelected(sender: UIButton!)
    {
        var height : Float = 0.0
        if sender.tag == 1
        {
            height = Float((locationsArray.count * 40) + 30)
        }
        else if sender.tag == 2
        {
            height = Float((sitesArray.count * 40) + 30)
        }
        else if sender.tag == 3
        {
            height = Float((typesArray.count * 40) + 30)
        }
        
        filterCriteriaSelectedLabel.text = filterTypes[sender.tag - 1]
        
        if filterOptionsViewHeightConstraint.constant  == 0.0
        {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                
                self.filterOptionsViewHeightConstraint.constant = 509.0
                
                
            }, completion: { (finished: Bool) -> Void in
                
                self.filterOptionsContainerViewHeightConstraint.constant = CGFloat(height)
                self.selectedFilterCriteriaTag = sender.tag
                self.filterOptionsTableView.reloadData()
                self.view.layoutSubviews()
            })
        }
        else
        {
            
            if selectedFilterCriteriaTag == sender.tag
            {
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                    
                    self.filterOptionsViewHeightConstraint.constant = 0.0
                    
                }, completion: { (finished: Bool) -> Void in
                    self.view.layoutSubviews()
                })
            }
            else
            {
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                    
                    self.filterOptionsViewHeightConstraint.constant = 500.0
                    
                }, completion: { (finished: Bool) -> Void in
                    
                    self.selectedFilterCriteriaTag = sender.tag
                    self.view.layoutSubviews()
                })
            }
            
        }
        
    }
    
    
    
    
    
    @IBAction func sortButtonPressed()
    {
        if sortButton.tag == 0 {
            let image = UIImage(named: "ic_sort_ascending")
            sortButton.setImage(image, for: .normal)
            sortButton.tag = 1
            self.assetGroupsArray = (self.assetGroupsArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "criticalAlertsCount", ascending: true), NSSortDescriptor(key: "warningsCount", ascending: true), NSSortDescriptor(key: "resolvedAlertsCount", ascending: true)]) as! Array
        }
        else
        {
            let image = UIImage(named: "ic_sort_descending")
            sortButton.setImage(image, for: .normal)
            sortButton.tag = 0
            self.assetGroupsArray = (self.assetGroupsArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "criticalAlertsCount", ascending: false), NSSortDescriptor(key: "warningsCount", ascending: false), NSSortDescriptor(key: "resolvedAlertsCount", ascending: false)]) as! Array
        }
        
        
        self.assetsCollectionView.reloadData()
    }
    
    @IBAction func swapViewButtonPressed()
    {
        
        
        if self.layoutOptionsViewHeightConstraint.constant == 0
        {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                self.swapViewButton.imageView?.image = UIImage.init(named: "ic_list_view_active")
                self.layoutOptionsViewHeightConstraint.constant = 200.0
                
            }, completion: { (finished: Bool) -> Void in
                
                self.view.layoutSubviews()
                
            })
        }
        else
        {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                self.swapViewButton.imageView?.image = UIImage.init(named: "ic_list_view")
                self.layoutOptionsViewHeightConstraint.constant = 0.0
                
            }, completion: { (finished: Bool) -> Void in
                
                self.view.layoutSubviews()
                
            })
        }
    }
    
    @IBAction func filterButtonPressed()
    {
        
        if self.filterViewWidthConstraint.constant == 0
        {
            UIView.animate(withDuration: 2.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                
                self.filterViewWidthConstraint.constant = 250.0
                
            }, completion: { (finished: Bool) -> Void in
                
                self.view.layoutSubviews()
                self.filterCollectionView.reloadData()
                self.filterButton.setImage(UIImage.init(named: "ic_filter_active"), for: .normal)
                
            })
        }
        else
        {
            UIView.animate(withDuration: 2.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                
                self.filterViewWidthConstraint.constant = 0.0
                
            }, completion: { (finished: Bool) -> Void in
                
                self.isFiltered = true
                self.view.layoutSubviews()
//                self.filterAssetsForSelectedConstraints()
//                self.filterCollectionView.reloadData()
                self.filterButton.setImage(UIImage.init(named: "ic_filter_inactive"), for: .normal)
                
            })
        }
        
        //        if isFiltered
        //        {
        //            assetGroupsArray = tempAssetGroupsArray
        //            self.assetsCollectionView.reloadData()
        //            self.isFiltered = false
        //        }
        //        else
        //        {
        //
        //        }
    }
    
    
    func filterAssetsForSelectedConstraints()
    {
        var locationPredicate: NSPredicate?
        var sitesPredicate: NSPredicate?
        var typesPredicate: NSPredicate?
        var filterPredicate: NSPredicate = NSPredicate(format: "location != %@","")
        
        for location in selectedLocationsArray
        {
            if locationPredicate != nil
            {
                locationPredicate = NSCompoundPredicate.init(type: .or, subpredicates: [locationPredicate!,NSPredicate(format: "location == %@", location)])
            }
            else
            {
                locationPredicate = NSPredicate(format: "location == %@", location)
            }
        }
        
        if locationPredicate != nil
        {
            filterPredicate = NSCompoundPredicate.init(type: .and, subpredicates: [filterPredicate, locationPredicate!])
        }
        
        
        
        for site in selectedSitedArray
        {
            if sitesPredicate != nil
            {
                sitesPredicate = NSCompoundPredicate.init(type: .or, subpredicates: [sitesPredicate!, NSPredicate(format: "siteId == %d", site)])
            }
            else
            {
                sitesPredicate = NSPredicate(format: "siteId == %d", site)
            }
        }
        
        if sitesPredicate != nil
        {
            filterPredicate = NSCompoundPredicate.init(type: .and, subpredicates: [filterPredicate, sitesPredicate!])
        }
        
        if isWithoutAlertsSelected
        {
            
            if typesPredicate != nil
            {
                typesPredicate = NSCompoundPredicate.init(type: .or, subpredicates: [typesPredicate!, NSPredicate(format: "(criticalAlertsCount == 0) AND (warningsCount == 0) AND (maintenanceAlertsCount == 0)")])
            }
            else
            {
                typesPredicate = NSPredicate(format: "(criticalAlertsCount == 0) AND (warningsCount == 0) AND (maintenanceAlertsCount == 0)")
            }

        }
        else
        {
            for type in selectedTypes
            {
                
                if type == "Critical"
                {
                    if typesPredicate != nil
                    {
                        typesPredicate = NSCompoundPredicate.init(type: .or, subpredicates: [typesPredicate!, NSPredicate(format: "criticalAlertsCount > 0")])
                    }
                    else
                    {
                        typesPredicate = NSPredicate(format: "criticalAlertsCount > 0")
                    }
                    
                }
                else if type == "Warning"
                {
                    if typesPredicate != nil
                    {
                        typesPredicate = NSCompoundPredicate.init(type: .or, subpredicates: [typesPredicate!, NSPredicate(format: "warningsCount > 0")])
                    }
                    else
                    {
                        typesPredicate = NSPredicate(format: "warningsCount > 0")
                    }
                }
                else
                {
                    if typesPredicate != nil
                    {
                        typesPredicate = NSCompoundPredicate.init(type: .or, subpredicates: [typesPredicate!, NSPredicate(format: "maintenanceAlertsCount > 0")])
                    }
                    else
                    {
                        typesPredicate = NSPredicate(format: "maintenanceAlertsCount > 0")
                    }
                }
            }
        }
        
        
        if typesPredicate != nil
        {
            filterPredicate = NSCompoundPredicate.init(type: .and, subpredicates: [filterPredicate, typesPredicate!])
        }
        
        
        
        
//        tempAssetGroupsArray = assetGroupsArray
        assetGroupsArray = (tempAssetGroupsArray as NSArray).filtered(using: filterPredicate) as! Array<AssetGroup>
        self.assetsCollectionView.reloadData()
        
        
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil , action: nil )
        
        var index: IndexPath?
        if(segue.identifier == "AssetGroupDetails"){
            if let cell = sender as? UICollectionViewCell{
                index = self.assetsCollectionView.indexPath(for: cell)
                
            }else if let cell = sender as? UICollectionViewCell{
                index = self.assetsCollectionView?.indexPath(for: cell)
            }
            
            let destinationVC = segue.destination as! AssetGroupDetailsViewController
            destinationVC.assetGroup = self.assetGroupsArray[(index?.row)!]
            
        }
        
    }
    
    
    func loadFilterParametersFromServer(){
        let filterManager = FilterWebServiceManager()
        
        DispatchQueue.global().async { [weak self] () -> Void in
            
            
            
            filterManager.fetchGeolocationAndSitesForFilter(success: {
                (site) in
                if(self != nil){
                    //                    self!.didFilterParamServiceSucceed = true
                    for(_,siteLoc) in (site?.enumerated())!{
                        if(URCManager.sharedInstance.geolocations.isEmpty){
                            URCManager.sharedInstance.geolocations.append(siteLoc.location)
                        }else{
                            var isExists = false
                            for(_,storedLoc) in URCManager.sharedInstance.geolocations.enumerated(){
                                if(storedLoc.id == siteLoc.location.id){
                                    isExists = true
                                    break
                                }
                            }
                            if(!isExists){
                                URCManager.sharedInstance.geolocations.append(siteLoc.location)
                            }
                        }
                    }
                    URCManager.sharedInstance.sites = site!
                    
                    self?.sitesArray = URCManager.sharedInstance.sites
                    self?.locationsArray = URCManager.sharedInstance.geolocations
                    
                }
                
            }, failure: {
                (error,statusCode) in
                print(error)
                if(self != nil){
                    //                    self!.didFilterParamServiceSucceed = false
                    //                    self!.showErrorFromService(statusCode)
                }
            })
        }
    }
    
    
    // MARK: - Filter
    
    
    
    func filterAssetGroupsForSearchString(searchText: String)
    {
        if searchText == ""
        {
            assetGroupsArray = tempAssetGroupsArray
        }
        else
        {
            let predicate = NSPredicate(format: "name contains[c] %@ OR grpDescription contains[c] %@ OR location contains[c] %@", searchText, searchText, searchText)
            assetGroupsArray = (tempAssetGroupsArray as NSArray).filtered(using: predicate) as! Array<AssetGroup>
        }
        
        self.assetsCollectionView.reloadData()
    }
    
    func highlightSearchStringforLabel(label: UILabel)
    {
        
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: label.text!)
        let pattern = searchTextEntered.lowercased()
        let range: NSRange = NSMakeRange(0, label.text!.characters.count)
        
        let regex = try! NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options())
        
        regex.enumerateMatches(in: label.text!.lowercased(), options: NSRegularExpression.MatchingOptions(), range: range) { (textCheckingResult, matchingFlags, stop) -> Void in
            let subRange = textCheckingResult?.range
            attributedString.addAttribute(NSBackgroundColorAttributeName, value: UIColor.yellow, range: subRange!)
        }
        
        label.attributedText = attributedString
        
    }

    
    // MARK: - Search Delegates
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
        if searchbarHeightConstraint.constant > 0.0
        {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                
                self.searchbarHeightConstraint.constant = 0.0
                
            }, completion: { (finished: Bool) -> Void in
                self.searchTextEntered = ""
                self.assetGroupsArray = self.tempAssetGroupsArray
                self.assetsCollectionView.reloadData()
                self.view.layoutSubviews()
            })
        }
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        searchTextEntered = searchText
        filterAssetGroupsForSearchString(searchText: searchText)
        print(searchText)
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool
    {
        tempAssetGroupsArray = assetGroupsArray
        return true
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool
    {
        searchTextEntered = ""
        assetGroupsArray = tempAssetGroupsArray
        self.assetsCollectionView.reloadData()
        return true
    }
    
    
    // MARK: - Filter Delegates
    
    
    @IBAction func filterOptionsDoneButtonPressed(_ sender: Any)
    {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
            
            self.filterOptionsViewHeightConstraint.constant = 0.0
            
        }, completion: { (finished: Bool) -> Void in
            self.view.layoutSubviews()
        })
        
        self.filterCollectionView.reloadData()
        filterAssetsForSelectedConstraints()
    }
    
    
    
    // MARK: - Tableview DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch selectedFilterCriteriaTag {
        case 1:
            return locationsArray.count
        case 2:
            return sitesArray.count
        case 3:
            return typesArray.count
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let filterCell : FilterTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCellID", for: indexPath) as! FilterTableViewCell
        
        switch selectedFilterCriteriaTag {
        case 1:
            filterCell.filterCriteriaValueLabel.text = locationsArray[indexPath.row].name
            
            if selectedLocationsArray.contains(locationsArray[indexPath.row].name)
            {
                filterCell.isSelectedImageView.image = UIImage(named: "ic_alert_check")
            }
            else
            {
                filterCell.isSelectedImageView.image = nil
            }
            
        case 2:
            filterCell.filterCriteriaValueLabel.text = sitesArray[indexPath.row].siteName
            
            if selectedSitedArray.contains(sitesArray[indexPath.row].siteId)
            {
                filterCell.isSelectedImageView.image = UIImage(named: "ic_alert_check")
            }
            else
            {
                filterCell.isSelectedImageView.image = nil
            }
            
        case 3:
            filterCell.filterCriteriaValueLabel.text = typesArray[indexPath.row]
            
            if selectedTypes.contains(typesArray[indexPath.row])
            {
                filterCell.isSelectedImageView.image = UIImage(named: "ic_alert_check")
            }
            else
            {
                filterCell.isSelectedImageView.image = nil
            }
            
        default:
            filterCell.filterCriteriaValueLabel.text = ""
        }
        
        
        return filterCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch selectedFilterCriteriaTag {
        case 1:
            if selectedLocationsArray.contains(locationsArray[indexPath.row].name)
            {
                
                if let index = selectedLocationsArray.index(of: locationsArray[indexPath.row].name) {
                    selectedLocationsArray.remove(at: index)
                }
            }
            else
            {
                selectedLocationsArray.append(locationsArray[indexPath.row].name)
            }
            
            self.filterOptionsTableView.reloadData()
            
        case 2:
            if selectedSitedArray.contains(sitesArray[indexPath.row].siteId)
            {
                
                if let index = selectedSitedArray.index(of: sitesArray[indexPath.row].siteId) {
                    selectedSitedArray.remove(at: index)
                }
            }
            else
            {
                selectedSitedArray.append(sitesArray[indexPath.row].siteId)
            }
            
            self.filterOptionsTableView.reloadData()
            
            
        case 3:
            if selectedTypes.contains(typesArray[indexPath.row])
            {
                
                if let index = selectedTypes.index(of: typesArray[indexPath.row]) {
                    selectedTypes.remove(at: index)
                }
            }
            else
            {
                selectedTypes.append(typesArray[indexPath.row])
            }
            
            self.filterOptionsTableView.reloadData()
            
        default:
            print("")
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        if collectionView == filterCollectionView
        {
            if indexPath.section == 3
            {
                if selectedTypes.contains(typesArray[indexPath.row])
                {
                    if let index = selectedTypes.index(of: typesArray[indexPath.row]) {
                        selectedTypes.remove(at: index)
                    }
                }
                else
                {
                    selectedTypes.append(typesArray[indexPath.row])
                }
                
            }
            else if indexPath.section == 2
            {
                if indexPath.row == 2
                {
                    if isMyAssetsSelected
                    {
                        isMyAssetsSelected = false
                    }
                    else
                    {
                        isMyAssetsSelected = true
                        isNotMyAssetsSelected = false
                    }
                }
                else if indexPath.row == 3
                {
                    if isNotMyAssetsSelected
                    {
                        isNotMyAssetsSelected = false
                    }
                    else
                    {
                        isNotMyAssetsSelected = true
                        isMyAssetsSelected = false
                    }
                }
                else if indexPath.row == 0
                {
                    if isWithAlertsSelected
                    {
                        isWithAlertsSelected = false
                    }
                    else
                    {
                        isWithAlertsSelected = true
                        isWithoutAlertsSelected = false
                    }
                }
                else
                {
                    if isWithoutAlertsSelected
                    {
                        isWithoutAlertsSelected = false
                    }
                    else
                    {
                        isWithoutAlertsSelected = true
                        isWithAlertsSelected = false
                    }
                }
            }
            
            
            self.filterCollectionView.reloadData()
            filterAssetsForSelectedConstraints()
        }
        else
        {
            if layoutSelected == "Tree"
            {
                let org = treeArray[indexPath.row]
                
                
                if (expandedObjectsArray as NSArray).contains(org)
                {
                    
                    let arrays: NSArray = treeArray as NSArray
                    let hierarchyArrayCopy: NSArray = hierarchyArray as NSArray
                    for i in indexPath.row+1..<arrays.count // 3..7
                    {
                        if hierarchyArrayCopy[i] as! Int > hierarchyArrayCopy[indexPath.row] as! Int
                        {

                            hierarchyArray.remove(at:  (hierarchyArray as NSArray).index(of: hierarchyArrayCopy.object(at: i)))
                            
                            treeArray.remove(at: (treeArray as NSArray).index(of: arrays.object(at: i)))
                            
                            if (expandedObjectsArray as NSArray).contains(arrays.object(at: i))
                            {
                                expandedObjectsArray.remove(at: (expandedObjectsArray as NSArray).index(of: arrays.object(at: i)))
                            }
  
                        }
                        else
                        {
                            break
                        }
                    }
                    
                    expandedObjectsArray.remove(at: (expandedObjectsArray as NSArray).index(of: org))
                    
                }
                else
                {
                    expandedObjectsArray.append(org)
                    if org is Organization
                    {
                        let organisation = org as! Organization
                        
                        if organisation.childOrganisations.count > 0
                        {
                            for childOrg in organisation.childOrganisations
                            {
                                treeArray.insert(childOrg, at: indexPath.row + 1)
                                
                                let hie = hierarchyArray[indexPath.row] + 1
                                hierarchyArray.insert(hie, at: indexPath.row + 1)
                                
                            }
                        }
                        else
                        {
                            self.activityIndicator.isHidden = false
                            self.activityIndicator.startAnimating()
                            getSitesDataForOrganization(organization: organisation, index: indexPath.row)
                        }
                        
                    }
                    else if org is Site
                    {
                        let site = org as! Site
                        
                        self.activityIndicator.isHidden = false
                        self.activityIndicator.startAnimating()
                        
                        getAssetGroupsForSite(site: site, index: indexPath.row)
                        
                    }
                    else if org is AssetGroup
                    {
                        let assetGroup = org as! AssetGroup
                        
                        self.activityIndicator.isHidden = false
                        self.activityIndicator.startAnimating()
                        
                        getDevicesForAssetGroup(assetGroup: assetGroup, index: indexPath.row)
                        
                        
                    }
                    
                    else if org is Asset
                    {
                        let asset = org as! Asset
                        let vc : AssetDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AssetDetailsViewControllerID") as! AssetDetailsViewController
                        
                        vc.asset = asset
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                        
//                        let assetGroup = org as! AssetGroup
//                        
//                        self.activityIndicator.isHidden = false
//                        self.activityIndicator.startAnimating()
//                        
//                        getDevicesForAssetGroup(assetGroup: assetGroup, index: indexPath.row)
                        
                        
                    }
                }
                
                print(hierarchyArray)
                
                self.assetsCollectionView.reloadData()
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
            }
        }
    }
    
    func getDevicesForAssetGroup(assetGroup: AssetGroup, index: Int)
    {
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        let webAction = ManagedDevicesWebServiceManager()
        let params = ["assetGroupId":"\(assetGroup.id)"]
        webAction.loadDevices(parameters: params as [String : String]!, success: {
            (response) in
            if let response = response{
                
                
                for asset in response
                {
                    self.treeArray.insert(asset, at: index + 1)
                    let hie = self.hierarchyArray[index] + 1
                    self.hierarchyArray.insert(hie, at: index + 1)
                }
                self.assetsCollectionView.reloadData()
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
            }
        }, failure: {
            (error,statusCode) in

            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
        })
    }
    
    func getAssetGroupsForSite(site: Site, index: Int)
    {
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        let service = AssetGroupWebService()
        
        service.loadAssetGroupsForSite(siteId: "\(site.siteId)", success: {
            (response) in
            
            for assetGroup in response!
            {
                self.treeArray.insert(assetGroup, at: index + 1)
                let hie = self.hierarchyArray[index] + 1
                self.hierarchyArray.insert(hie, at: index + 1)
            }
            self.assetsCollectionView.reloadData()
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
            
            
        }, failure: {
            (error,statusCode) in
            print(error!)
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
        })
    }
    
    
    func getSitesDataForOrganization(organization:Organization, index: Int)    {
        
        let filter = FilterWebServiceManager()
        filter.fetchSites(orgId: "\(organization.orgID)", success: {
            (response) in
            
            self.addSitesToOrganization(organization: organization, sites: response!, index: index)
            
            
            
        }, failure: {
            (error,statusCode) in
            print(error)
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
        })
    }
    
    
    func addSitesToOrganization(organization:Organization, sites: Array<Site>, index: Int)
    {
        for site in sites
        {
            treeArray.insert(site, at: index + 1)
            let hie = hierarchyArray[index] + 1
            hierarchyArray.insert(hie, at: index + 1)

        }
        
        self.assetsCollectionView.reloadData()
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
        

    }
    
    // Mark - layout changes
    
    
    @IBAction func layoutSelected(_ sender: UIButton)
    {
        if layoutTypes[sender.tag - 1] != layoutSelected
        {
            
            layoutSelected = layoutTypes[sender.tag - 1]
            
            
            if sender.tag == 3 || sender.tag == 4
            {
                self.navigationItem.rightBarButtonItem?.isEnabled = false
                self.filterButton.isEnabled = false
                self.sortButton.isEnabled = false
                self.severityLabel.isEnabled = false
            }
            else
            {
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                self.filterButton.isEnabled = true
                self.sortButton.isEnabled = true
                self.severityLabel.isEnabled = true
            }
            
            self.swapViewButton.imageView?.image = UIImage.init(named: self.layoutTypeImages[sender.tag - 1])
            
            if sender.tag == 4
            {
                UIView.animate(withDuration: 1.0, animations: {
                    self.mapViewHeightConstraint.constant = 504
                    self.layoutOptionsViewHeightConstraint.constant = 0.0
                    
                    
                }, completion: { (Bool) in
                    self.loadMapView()
                    self.view.layoutSubviews()
                })
            }
            else if sender.tag != 3
            {
                
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                    
                    self.layoutOptionsViewHeightConstraint.constant = 0.0
                    self.mapViewHeightConstraint.constant = 0.0
                    
                }, completion: { (finished: Bool) -> Void in
                    
                    self.view.layoutSubviews()
                    self.assetsCollectionView.reloadData()
                })
                
            }
            else if sender.tag == 3
            {
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                    
                    self.layoutOptionsViewHeightConstraint.constant = 0.0
                    self.mapViewHeightConstraint.constant = 0.0
                    
                }, completion: { (finished: Bool) -> Void in
                    
                    self.view.layoutSubviews()
                    self.loadOrgData()
                })
                
            }
            
        }
        else
        {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                
                self.layoutOptionsViewHeightConstraint.constant = 0.0
                
            }, completion: { (finished: Bool) -> Void in
                
                self.view.layoutSubviews()
            })
        }

    }
    
    
    
    func updateSitesData(organization: Organization)
    {
        
    }
    
    
    
    // Mark - map View
    
    func loadMapView()
    {
        let assetGroup = self.assetGroupsArray.first
        let initialLocation = CLLocation(latitude: (assetGroup?.geoLocationInfo.geoLocationCenterLatitude)!, longitude: (assetGroup?.geoLocationInfo.geoLocationCenterLongitude)!)
        
        centerMapOnLocation(location: initialLocation)
        
        addAllSiteAnnotations()
    }
    
    func centerMapOnLocation(location: CLLocation) {
        
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 5.0, regionRadius * 5.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    func addAllSiteAnnotations(){
        let sites = URCManager.sharedInstance.sites
        
        var annotations : Array<SiteMKAnnotation> = []
        for (_,site) in sites.enumerated(){
            
            let site = SiteMKAnnotation(title: site.siteName , id: site.siteId,
                                        coordinate: CLLocationCoordinate2D(latitude: site.location.latitude, longitude: site.location.longitude))
            annotations.append(site)
            
            self.mapView!.addAnnotation(site as MKAnnotation)
        }
        
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if(annotation.isKind(of: MKUserLocation.self) == true){
            return nil
        }
        if (annotation.isKind(of: SiteMKAnnotation.self) == true) {
            let identifier = "customPin"
            var view: MKAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            { // 2
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                // 3
                view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
                //let imageName = annotation.imageName
                view.image = UIImage(named: Constants.ImagesName.MAP_PIN_GREEN.rawValue)
                
            }
            
            return view
        }
        
        var view: MKAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: "pin")
        { // 2
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            // 3
            view = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            //let imageName = annotation.imageName
            view.image = UIImage(named: Constants.ImagesName.MAP_PIN_GREEN.rawValue)
            
        }
        
        return view
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl)
    {
        print("y")
        self.layoutSelected(listLayoutButton)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView)
    {
        print("s")
    }
    
    
    @IBAction func resetFilterButtonPressed() {
        
        selectedSitedArray = []
        selectedLocationsArray = []
        selectedTypes = []
        isMyAssetsSelected = false
        isNotMyAssetsSelected = false
        isWithAlertsSelected = false
        isWithoutAlertsSelected = false
        filterCollectionView.reloadData()
        
    }
    
    
    func loadOrgData()
    {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        let orgWebAction = OrganizationsWebServiceManager()
        orgWebAction.loadOrganizations(success: {
            (response) in
            if let response = response{
                
                self.organizationsArray = response
                self.treeArray = response
                
                
                self.assetsCollectionView.reloadData()
                self.generateData()

                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                //                self.loadFilterParametersFromServer()
                
            }
        }, failure:
            {
                (error,statusCode) in
                if(statusCode != 0){
                    
                }
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
        })
    }
    
    func generateData()
    {

        for _ in treeArray
        {
            hierarchyArray.append(1)
        }
    }
    
}


