//
//  CardOptionsCollectionViewCell.swift
//  URC
//
//  Created by Saran Mahadevan on 06/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class CardOptionsCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var cardOptionImageView: UIImageView!
    @IBOutlet weak var cardOptionNameLabel: UILabel!
    
    
    
}
