//
//  Constants.swift
//  URC
//
//  Created by Saran Mahadevan on 28/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

let kBASE_URL_OLD:String = "http://mbaas-poc2.westus.cloudapp.azure.com:8080/api"
let kGRAPH_BASE_URL: String = "http://waygumdemo1.cloudapp.net:3000/dashboard/db"
//let kBASE_URL:String = "http://indiadevres4.cloudapp.net:8080/api"
let kBASE_URL:String = "http://indiadevmisc.cloudapp.net:8080/api"
//let kBASE_URL:String = "http://indadevmisc2.cloudapp.net:8080/api"
//let kBASE_URL:String = "http://waygumdemo2.cloudapp.net:8080/api"
//let kBASE_URL: String = "https://mbaas2.waygum.io/api/"

let kASSET_GROUPS_URL :String = "\(kBASE_URL)/managedAssetGroups"
let kASSETS_URL :String = "\(kBASE_URL)/managedDevices"
let kALERTS_URL :String = "\(kBASE_URL)/alerts"
let kASSETS_READING_URL :String = "\(kBASE_URL)/measurementEvents?deviceId="
let kGEOLOCATIONS_AND_SITES_LIST_URL : String = "\(kBASE_URL)/managedSites"
let kFILTERED_ASSET_GROUPS_URL : String = "\(kASSET_GROUPS_URL)/search"
let kROCKET_CHAT_URL : String = "http://collab-poc.westus.cloudapp.azure.com:3000/"

let kSUB_IDENTITY_BASED_ORG_URL_FIRSTPART : String = "\(kBASE_URL)/identitySubjects/"
let kSUB_IDENTITY_BASED_ORG_URL_LASTPART : String = "/organizations"
let kPARENT_ORG_HIERARCHY_URL_FIRSTPART : String = "\(kBASE_URL)/organizations/"
let kPARENT_ORG_HIERARCHY_URL_LASTPART : String = "/hierarchy"
let kORG_BASED_SITES_URL : String = "\(kBASE_URL)/managedSites?orgId="
let kSITE_BASED_ASSETGROUPS_URL : String = "\(kBASE_URL)/managedAssetGroups?siteId="
let kASSET_GROUP_BASED_ASSETS_URL_FIRSTPART : String = "\(kBASE_URL)/managedAssetGroups/"
let kASSET_GROUP_BASED_ASSETS_URL_LASTPART : String = "/managedDevices/"

let kLOGIN_URL = "\(kBASE_URL)/authenticate"
let kLOGOUT_URL = "\(kBASE_URL)/logout"

let kORGANIZATION_URL = "\(kBASE_URL)/organizations"
let kORGANIZATION_BY_ID_URL = "\(kORGANIZATION_URL)/"
let kORGANIZATION_BY_UUID_URL = "\(kORGANIZATION_URL)?orgId="

let kNOTIFICATIONS_URL = "\(kBASE_URL)/notifications"
let kACKNOWLEDGE_NOTIFICATIONS_URL = "\(kBASE_URL)/notifications/acknowledge"

let kMANAGED_SITES_FOR_ORGID_URL = "\(kBASE_URL)/managedSites?orgId="
let kMANAGED_ASSETGRP_FOR_SITEID_URL = "\(kBASE_URL)/managedAssetGroups?siteId="
let kDEVICE_SPECIFICATION_URL = "\(kBASE_URL)/deviceSpecifications"
let kCREATE_DEVICE_URL = "\(kBASE_URL)/managedDevices"

let kCREATE_ORG_USER_URL = "\(kBASE_URL)/organizationWithUser"
let kCREATE_USER_URL = "\(kBASE_URL)/userSubjects"
let kCREATE_SITE_NETWORK_URL_FIRSTPART = "\(kORGANIZATION_URL)/"
let kCREATE_SITE_NETWORK_URL_SECONDPART = "/managedSites"
let kGET_EVENT_GROUPS_URL_FIRSTPART = "\(kBASE_URL)/eventAttributes/json?group="
let kGET_EVENT_GROUPS_URL_LASTPART = "&source=Monnit"

let kGRAFANA_DOMAIN = "https://mbaas2.waygum.io:3001/"
let kGRAFANA_API_KEY = "eyJrIjoiMU1oSExYMUdwMjZrMHhaUUIxUGJQaXJLZmkzUVAwNEEiLCJuIjoiSW9zQXBwIiwiaWQiOjF9"

let kSORT_ARRAY: Dictionary<String,String> = ["Name":"name","Location":"location","Site":"location","Id":"id","Severity":"notificationCount"]

let kFILTER_CHECKBOX: Dictionary<String,String> = ["My Assets":"myAssets","Not My Assets":"othersAssets","Without Alerts":"noalerts","With Alerts":"withAlerts"]

//let kFILTER_DROPDOWN_DETAILS: Array<Dictionary<String,AnyObject>> = [
//    ["ModelValue":"geoLocationInfoIds" as AnyObject,"DisplayName":"Filter Locations" as AnyObject,"ValueFunction":Constants.getLocationDropDownOptions() as AnyObject,"Image":"ic_filter_location.png" as AnyObject,"Image_Active":"ic_filter_location_active.png" as AnyObject,"Tag":1207 as AnyObject],
//    ["ModelValue":"managedSiteIds" as AnyObject,"DisplayName":"Filter Sites","ValueFunction": Constants.getSiteDropDownOptions(),"Image":"ic_filter_site.png","Image_Active":"ic_filter_site_active.png","Tag":1208],
//    ["ModelValue":"type","DisplayName":"Filter Types","ValueFunction": Constants.getTypeDropDownOptions(),"Image":"","Image_Active":"ic_filter_location_active.png","Tag":1209]
//]

let kDROPDOWN_LOCATION_OPTIONS: Array<String> = ["None","Central Ohio","San Francisco","Southern California","Texas","Germany","Scotland","France"]
let kDROPDOWN_SITE_OPTIONS: Array<String> = ["None", "Site 1","Site 2","Site 3","Site 4","Site 5"]
let kDROPDOWN_TYPE_OPTIONS: Array<String> = ["None","Sensor","Gateway","Machine","Machine Group"]
let kDROPDOWN_NONE_OPTION: String = "None"
let kTRUE: String = "true"
let kFALSE: String = "false"
let kDASHBOARD_TITLE: String = "Dashboard"
let kINSTALLDEVICETITLE: String = "Install Device"
let kCOLLABORATION_TITLE: String = "Collaboration"
let kINSTALLED_DEVICE_UUID_PREFIX : String = "MAD"
let kUSER_ROLES_FOR_USER_ADMIN : Array<String> = ["ManagedSite-C","ManagedAssetGroup-C","Organization-U","Organization-R","Organization-M","ManagedSite-R","ManagedSite-U","ManagedAssetGroup-R","ManagedAssetGroup-U","ManagedDevice-C","ManagedDevice-R","ManagedDevice-U"]

//let kHELP_SCREENS_CONFIG : Array<Dictionary<String,AnyObject>> = [
//    ["ImageName":"help1.png","HelpText":"The dashboard presents the high-level information of all the industrial asset groups in one go. By default all the assets linked to your organization(s) are shown.\n\nFrom the dashboard you can choose a specific asset group that you wish to view."],
//    ["ImageName":"help2.png","HelpText":"\n You can filter assets based on the industrial site or location"],
//    ["ImageName":"help3.png","HelpText":"You can change the dashboard view to one of the following.\nListView - The default view that gives the asset groups as a list.\nGrid View - Booksheld view of asset groups.\nTree View - Hierarchial view of organizations, sites asset groups and assets\nMap View - View sites plotted on a map"],
//    ["ImageName":"help4.png","HelpText":"A quick view of all the assets tagged to the asset group are displayed along with the latest measurements. From this view, you can drill-down to a detailed view of individual assets."],
//    ["ImageName":"help5.png","HelpText":"Graphical and tabular visualization of data are available. You also have an option to edit device parameters. Additional information pertaining to manuals and OEM helpline can also be viewed."]]

class Constants: NSObject {
    
    struct Notifications {
        static let DashboardSelected = "DashboardSelected"
        static let AlertsSelected = "AlertsSelected"
        static let CollaborationSelected = "CollaborationSelected"
        static let InstallDeviceSelected = "InstallDeviceSelected"
        static let UserSettingsSelected = "UserSettingsSelected"
        static let AccountSettingsSelected = "AccountSettingsSelected"
        static let LogOutSelected = "LogOutSelected"
        static let HelpSelected = "HelpSelected"
    }
    
    struct MenuItems {
        
        static let CreateAccount = "Create Account"
        static let Alerts = "Alerts"
        static let Dashboard = "Dashboard"
        static let InstallDevice = "Install Device"
        static let Collaboration = "Collaboration"
        static let UserSettings = "User Settings"
        static let AccountSettings = "Account Settings"
        static let Help = "Help"
        static let LogOut = "Logout"
    }
    
    struct ScreenNames {
        static let Dashboard_List = "List Dashboard"
        static let Dashboard_Map = "Map Dashboard"
        static let Dashboard_Grid = "Grid Dashboard"
        static let Dashboard_Tree = "Tree Dashboard"
        static let Devices = "Devices"
        static let Graphical_Data = "Graphical Data"
        static let Tabulated_Data = "Tabulated Data"
        static let Edit_Device = "Edit Device"
        static let More_Device = "More Device"
        static let Manuals = "Manuals"
        static let Call_OEM_Helpline = "Call OEM Helpline"
        static let View_Ownership = "View_Ownership"
        static let Edit_User = "Edit User"
        static let Help = "Help"
        static let LogOut = "Logout"
    }
    
    struct UserRoles {
        static let CreateSubOrg = "Organization-C"
        static let ManageUsersInSubOrg = "Organization-A-M"
        static let ManageUsersInOrg = "Organization-M"
    }
    
    struct UIInterfaceElementsIdentifier {
        static let MainStoryBoard = "Main"
        static let LoginVC = "LoginViewController"
        static let AccInstructionVC = "AccountCreationInstructionVC"
        static let UserInstructionVC = "UserCreationInstructionVC"
    }
    
    
    struct DeviceType {
        static let Sensor = "sensor"
        static let Gateway = "gateway"
    }
    
    struct SignUpMailOptoins{
        static let ToRecipients = ["support@waygum.io"]
        static let Subject = "Sign Up"
        static let MessageBody = "Thanks for your Interest.Send us the below details to get your login credentials.\n First Name : \n Last Name : \n Company Name: \n Contact Phone: \n Reason for installing app : \n"
        static let AlternativeMessage = "Mail to \(Constants.SignUpMailOptoins.ToRecipients[0]) with your name,contact number,company name and the reason for installing, to get your login credentials."
    }
    
    enum Symbols: String{
        case Percentage = " %"
        case NewLineCharacter = "\n"
        case JsonNewLineChar = "\\n"
    }
    
    enum Labels: String{
        case kWIRED_SIGNAL_LABEL = "Wired"
        case kWIRED_BATTERY_LABEL = "Line"
        case kSIDEMENU_ITEM_DASHBOARD = "Dashboard"
        case kSIDEMENU_ITEM_COLLABORATION = "Collaboration"
        case kSIDEMENU_ITEM_INSTALLDEVICE = "Install device"
        case kSIDEMENU_ITEM_LOGOUT = "Log out"
        case kNO_DATA_FOUND = "No Data Found"
        case kDISPLAY_OPTION_LIST = "LIST"
        case kDISPLAY_OPTION_GRID = "GRID"
        case kDISPLAY_OPTION_TREE = "TREE"
        case kDISPLAY_OPTION_MAP = "MAP"
        case kCALL_OEM_HELPLINE = "Call OEM Helpline"
        case kVIEW_OWNERSHIP_DETAILS = "View Ownership Details"
        case kOEM_ADDRESS_HEADING = "OEM"
        case kOWNERSHIP_ADDRESS_HEADING = "Owner"
        case kOEM_CONTACT_HEADING = "Call Helpline"
        case kOWNERSHIP_CONTACT_HEADING = "Contact"
        case kOEM_NAVIGATION_TITLE = "OEM Helpline"
        case kOWNERSHIP_NAVIGATION_TITLE = "Ownership Details"
        case kMANUALS_TITLE = "Manuals"
        case kTAB_MORE_ITEM_HEADING = "More"
        case kTAB_DATA_ITEM_HEADING = "Data"
        case kTAB_GRAPHS_ITEM_HEADING = "Graphs"
        case kTAB_ALERTS_ITEM_HEADING = "Alerts"
        case kTAB_CONTROL_ITEM_HEADING = "Control"
        case kCURRENT_LOCATION = "Current Location"
        case kDEFAULT_WEBVIEW_TITLE = "Document"
        case kWEBSITE_LABEL = "Website"
        case kINSTALL_DEVICE_STEP1_LABEL = "Step 1"
        case kINSTALL_DEVICE_STEP1_INSTRUCTION = "Select the device specification:"
        case kSELECT_ORG_LABEL = "Select Account.."
        case kSELECT_SITE_LABEL = "Select Site.."
        case kSELECT_NETWORK_LABEL = "Select Network.."
        case kSELECT_DEVICE_SPEC_LABEL = "Select Device Specification.."
        case kVIEW_DOCUMENTATION_LABEL = "View Documentation"
        case kNEXT_STEP_LABEL = "Next Step"
        case kINSTALL_DEVICE_STEP2_LABEL = "Step 2"
        case kINSTALL_DEVICE_STEP2_INSTRUCTION = "Scan the predefined QR Code on the label of the device:"
        case kINSTALL_DEVICE_SCAN_CODE_LABEL = "Scan Code"
        case kINSTALL_DEVICE_STEP3_LABEL = "Step 3"
        case kINSTALL_DEVICE_STEP3_INSTRUCTION = "Review device details: "
        case kINSTALL_DEVICE_STEP3_COMPLETE_INSTALLATION_LABEL = "Complete Installation"
        case kINSTALL_DEVICE_SUCCESSFULL_INSTALLATION_LABEL = "Device Sucessfully Installed"
        case kVIEW_DEVICES_LABEL = "View Devices"
        case kINSTALL_DEVICE_STEP3_INSTAL_NEW_DEVICE_LABEL = "Install Another"
        
        case kALERT_OK_LABEL = "OK"
        case kALERT_SCAN_AGAIN_LABEL = "Scan Again"
        case kERROR_ALERT_TITLE = "Error"
        case kBARCODE_SCAN_ALERT_ERROR_MES = "Reader not supported by the current device"
        case kINSTALLDEVICE_NO_DATA_ALERT_TITLE = "Install Device"
        case kINSTALLDEVICE_NO_SITES_MES = "No Sites available for the selection"
        case kINSTALLDEVICE_NO_ASSETGRPS_MES = "No Asset Groups available for the selection"
        case kINSTALLDEVICE_ERROR_SERVER_MES = "Please Try Again"
        case kSERVER_ERROR_MESSAGE = "Sorry, we are currently facing some issues.Please try again later."
        case kNO_ITERNET_CONNECTION_MESSAGE = "You are Offline. Please check your Internet Connection."
        case kINVALID_LOGIN_CREDENTIALS = "Invalid Credentials"
        case kINSTALLATION_FAILED_ALERT_TITLE = "Installation Failed"
        case kINSTALLATION_FAILED_ALERT_MESSAGE = "Please Install Again"
        case kALERT_REFRESH_BUTTON_TITLE = "Refresh"
        case kCANCEL = "Cancel"
        
        case kASSET_NAME_LABEL = "Sensor Name"
        case kASSET_LOC_LABEL = "Sensor Location"
        case kASSET_DES_LABEL = "Sensor Description"
        case kASSET_REMOVE_LABEL = "Remove Sensor"
        case kSAVE_LABEL = "Save"
        case kASSET_ID_LABEL = "Sensor ID"
        case kASSET_CODE_LABEL = "Sensor Code"
        
        case kGATEWAY_NAME_LABEL = "Gateway Name"
        case kGATEWAY_ID_LABEL = "Gateway ID"
        case kGATEWAY_CODE_LABEL = "Gateway Code"
        
        case kHEARTBEAT_INTERVAL_LABEL = "Heartbeat Interval"
        case kAWARESTATE_LABEL = "Aware State Heartbeat"
        case kMAKE_LABEL = "Make"
        case kMODEL_LABEL = "Model"
        case kSERIAL_NO_LABEL = "Serial Number"
        case kNOTES_LABEL = "Notes"
        case kOPTIONAL_LABEL = "optional"
        case kMINUTES_LABEL = "Minutes"
        case kCLOSE_LABEL = "Close"
        case kNETWORK_LABEL = "Network"
        
        case kHELP_LABEL = "Help"
        case kACCOUNT_SETTINGS_LABEL = "Account Settings"
        case kUSER_SETTINGS_LABEL = "User Settings"
        case kCREATE_ACCOUNT_LABEL = "Create Account"
        case kEDIT_ACCOUNT_LABEL = "Edit Account"
        case kCREATE_USER_LABEL = "Create User"
        case kEDIT_USER_LABEL = "Edit User"
        
        case kCOMPANY_NAME = "Company Name"
        case kACCOUNT_NUMBER = "Account Number"
        case kTIME_ZONE = "Time Zone"
        case kADDRESS = "Address"
        case kADDRESS_2 = "Address 2"
        case kCITY = "City"
        case kSTATE = "State"
        case kCOUNTRY = "Country"
        case kPOSTAL_CODE = "Postal Code"
        case kCOMPLETE_ACC_SETUP = "Proceed to setup User"
        case kSELECT = "Select .."
        case kFIRST_NAME = "First Name"
        case kLAST_NAME = "Last Name"
        case kEMAIL = "Email"
        case kPHONE = "Phone"
        case kSMS_CARRIER = "Mobile Carrier"
        case kUSER_ID = "User Id"
        case kPASSWORD = "Password"
        case kCONFIRM_PASSWORD = "Confirm Password"
        case kCOMPLETE_USER_SETUP = "Complete User Setup"
        
        case kACCOUNT_CREATION_INSTRUCTION = "Before installing sensors, you will need to create an account that will be associated with  your organization"
        case kUSER_CREATION_INSTRUCTION = "A user will now be created to access the account"
        case kINSTALL_DEVICE_INSTRUCTION_1A = "Please verify the box contents and lay out all the equipment on a table or flat surface "
        case kINSTALL_DEVICE_INSTRUCTION_1B = "The first device to be installed will be the gateway(s)"
        case kINSTALL_DEVICE_INSTRUCTION_2A = "We will now install sensors one by one. At this stage please do not power on the gateway or sensors"
        case kINSTALL_DEVICE_INSTRUCTION_3A = "Power the gateway and confirm that the three gateway lights are solid green"
        case kINSTALL_DEVICE_INSTRUCTION_3B = "Insert the batteries into all the sensors to power them on"
        case kINSTALL_DEVICE_INSTRUCTION_3C = "It may take upto 30 minutes from powering on, the gateways and sensors to communicate"
        case kINSTALL_DEVICE_INSTRUCTION_4A = "Once you have ascertained that the gateways and sensors are communicating, survey the location and choose the final installation location"
        case kINSTALL_DEVICE_INSTRUCTION_4B = "Then after installation, update the location for each sensor through the control screen"
        case kFROM_DATE_RANGE_IMPROPER = "Select a From date earlier than To date"
        case kTO_DATE_RANGE_IMPROPER = "Select a To date greater than From date"
        case kINCORRECT_DATE_RANGE = "Incorrect Date Range"
        case kFROM = "From"
        case kSESSION_EXPIRED_MES = "Your Session expired. Please login again to continue"
        case kERROR_SAVING_CHANGES = "Faild to save changes made"
        case kEDITING_FAILED = "Edit Failed"
        
        case kUSER_PASSWORD_EMPTY = "Username or Password field is Empty"
        case kSAVED_CHANGES_TO_ACC = "Succesfully saved changes to the Account"
        case kSAVE_USER_LABEL = "Save User"
        case kSAVE_ACC_LABEL = "Save Account"
        
        case kLOGIN_ID_VALIDATION_MES = "Ensure User Id is min 8 charcarters and contains atleast 1 alphabet and 1 number"
        case kPSWD_VALIDATION_MES = "Ensure Password is min 8 charcarters and contains atleast 1 alphabet and 1 number"
        case kEMAIL_VALIDATION_MES = "Enter a valid emailId"
        case kPHONE_VALIDATION_MES = "Enter a valid Phone number"
        case kPSWD_MATCH_VALIDATION_MES = "Passwords are not matching"
        
        case kBACK_TO_DASHBOARD = "Back To Dashboard"
    }
    
    enum OrganizationHierarchyMembers : String {
        case ParentOrganization = "ParentOrganization"
        case ChildOrganization = "ChildOrganization"
        case Site = "Site"
        case AssetGroup = "AssetGroup"
        case Asset = "Asset"
    }
    
    enum AssetMoreOptions : String{
        case CallHelpLine = "Call OEM Helpline"
        case ViewOwnershipDetails = "View Ownership Details"
        case Manuals = "Manuals"
    }
    
    enum ActivityIndicatorTexts: String {
        case FetchAssets = "Fetching Assets"
        case FetchAssetGroups = "Fetching Asset Groups"
        case FetchAlerts = "Fetching Alerts"
        case FetchOrganizations = "Fetching Organizations"
        case FetchManagedSites = "Fetching Managed Sites"
        case FetchNetworks = "Fetching Networks"
        case FetchDeviceSpec = "Fetching Device Specs"
        case CreateDevice = "Installing Device"
        case LoadingCollaboration = "Loading Collaboration"
        case Loading = "Loading. . ."
        case LoadingData = "Loading Data"
        case SavingChanges = "Saving Changes.."
        case CreatingUser  = "Creating User.."
        case SavingUser = "Saving User.."
        case SavingAccount = "Saving Account.."
        case AccountSetup = "Setting Account.."
        case NewUserLogin = "Logging in as"
        case NetworkSetUp = "Setting up Network.."
        case SaveDevice = "Saving Device.."
    }
    
    enum FilterDropDown: String{
        case GeoLocation = "geoLocationInfoIds"
        case Site = "managedSiteIds"
    }
    
    enum DropDownKeys: String {
        case Model = "ModelValue"
        case Valuefuntion = "ValueFunction"
        case InActiveImage = "Image"
        case ActiveImage = "Image_Active"
        case DisplayName = "DisplayName"
        case Tag = "Tag"
    }
    
    enum DisplayStyleOptions: String {
        case GRID = "GRID"
        case LIST = "LIST"
        case TREE = "TREE"
        case MAP = "MAP"
    }
    
    enum StatusCodes: Int {
        case Unauthorized = 401
        case Ok = 200
        case Created = 201
        case Accepted = 202
    }
    
    enum ImagesName: String {
        case LIST_VIEW = "ic_list_view.png"
        case LIST_VIEW_ACTIVE = "ic_list_view_active.png"
        case GRID_VIEW = "ic_tile_view.png"
        case GRID_VIEW_ACTIVE = "ic_tile_view_active.png"
        case TREE_VIEW = "ic_tree_view.png"
        case TREE_VIEW_ACTIVE = "ic_tree_view_active.png"
        case MAP_VIEW = "ic_map_view.png"
        case MAP_VIEW_ACTIVE = "ic_map_view_active.png"
        case FILTER_ACTIVE = "ic_filter_active.png"
        case FILTER_INACTIVE = "ic_filter_inactive.png"
        case BATTERY_STRENGTH_05 = "ic_battery_5.png"
        case BATTERY_STRENGTH_20 = "ic_battery_20.png"
        case BATTERY_STRENGTH_50 = "ic_battery_50.png"
        case BATTERY_STRENGTH_75 = "ic_battery_75.png"
        case BATTERY_STRENGTH_FULL = "ic_battery_full.png"
        case SIGNAL_STRENGTH_NONE = "ic_signal_none.png"
        case SIGNAL_STRENGTH_33 = "ic_signal_one.png"
        case SIGNAL_STRENGTH_66 = "ic_signal_two.png"
        case SIGNAL_STRENGTH_FULL = "ic_signal_full.png"
        case WIRED_SIGNAL_STRENGTH = "ic_signal_wired.png"
        case WIRED_BATTERY_STRENGTH = "ic_power_wired.png"
        case MAP_PIN_RED = "map_pin_red.png"
        case MAP_PIN_GREEN = "map_pin_green.png"
        case CALL_ICON = "ic_call.png"
        case MAIL_ICON = "ic_envelope.png"
        case SITE_ICON = "ic_filter_site.png"
        case NAV_BACK_ICON = "ic_back_nav.png"
        case DEFAULT_ASSET_ICON = "ic_tab_devices.png"
        case WARNING_ICON = "ic_warning.png"
        case SIDE_MENU = "ic_menu_nav.png"
        case WHITE_ADD_ICON = "ic_add.png"
        case WHITE_CLOSE_ICON = "ic_close_nav.png"
        case SUCCESS_ICON = "ic_install_complete.png"
        case NEXT_ICON = "button_next.png"
        case PREV_ICON = "button_prev.png"
        case DASHBOARD_MENU_ICON = "ic_drawer_dash.png"
        case NEW_MENU_ICON = "ic_drawer_new.png"
        case COLLAB_MENU_ICON = "ic_drawer_collab.png"
        case ACC_SETTINGS_MENU_ICON = "ic_drawer_settings.png"
        case USER_SETTINGS_MENU_ICON = "ic_user_settings.png"
        case HELP_MENU_ICON = "ic_drawer_help.png"
        case LOGOUT_MENU_ICON = "ic_drawer_logout.png"
    }
    
    static func getbaseUrl() -> String {return kBASE_URL}
    static func getGraphBaseUrl() ->  String {return kGRAPH_BASE_URL}
    
    static func getAssetGroupUrl() -> String {return kASSET_GROUPS_URL}
    static func getAssetsUrl() ->String {return kASSETS_URL}
    static func getAlertsUrl() ->String {return kALERTS_URL}
    static func getLoginUrl() -> String {return kLOGIN_URL}
    static func getLogoutUrl() -> String {return kLOGOUT_URL}
    static func getAssetsReadingUrl() -> String{return kASSETS_READING_URL}
    static func getGeoLocationsAndSitesUrl() -> String{return kGEOLOCATIONS_AND_SITES_LIST_URL}
    static func getFiteredAssetGroups() -> String{return kFILTERED_ASSET_GROUPS_URL}
    static func getRocketChatUrl() -> String{return kROCKET_CHAT_URL}
    
    static func getSubIdentityBasedOrgsFirstPart() -> String {return kSUB_IDENTITY_BASED_ORG_URL_FIRSTPART}
    static func getSubIdentityBasedOrgsLastPart() -> String {return kSUB_IDENTITY_BASED_ORG_URL_LASTPART}
    static func getParentOrgHierarchyFirstPart() -> String {return kPARENT_ORG_HIERARCHY_URL_FIRSTPART}
    static func getParentOrgHierarchyLastPart() -> String {return kPARENT_ORG_HIERARCHY_URL_LASTPART}
    static func getOrgBasedSitesUrl() -> String {return kORG_BASED_SITES_URL}
    static func getSiteBasedAssetGroups() -> String {return kSITE_BASED_ASSETGROUPS_URL}
    static func getAssetGroupBasedAssetsFirstPart() -> String {return kASSET_GROUP_BASED_ASSETS_URL_FIRSTPART}
    static func getAssetGroupBasedAssetsLastPart() -> String {return kASSET_GROUP_BASED_ASSETS_URL_LASTPART}
    static func getCreateSiteNetworkFirstPart() -> String {return kCREATE_SITE_NETWORK_URL_FIRSTPART}
    static func getCreateSiteNetworkSecondPart() -> String {return kCREATE_SITE_NETWORK_URL_SECONDPART}
    
    
    static func getOrganizationURL()-> String{return kORGANIZATION_URL}
    static func getOrganizationByIdURL()-> String{return kORGANIZATION_BY_ID_URL}
    static func getOrganizationByUUIdURL()-> String{return kORGANIZATION_BY_UUID_URL}
    
    
    static func getNotificationURL()-> String{return kNOTIFICATIONS_URL}
    static func getAcknowledgeNotificationURL()-> String{return kACKNOWLEDGE_NOTIFICATIONS_URL}
    
    static func getManagedSitesOrgIDURL()-> String{return kMANAGED_SITES_FOR_ORGID_URL}
    static func getAssetGrpSiteIDURL()-> String{return kMANAGED_ASSETGRP_FOR_SITEID_URL}
    static func getDeviceSpecificationURL()-> String{return kDEVICE_SPECIFICATION_URL}
    static func getCreateDeviceURL()-> String{return kCREATE_DEVICE_URL}
    static func getEditDeviceURL()-> String{return kCREATE_DEVICE_URL}
    static func getCreateEditUserURL() ->String{return kCREATE_USER_URL}
    static func getCreateOrgUserURL() -> String{return kCREATE_ORG_USER_URL}
    static func getEventGroupValuesURLFirstPart() -> String{return kGET_EVENT_GROUPS_URL_FIRSTPART}
    static func getEventGroupValuesURLLastPart() -> String{return kGET_EVENT_GROUPS_URL_LASTPART}
    
    static func getSortArray() ->Dictionary<String,String> {return kSORT_ARRAY}
    static func getFilterCheckboxDictionary() ->Dictionary<String,String> {return kFILTER_CHECKBOX}
    
    static func getLocationDropDownOptions() -> Array<String> {return kDROPDOWN_LOCATION_OPTIONS}
    static func getSiteDropDownOptions() -> Array<String> {return kDROPDOWN_SITE_OPTIONS}
    static func getTypeDropDownOptions() -> Array<String> {return kDROPDOWN_TYPE_OPTIONS}
    
    static func getNoneOptionValue() -> String {return kDROPDOWN_NONE_OPTION}
//    static func getFilterDropDownDetails()->Array<Dictionary<String,AnyObject>> {return kFILTER_DROPDOWN_DETAILS}
    static func getTrueConstant() ->String {return kTRUE}
    static func getFalseConstant() ->String {return kFALSE}
    static func getDashboardTitle() ->String {return kDASHBOARD_TITLE}
    static func getCollaborationTitle() ->String {return kCOLLABORATION_TITLE}
    static func getInstallDeviceTitle() ->String {return kINSTALLDEVICETITLE}
    static func getDomainForGrafana() ->String { return kGRAFANA_DOMAIN}
    static func getGrafanaKey() -> String {return kGRAFANA_API_KEY}
    
    static func getInstalledDeviceValidUUIDPrefix() -> String {return kINSTALLED_DEVICE_UUID_PREFIX}
    
    static func getUserRolesForUserAdmin() -> Array<String>{return kUSER_ROLES_FOR_USER_ADMIN}
//    static func getHelpScreenConfig() -> Array<Dictionary<String,AnyObject>>{return kHELP_SCREENS_CONFIG}
    
//    static func getValueForDropDownWithTag(controlTag: Int, key: String) -> String{
//        var ddValue: String = ""
//        for ddDefinition in Constants.getFilterDropDownDetails(){
//            if((ddDefinition["Tag"] as! Int) == controlTag){
//                ddValue = ddDefinition[key] as! String
//            }
//        }
//        return ddValue
//    }
//    
//    static func getValueForKeyWithKnowKeyValue(knownKey: String, knownValue: String, key: String) ->AnyObject{
//        var ddValue : AnyObject!
//        for ddDefinition in Constants.getFilterDropDownDetails(){
//            if((ddDefinition[knownKey] as! String) == knownValue){
//                if let value = ddDefinition[key] as? Int {
//                    ddValue = Int(value) as AnyObject!
//                }else{
//                    ddValue = ddDefinition[key] as! String as AnyObject!
//                }
//            }
//        }
//        return ddValue
//    }
    
}
