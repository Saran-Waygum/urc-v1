//
//  AlertsDetailViewController.swift
//  URC
//
//  Created by Saran Mahadevan on 08/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AlertsDetailViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var alert: Alert = Alert()
    
    
    @IBOutlet weak var alertNameLabel: UILabel!
    @IBOutlet weak var alertDescriptionLabel: UILabel!
    @IBOutlet weak var alertLocationLabel: UILabel!
    @IBOutlet weak var alertStatusLabel: UILabel!
    @IBOutlet weak var assetsCollectionView: UICollectionView!
    @IBOutlet weak var alertImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    @IBOutlet weak var alertHistoryLabel: UILabel!
    @IBOutlet weak var notesHistoryLabel: UILabel!
    
    @IBOutlet weak var workflowButton: UIButton!
    @IBOutlet weak var acknowledgeButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!
    
    @IBOutlet weak var rejectReasonAlertHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var rejectReasonAlertView: UIView!
    @IBOutlet weak var rejectReasonTextView: UITextView!
    
    
    var measurementsArray: Array<AssetMeasurementEvent> = []
    var notesDictionary : Dictionary<String, Int> = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadDataFromServer()
    }
    
    
    func updateUI()
    {
        self.alertNameLabel.text = alert.alertRule.alertRuleName
        
        self.title = "Alert Detail"
        
        alertNameLabel.font = UIFont(name: Styles.Style.boldStyleCustomFont, size: 13.0)
        alertStatusLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        alertDescriptionLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        alertLocationLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        
        alertHistoryLabel.font = UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 14.0)
        notesHistoryLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        
        
        if alert.state == "ACKNOWLEDGED"
        {
            alertDescriptionLabel.text = "Acknowledged " + alert.dateUpdated
            alertStatusLabel.text = "In Progress"
        }
        else if alert.state == "GENERATED"
        {
            alertDescriptionLabel.text = "Generated " + alert.dateUpdated
            alertStatusLabel.text = "Waiting for action"
        }
        
        
        
        if alert.alertLevel == "critical"
        {
            alertImageView.image = UIImage.init(named: "critical")
        }
        else if alert.alertLevel == "warning"
        {
            alertImageView.image = UIImage.init(named: "warning")
        }
        else
        {
            alertImageView.image = UIImage.init(named: "maintenance")
        }
        

        self.setupButton(button: workflowButton)
        self.setupButton(button: acknowledgeButton)
        self.setupButton(button: rejectButton)

        
        let silenceButton   = UIBarButtonItem(image: UIImage(named: "ic_alert_silenced")!,  style: .plain, target: self, action: #selector(AlertsDetailViewController.silenceAlertPressed))
        let collabButton = UIBarButtonItem(image: UIImage(named: "collab nav")!,  style: .plain, target: self, action: #selector(AlertsDetailViewController.silenceAlertPressed))
        
        navigationItem.rightBarButtonItems = [silenceButton, collabButton]
        setNavigationBarStyle()
    }
    
    func setNavigationBarStyle(){
        
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 16.0)!]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = Styles.Style.appNavigationBarColor
        
    }
    
    
    func setupButton(button: UIButton) {
        let spacing: CGFloat = 6.0
        let imageSize: CGSize = button.imageView!.image!.size
        button.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0)
        let labelString = NSString(string: button.titleLabel!.text!)
        let titleSize = labelString.size(attributes: [NSFontAttributeName: button.titleLabel!.font])
        button.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width)
        let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        button.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func loadDataFromServer()
    {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        let params = ["deviceId": "\(alert.managedDevice.id)", "isLatestReading": "true"]
        
        let alertsWebService = MeasurementEventsWebServiceManager()
        alertsWebService.loadMeasurementEvents(parameters: params, success: {
            (response) in
            if let response = response{
                
                self.measurementsArray = response
                self.assetsCollectionView.reloadData()
                
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
                self.loadNotesSummary()
                
            }
        }, failure:
            {
                (error,statusCode) in
                
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
                if(statusCode != 0){
                    print("")
                }
        })
    }
    
    
    // MARK: - CollectionView DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let assetCell : AlertsDetailCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlertsDetailCollectionViewCellID", for: indexPath as IndexPath) as! AlertsDetailCollectionViewCell
        
        if indexPath.section == 0
        {
            assetCell.configureAlert(asset: alert.managedAssetGroup, isAssetGroup: true, alert: alert)
        }
        else
        {
            assetCell.configureAlert(asset: alert.managedDevice, isAssetGroup: false, alert: alert)
            
//            let quantities: NSMutableAttributedString = NSMutableAttributedString()
//            let measurementValues: NSMutableAttributedString = NSMutableAttributedString()
//            
//            for index in 0...measurementsArray.count - 1
//            {
//                if index != 0
//                {
//                    let lineBreak = NSAttributedString(string: "\n", attributes: [NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)!])
//                    quantities.append(lineBreak)
//                    measurementValues.append(lineBreak)
//                }
//                
//                let alertAsset = NSAttributedString(string: measurementsArray[index].name + " ", attributes: [ NSForegroundColorAttributeName: UIColor.black , NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)!])
//                quantities.append(alertAsset)
//                
//                
//                if measurementsArray[index].alertLevel == "critical"
//                {
//                    
//                    let measurementValue = NSAttributedString(string: asset.assetMeasurements[index].measurementReading + " " + measurementsArray[index].unitSymbol, attributes: [ NSForegroundColorAttributeName: Styles.Style.criticalAlertColor , NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)!])
//                    measurementValues.append(measurementValue)
//                }
//                    
//                else if measurementsArray[index].alertLevel == "warning"
//                {
//                    
//                    let measurementValue = NSAttributedString(string: asset.assetMeasurements[index].measurementReading + " " + measurementsArray[index].unitSymbol, attributes: [ NSForegroundColorAttributeName: Styles.Style.warningAlertColor , NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)!])
//                    measurementValues.append(measurementValue)
//                    
//                }
//                else
//                {
//                    
//                    let measurementValue = NSAttributedString(string: asset.assetMeasurements[index].measurementReading + " " + measurementsArray[index].unitSymbol, attributes: [ NSForegroundColorAttributeName: Styles.Style.resolvedAlertColor , NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)!])
//                    measurementValues.append(measurementValue)
//                }
//                
//            }
//            
//            
//            let paragraphStyle = NSMutableParagraphStyle()
//            paragraphStyle.lineSpacing = 8
//            paragraphStyle.alignment = .right
//            measurementValues.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, measurementValues.length))
//            
//            
//            let paragraphStyleRight = NSMutableParagraphStyle()
//            paragraphStyleRight.lineSpacing = 8
//            paragraphStyleRight.alignment = .left
//            quantities.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyleRight, range:NSMakeRange(0, quantities.length))
//            
//            assetCell.measurementsLabel.attributedText = quantities
//            assetCell.measuredValuesLabel.attributedText = measurementValues

            
            
            
            if measurementsArray.count > 0
            {
                var quantities: String = ""
                var measurementValues: String = ""
                
                for measurement in measurementsArray
                {
                    if quantities != ""
                    {
                        quantities = quantities + "\n"
                    }
                    quantities = quantities.appending(measurement.displayData)
                    
                    if measurementValues != ""
                    {
                        measurementValues = measurementValues + "\n"
                    }
                    measurementValues = measurementValues + measurement.measurementReading
                    
                }
                
                let attributedMeasurementValues = NSMutableAttributedString.init(string: measurementValues)
                
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineSpacing = 8
                paragraphStyle.alignment = .right
                attributedMeasurementValues.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attributedMeasurementValues.length))
                
                
                let attributedQuantities = NSMutableAttributedString.init(string: quantities)
                let paragraphStyleRight = NSMutableParagraphStyle()
                paragraphStyleRight.lineSpacing = 8
                paragraphStyleRight.alignment = .left
                attributedQuantities.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyleRight, range:NSMakeRange(0, attributedQuantities.length))
                
                
                assetCell.measurementsLabel.attributedText = attributedQuantities
                assetCell.measuredValuesLabel.attributedText = attributedMeasurementValues
            }
            
            
            
        }
        
        
        
        return assetCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        let headerView  = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AlertDetailCollectionReusableViewID", for: indexPath) as! AlertDetailCollectionReusableView
        
        headerView.headerLabel.font = UIFont(name: Styles.Style.boldStyleCustomFont, size: 13.0)
        
        if indexPath.section == 0
        {
            headerView.headerLabel.text = "ASSETS"
        }
        else
        {
            headerView.headerLabel.text = "DEVICES"
        }
        
        return headerView
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 0
        {
            return CGSize.init(width: 384, height: 70)
        }
        else
        {
            return CGSize.init(width: 384, height: 120)
        }
        
    }
    
    
    @IBAction func acknowledgeAlertButtonPressed()
    {
        alert.state = "ACKNOWLEDGED"
        alert.updatedDate = Date() as NSDate
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d hh:mm a"
        alert.dateUpdated = formatter.string(from: Date())

    }
    
    func silenceAlertPressed()
    {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        let alertService = AlertsWebServiceManager()
        let params = ["alertId":"\(alert.alertId)"]
        
        alertService.silenceAlertNotification(parameters: params, success: {
            (response) in
            if response != nil{
                print("Silenced")
                
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
            }
        }, failure:
            {
                (error,statusCode) in
                
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
                if(statusCode != 0){
                    print("")
                }
        })
    }
    
    
    func loadNotesSummary()
    {
        let notesCall = NotesWebServiceManager()
        
        notesCall.loadNotes(alertId: 123, success: { (response) in
            
            if let response = response{
                
                self.notesDictionary = response
                
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                self.updateNotesData()
            }
        }, failure: {
            (error,statusCode) in
            
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
            
            if(statusCode != 0){
                print("")
            }
        })
        
    }
    
    func updateNotesData()
    {
        var notesSummary = ""
        
        if let docs = notesDictionary["document"]
        {
            notesSummary = notesSummary + "\(docs)" + "Documents, "
        }
        if let audio = notesDictionary["audio"]
        {
            notesSummary = notesSummary + "\(audio)" + "Audio Files, "
        }
        if let video = notesDictionary["video"]
        {
            notesSummary = notesSummary + "\(video)" + "Videos, "
        }
        if let picture = notesDictionary["picture"]
        {
            notesSummary = notesSummary + "\(picture)" + "Pictures"
        }
        
        self.notesHistoryLabel.text = notesSummary
    }
    
    @IBAction func alertHistoryPressed(_ sender: UIButton) {
        
        let vc : NotesListViewController = UIStoryboard(name: "UtilsStoryboard", bundle: nil).instantiateViewController(withIdentifier: "NotesListViewControllerID") as! NotesListViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func rejectAlertButtonPressed()
    {
        
    }
    
    @IBAction func rejectReasonSubmitButtonPressed(_ sender: UIButton) {
    }
    
}
