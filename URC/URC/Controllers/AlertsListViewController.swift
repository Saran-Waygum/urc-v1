//
//  AlertsListViewController.swift
//  URC
//
//  Created by Saran Mahadevan on 08/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AlertsListViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var alertsListCollectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var alertsArray : Array<Alert> = []
    var tempAlertsArray : Array<Alert> = []
    
    
    
    var isAllAlertsSelected: Bool = true
    var searchTextEntered: String = ""
    
    
    
    @IBOutlet weak var allAlertsButton: UIButton!
    @IBOutlet weak var myAlertsButton: UIButton!
    @IBOutlet weak var allAlertsView: UIView!
    @IBOutlet weak var myAlertsView: UIView!
    @IBOutlet weak var searchBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var filterCollectionView: UICollectionView!
    
    
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var filterOptionsView: UIView!
    @IBOutlet weak var alphaLayer: UIView!
    @IBOutlet weak var filterOptionsContainerView: UIView!
    @IBOutlet weak var filterOptionsViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var filterCriteriaSelectedLabel: UILabel!
    @IBOutlet weak var filterOptionsContainerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var filterOptionsTableView: UITableView!
    @IBOutlet weak var filterViewWidthConstraint: NSLayoutConstraint!
    
    
    var isFiltered : Bool = false
    var selectedState: String = ""
    var selectedFilterCriteriaTag: Int = 0
    
    let filterTypes: Array<String> = ["TIME","LOCATION","SITE","ASSET", "DEVICE"]
    
    let filterTypeActiveImages : Array<String> = ["ic_filter_time_active","ic_filter_location_active","ic_filter_site_active","ic_filter_type_active","ic_tab_devices_active"]
    let filterTypeInActiveImages : Array<String> = ["ic_filter_time","ic_filter_location","ic_filter_site","ic_filter_type","ic_tab_devices"]
    
    
    let durationsArray : Array<String> = ["All Alerts","Last 5 mins","Last 15 mins","Last 30 mins", "Last hour", "Last 8 hours", "Last 12 hours", "Last 24 hours", "Last week", "Last Month"]
    let durationInSecondsArray : Array<String> = ["1","300","900","1800","3600","28800","43200","86400","604800","2592000"]
    var typesArray : Array<String> = ["Critical", "Warning", "Maintanance"]
    var typeImagesArray : Array<String> = ["critical", "warning", "maintenance"]
    
    var locationsArray : Array<Location> = URCManager.sharedInstance.geolocations
    var sitesArray : Array<Site> = URCManager.sharedInstance.sites
    var assetGroupsArray: Array<AssetGroup> = []
    var assetsArray: Array<Asset> = []
    var tempAssetGroupsArray: Array<AssetGroup>=[]
    
    var selectedDuration : String = ""
    var selectedLocationsArray : Array<Location> = []
    var selectedSitesArray : Array<Site> = []
    var selectedAssetGroupsArray : Array<AssetGroup> = []
    var selectedDevicesArray : Array<Asset> = []
    var selectedTypesArray : Array<String> = []
    
    let alertTypes : Array<String> = ["GENERATED", "ACKNOWLEDGED", "CLOSED","REJECTED"]
    var selectedAlertTypes : Array<String> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
        setNavigationBarStyle()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        loadDataFromServer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateUI()
    {
        self.title = "Alerts"
        
        allAlertsView.backgroundColor = Styles.Style.tabSelectionColor
        myAlertsView.backgroundColor = UIColor.clear
        allAlertsButton.titleLabel?.textColor = UIColor.black
        myAlertsButton.titleLabel?.textColor = UIColor.lightGray
        self.view.layoutSubviews()
        
        self.navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(AlertsListViewController.searchAlerts)), animated: true)
        alphaLayer.backgroundColor = UIColor.clear.withAlphaComponent(0.8)
    }
    
    func searchAlerts()
    {
        if searchBarHeightConstraint.constant == 0.0
        {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                
                self.searchBarHeightConstraint.constant = 44.0
                
            }, completion: { (finished: Bool) -> Void in
                self.view.layoutSubviews()
            })
        }
        else
        {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                
                self.searchBarHeightConstraint.constant = 0.0
                
            }, completion: { (finished: Bool) -> Void in
                self.view.layoutSubviews()
            })
        }
    }
    
    func setNavigationBarStyle(){
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationController!.navigationBar.barTintColor = Styles.Style.appNavigationBarColor
        self.navigationController!.navigationBar.tintColor = Styles.Style.appNavigationContentColor
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        
    }
    
    func loadDataFromServer()
    {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
//        var params = ["alertStatus":"ACKNOWLEDGED,GENERATED"]
        
        var params = ["":""]
        
        
        let alertsWebService = AlertsWebServiceManager()
        
        if selectedDuration != "" && selectedDuration != "All Alerts"
        {
            let durationIndex = durationsArray.index(of: selectedDuration)!
            
            params["duration"] = durationInSecondsArray[durationIndex]
        }
        
        if selectedTypesArray.count > 0
        {
            var alertLevelCondition = "alertLevel="
            for type in selectedTypesArray
            {
                alertLevelCondition = alertLevelCondition + type
                alertLevelCondition = alertLevelCondition + ","
            }
            alertLevelCondition = alertLevelCondition.substring(to: alertLevelCondition.index(before: alertLevelCondition.endIndex))
            
            params["alertLevel"] = alertLevelCondition
            
        }
        
        if selectedAlertTypes.count > 0
        {
            var alertLevelCondition = "alertStatus="
            
            for type in selectedAlertTypes
            {
                alertLevelCondition = alertLevelCondition + type
                alertLevelCondition = alertLevelCondition + ","
            }
            alertLevelCondition = alertLevelCondition.substring(to: alertLevelCondition.index(before: alertLevelCondition.endIndex))
            
            params["alertStatus"] = alertLevelCondition
            
        }
        
        
        
        
        
        if selectedLocationsArray.count > 0 || selectedSitesArray.count > 0 || selectedAssetGroupsArray.count > 0 || selectedDevicesArray.count > 0
        {
            
            if selectedDevicesArray.count > 0
            {
                var deviceSpedIds = "deviceId="
                
                for asset in selectedDevicesArray
                {
                    deviceSpedIds = deviceSpedIds + "\(asset.id)"
                    deviceSpedIds = deviceSpedIds + ","
                }
                
                deviceSpedIds = deviceSpedIds.substring(to: deviceSpedIds.index(before: deviceSpedIds.endIndex))
                
                params["deviceId"] = deviceSpedIds

            }
            else
            {
                var assetGroupIds = "assetGroupId="
                
                if selectedAssetGroupsArray.count > 0
                {
                    for assetGroup in selectedAssetGroupsArray
                    {
                        assetGroupIds = assetGroupIds + "\(assetGroup.id)"
                        assetGroupIds = assetGroupIds + ","
                    }
                    
                    assetGroupIds = assetGroupIds.substring(to: assetGroupIds.index(before: assetGroupIds.endIndex))
                }
                else
                {
                    for assetGroup in assetGroupsArray
                    {
                        assetGroupIds = assetGroupIds + "\(assetGroup.id)"
                        assetGroupIds = assetGroupIds + ","
                    }
                    
                    assetGroupIds = assetGroupIds.substring(to: assetGroupIds.index(before: assetGroupIds.endIndex))
                    
                }
                
                params["assetGroupId"] = assetGroupIds
            }
        }
        
        
        alertsWebService.loadAlerts(parameters: params as [String : AnyObject]!, success: {
            (response) in
            if let response = response{
                
                self.alertsArray = response
                self.alertsListCollectionView.reloadData()
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                self.loadFilterParametersFromServer()
            }
        }, failure:
            {
                (error,statusCode) in
                if(statusCode != 0){
                    print("")
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                    
                }
        })
    }
    
    
    func loadFilterParametersFromServer()
    {
        let filterManager = FilterWebServiceManager()
        
        
//        if URCManager.sharedInstance.sites.count == 0
//        {
            DispatchQueue.global().async { [weak self] () -> Void in
                
                
                
                filterManager.fetchGeolocationAndSitesForFilter(success: {
                    (site) in
                    if(self != nil){
                        //                    self!.didFilterParamServiceSucceed = true
                        for(_,siteLoc) in (site?.enumerated())!{
                            if(URCManager.sharedInstance.geolocations.isEmpty){
                                URCManager.sharedInstance.geolocations.append(siteLoc.location)
                            }else{
                                var isExists = false
                                for(_,storedLoc) in URCManager.sharedInstance.geolocations.enumerated(){
                                    if(storedLoc.id == siteLoc.location.id){
                                        isExists = true
                                        break
                                    }
                                }
                                if(!isExists){
                                    URCManager.sharedInstance.geolocations.append(siteLoc.location)
                                }
                            }
                        }
                        URCManager.sharedInstance.sites = site!
                        
                        self?.sitesArray = URCManager.sharedInstance.sites
                        self?.locationsArray = URCManager.sharedInstance.geolocations
                        self?.loadAssetGroups()
                        
                    }
                    
                }, failure: {
                    (error,statusCode) in
                    print(error)
                    if(self != nil){
                        //                    self!.didFilterParamServiceSucceed = false
                        //                    self!.showErrorFromService(statusCode)
                    }
                })
            }
//        }
        
        
    }
    
    
    func loadAssetGroups()
    {
        if assetGroupsArray.count == 0
        {
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            
            let webAction = AssetGroupWebService()
            webAction.loadAssetGroups(success: {
                (response) in
                if let response = response{
                    
                    self.assetGroupsArray = response
                    self.tempAssetGroupsArray = response
                    
                    self.activityIndicator.isHidden = true
                    self.activityIndicator.stopAnimating()
                    
                }
            }, failure:
                {
                    (error,statusCode) in
                    if(statusCode != 0){
                        
                    }
                    self.activityIndicator.isHidden = true
                    self.activityIndicator.stopAnimating()
            })
        }
    }
    
    
    
    func loadDevicesForAssets()
    {

        
        let webAction = ManagedDevicesWebServiceManager()
        var assetGroupIds = "assetGrpId="
        var params = ["":""]
        
        if selectedAssetGroupsArray.count > 0
        {
            for assetGroup in selectedAssetGroupsArray
            {
                assetGroupIds = assetGroupIds + "\(assetGroup.id)"
                assetGroupIds = assetGroupIds + ","
            }
            
            assetGroupIds = assetGroupIds.substring(to: assetGroupIds.index(before: assetGroupIds.endIndex))
            
            params["assetGrpId"] = assetGroupIds
        
        }
        
        webAction.loadDevicesForAssets(parameters: params as [String : String]!, success: {
            (response) in
            if let response = response{
                
                self.assetsArray = response
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
            }
        }, failure: {
            (error,statusCode) in
            
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
        })
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil , action: nil )
        
        var index: IndexPath?
        if(segue.identifier == "AlertsDetailID"){
            if let cell = sender as? UICollectionViewCell{
                index = self.alertsListCollectionView.indexPath(for: cell)
                
            }else if let cell = sender as? UICollectionViewCell{
                index = self.alertsListCollectionView?.indexPath(for: cell)
            }
            
            let destinationVC = segue.destination as! AlertsDetailViewController
            destinationVC.alert = self.alertsArray[(index?.row)!]
            
        }
        
    }
    
    
    
    // MARK: - Sorting
    
    @IBAction func sortButtonPressed()
    {
        if sortButton.tag == 0 {
            let image = UIImage(named: "ic_sort_ascending")
            sortButton.setImage(image, for: .normal)
            sortButton.tag = 1
            self.alertsArray = (self.alertsArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "updatedDate", ascending: false)]) as! Array
        }
        else
        {
            let image = UIImage(named: "ic_sort_descending")
            sortButton.setImage(image, for: .normal)
            sortButton.tag = 0
            self.alertsArray = (self.alertsArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "updatedDate", ascending: true)]) as! Array
        }
        
        
        self.alertsListCollectionView.reloadData()
        
    }
    
    
    // MARK: - Filter
    
    @IBAction func filterButtonPressed()
    {
        
        DispatchQueue.main.async {
            
            if self.filterViewWidthConstraint.constant == 250.0
            {
                UIView.animate(withDuration: 2.0, delay: 0.0, options: UIViewAnimationOptions.transitionFlipFromRight, animations: { () -> Void in
                    
                    
                    self.filterViewWidthConstraint.constant = 0.0
                    
                }, completion: { (finished: Bool) -> Void in
                    
                    self.filterButton.setImage(UIImage(named: "ic_filter_inactive"), for: .normal)
                    self.view.layoutSubviews()
                })
            }
            else
            {
                UIView.animate(withDuration: 2.0, delay: 0.0, options: UIViewAnimationOptions.transitionFlipFromRight, animations: { () -> Void in
                    
                    
                    self.filterViewWidthConstraint.constant = 250.0
                    
                }, completion: { (finished: Bool) -> Void in
                    
                    self.filterButton.setImage(UIImage(named: "ic_filter_active"), for: .normal)
                    self.view.layoutSubviews()
                })
            }
        }
    }
    
    
    
    
    @IBAction func filterCriteriaSelected(_ sender: UIButton)
    {
        
        var height : Float = 200.0
        //        if sender.tag == 1
        //        {
        //            height = Float((locationsArray.count * 40) + 30)
        //        }
        //        else if sender.tag == 2
        //        {
        //            height = Float((sitesArray.count * 40) + 30)
        //        }
        //        else if sender.tag == 3
        //        {
        //            height = Float((typesArray.count * 40) + 30)
        //        }
        
        filterCriteriaSelectedLabel.text = filterTypes[sender.tag - 1]
        
        if filterOptionsViewHeightConstraint.constant  == 0.0
        {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                
                self.filterOptionsViewHeightConstraint.constant = 504.0
                self.filterOptionsContainerViewHeightConstraint.constant = CGFloat(height)
                
                
            }, completion: { (finished: Bool) -> Void in
                
                
                self.selectedFilterCriteriaTag = sender.tag
                self.filterOptionsTableView.reloadData()
                self.view.layoutSubviews()
            })
        }
        else
        {
            
            if selectedFilterCriteriaTag == sender.tag
            {
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                    
                    self.filterOptionsViewHeightConstraint.constant = 0.0
                    
                }, completion: { (finished: Bool) -> Void in
                    self.view.layoutSubviews()
                })
            }
            else
            {
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                    
                    self.filterOptionsViewHeightConstraint.constant = 500.0
                    
                }, completion: { (finished: Bool) -> Void in
                    
                    self.selectedFilterCriteriaTag = sender.tag
                    self.view.layoutSubviews()
                })
            }
            
        }
        
    }
    
    
    // MARK: - Switching Tabs
    
    @IBAction func allAlertsClicked()
    {
        allAlertsView.backgroundColor = Styles.Style.tabSelectionColor
        myAlertsView.backgroundColor = UIColor.clear
        isAllAlertsSelected = true
        self.alertsListCollectionView.reloadData()
        allAlertsButton.titleLabel?.textColor = UIColor.black
        myAlertsButton.titleLabel?.textColor = UIColor.lightGray
        self.view.layoutSubviews()
    }
    
    
    @IBAction func myAlertsClicked()
    {
        myAlertsView.backgroundColor = Styles.Style.tabSelectionColor
        allAlertsView.backgroundColor = UIColor.clear
        isAllAlertsSelected = false
        self.alertsListCollectionView.reloadData()
        myAlertsButton.titleLabel?.textColor = UIColor.black
        allAlertsButton.titleLabel?.textColor = UIColor.lightGray
        self.view.layoutSubviews()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        
        if collectionView == filterCollectionView{
            
            return 7
            
        }
        else
        {
            return 1
        }
    }
    
    // MARK: - CollectionView DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == filterCollectionView
        {
            if section == 0
            {
                if selectedDuration == ""
                {
                    return 0
                }
                else
                {
                    return 1
                }
                
            }
            else if section == 1
            {
                return selectedSitesArray.count
            }
            else if section == 2
            {
                return selectedLocationsArray.count
            }
            else if section == 3
            {
                return selectedAssetGroupsArray.count
            }
            else if section == 4
            {
                return selectedDevicesArray.count
            }
            else if section == 5
            {
                return typesArray.count
            }
            else
            {
                return alertTypes.count
            }
        }
        else
        {
            return alertsArray.count
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        
        if collectionView == filterCollectionView
        {
            return CGSize(width: collectionView.frame.size.width, height: 30)
        }
        else
        {
            let alert = self.alertsArray[indexPath.row]
            return CGSize(width: collectionView.frame.size.width, height: AlertsListCollectionViewCell.heightForItem(alert: alert))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == filterCollectionView
        {
            let filterCell : AlertsFilterCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlertsFilterCollectionViewCellID", for: indexPath as IndexPath) as! AlertsFilterCollectionViewCell
            
            if indexPath.section == 0
            {
                filterCell.filterCriteriaNameLabel.text = selectedDuration
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.checkBoxWidth.constant = 0
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.removeFilterCriteriaButtonWidth.constant = 15
                filterCell.filterCriteriaNameLabel.textColor = UIColor.black
            }
            else if indexPath.section == 1
            {
                filterCell.filterCriteriaNameLabel.text = selectedSitesArray[indexPath.row].siteName
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.checkBoxWidth.constant = 0
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.removeFilterCriteriaButtonWidth.constant = 15
                filterCell.filterCriteriaNameLabel.textColor = UIColor.black
            }
            else if indexPath.section == 2
            {
                filterCell.filterCriteriaNameLabel.text = selectedLocationsArray[indexPath.row].name
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.checkBoxWidth.constant = 0
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.removeFilterCriteriaButtonWidth.constant = 15
                filterCell.filterCriteriaNameLabel.textColor = UIColor.black
            }
            else if indexPath.section == 3
            {
                filterCell.filterCriteriaNameLabel.text = selectedAssetGroupsArray[indexPath.row].name
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.checkBoxWidth.constant = 0
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.removeFilterCriteriaButtonWidth.constant = 15
                filterCell.filterCriteriaNameLabel.textColor = UIColor.black
            }
            else if indexPath.section == 4
            {
                filterCell.filterCriteriaNameLabel.text = selectedDevicesArray[indexPath.row].name
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.checkBoxWidth.constant = 0
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.removeFilterCriteriaButtonWidth.constant = 15
                filterCell.filterCriteriaNameLabel.textColor = UIColor.black
            }
            else if indexPath.section == 5
            {
                filterCell.filterCriteriaNameLabel.text = typesArray[indexPath.row]
                filterCell.filterCriteriaImageView.image = UIImage.init(named: typeImagesArray[indexPath.row])
                filterCell.checkBoxWidth.constant = 15
                filterCell.filterCriteriaImageViewWidth.constant = 20
                filterCell.removeFilterCriteriaButtonWidth.constant = 0
                
                if selectedTypesArray.contains(typesArray[indexPath.row])
                {
                    filterCell.checkBoxButton.setImage(UIImage.init(named: "checkbox_square_active"), for: .normal)
                    filterCell.filterCriteriaNameLabel.textColor = UIColor.black
                }
                else
                {
                    filterCell.checkBoxButton.setImage(UIImage.init(named: "checkbox_square_unactive"), for: .normal)
                    filterCell.filterCriteriaNameLabel.textColor = UIColor.gray
                }
                
            }
            else
            {
                filterCell.filterCriteriaNameLabel.text = alertTypes[indexPath.row].camelCase()
                filterCell.filterCriteriaImageViewWidth.constant = 0
                filterCell.checkBoxWidth.constant = 15
                filterCell.removeFilterCriteriaButtonWidth.constant = 0
                
                if selectedAlertTypes.contains(alertTypes[indexPath.row])
                {
                    filterCell.checkBoxButton.setImage(UIImage.init(named: "checkbox_square_active"), for: .normal)
                    filterCell.filterCriteriaNameLabel.textColor = UIColor.black
                }
                else
                {
                    filterCell.checkBoxButton.setImage(UIImage.init(named: "checkbox_square_unactive"), for: .normal)
                    filterCell.filterCriteriaNameLabel.textColor = UIColor.gray
                }
                
            }
            
            
            filterCell.tapAction = { (cell) in
                self.removeFilterCondition(indexPath: collectionView.indexPath(for: cell)!)
            }
            filterCell.layoutSubviews()
            
            filterCell.filterCriteriaNameLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)
            return filterCell
        }
        else
        {
            let alertCell : AlertsListCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlertsListCollectionViewCellID", for: indexPath as IndexPath) as! AlertsListCollectionViewCell
            
            let alert = alertsArray[indexPath.row]
            alertCell.configureAlert(alert: alert)
            
            if searchTextEntered.characters.count != 0
            {
                self.highlightSearchStringforLabel(label: alertCell.alertNameLabel)
            }
            
            return alertCell
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        
        if indexPath.section < 5
        {
            let headerView  = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AlertsFilterCollectionReusableViewID", for: indexPath) as! AlertsFilterCollectionReusableView
            
            headerView.filterOptionsButton.setTitle(filterTypes[indexPath.section], for: .normal)
            
            
            headerView.filterOptionsButton.tag = indexPath.section + 1
            
            headerView.filterOptionsButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0)
            headerView.filterOptionsButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0)
            headerView.filterOptionsButton.addTarget(self, action: #selector(AssetsListViewController.filterTypeSelected(sender:)), for: UIControlEvents.touchUpInside)
            
            
            
            if ((indexPath.section == 0 && selectedDuration != "") || (indexPath.section == 1 && selectedSitesArray.count > 0) || (indexPath.section == 2 && selectedLocationsArray.count > 0) || (indexPath.section == 3 && selectedAssetGroupsArray.count > 0) || (indexPath.section == 4 && selectedDevicesArray.count > 0))
            {
                headerView.filterOptionsButton.setImage(UIImage.init(named: filterTypeActiveImages[indexPath.section]), for: .normal)
                headerView.filterOptionsButton.setTitleColor(UIColor.black, for: .normal)
                headerView.filterOptionsButton.titleLabel?.font = UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)
            }
            else
            {
                headerView.filterOptionsButton.setImage(UIImage.init(named: filterTypeInActiveImages[indexPath.section]), for: .normal)
                
                headerView.filterOptionsButton.setTitleColor(UIColor.gray, for: .normal)
                headerView.filterOptionsButton.titleLabel?.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)
            }
            
            
            headerView.sectionSeparatorViewHeightConstraint.constant = 0
            headerView.filterOptionsExpandImage.isHidden = false
            
            return headerView
            
        }
        else
        {
            let headerView  = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AlertsFilterCollectionReusableViewID", for: indexPath) as! AlertsFilterCollectionReusableView
            
            headerView.sectionSeparatorViewHeightConstraint.constant = 1
            
            headerView.filterOptionsButton.setTitle("", for: .normal)
            headerView.filterOptionsButton.setImage(nil, for: .normal)
            
            headerView.filterOptionsExpandImage.isHidden = true
            
            return headerView
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        if collectionView == filterCollectionView
        {
            if (section < 5)
            {
                return CGSize.init(width: collectionView.frame.size.width, height: 40)
            }
            else
            {
                return CGSize.init(width: collectionView.frame.size.width, height: 20)
            }
            
        }
        else
        {
            return CGSize.zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        
        if collectionView == filterCollectionView
        {
            if indexPath.section == 5
            {
                if selectedTypesArray.contains(typesArray[indexPath.row])
                {
                    if let index = selectedTypesArray.index(of: typesArray[indexPath.row]) {
                        selectedTypesArray.remove(at: index)
                    }
                }
                else
                {
                    selectedTypesArray.append(typesArray[indexPath.row])
                }
                
                collectionView.reloadItems(at: [indexPath])
                
            }
            else if indexPath.section == 6
            {
                if selectedAlertTypes.contains(alertTypes[indexPath.row])
                {
                    if let index = selectedAlertTypes.index(of: alertTypes[indexPath.row]) {
                        selectedAlertTypes.remove(at: index)
                    }
                }
                else
                {
                    selectedAlertTypes.append(alertTypes[indexPath.row])
                }
                
                collectionView.reloadItems(at: [indexPath])
            }
        }
        loadDataFromServer()
        
    }
    
    
    func removeFilterCondition(indexPath: IndexPath)
    {
        
        if indexPath.section == 0
        {
            selectedDuration = ""
        }
        else if indexPath.section == 1
        {
            selectedSitesArray.remove(at: indexPath.row)
        }
        else if indexPath.section == 2
        {
            selectedLocationsArray.remove(at: indexPath.row)
        }
        else if indexPath.section == 3
        {
            selectedAssetGroupsArray.remove(at: indexPath.row)
        }
        else if indexPath.section == 4
        {
            selectedDevicesArray.remove(at: indexPath.row)
        }
        
        filterCollectionView.reloadData()
        loadDataFromServer()
        
    }
    
    
    // MARK: - Search Delegates
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
        
        if searchBarHeightConstraint.constant > 0.0
        {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                
                self.searchBarHeightConstraint.constant = 0.0
                
            }, completion: { (finished: Bool) -> Void in
                self.searchTextEntered = ""
                self.alertsArray = self.tempAlertsArray
                self.alertsListCollectionView.reloadData()
                self.view.layoutSubviews()
            })
        }
        
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        
        searchTextEntered = searchText
        filterAssetGroupsForSearchString(searchText: searchText)
        print(searchText)
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool
    {
        tempAlertsArray = alertsArray
        return true
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool
    {
        searchTextEntered = ""
        alertsArray = tempAlertsArray
        self.alertsListCollectionView.reloadData()
        return true
    }
    
    func filterAssetGroupsForSearchString(searchText: String)
    {
        if searchText == ""
        {
            alertsArray = tempAlertsArray
        }
        else
        {
            let predicate = NSPredicate(format: "alertRule.alertRuleName contains[c] %@", searchText)
            alertsArray = (tempAlertsArray as NSArray).filtered(using: predicate) as! Array<Alert>
        }
        
        self.alertsListCollectionView.reloadData()
    }
    
    func highlightSearchStringforLabel(label: UILabel)
    {
        
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: label.text!)
        let pattern = searchTextEntered.lowercased()
        let range: NSRange = NSMakeRange(0, label.text!.characters.count)
        
        let regex = try! NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options())
        
        regex.enumerateMatches(in: label.text!.lowercased(), options: NSRegularExpression.MatchingOptions(), range: range) { (textCheckingResult, matchingFlags, stop) -> Void in
            let subRange = textCheckingResult?.range
            attributedString.addAttribute(NSBackgroundColorAttributeName, value: UIColor.yellow, range: subRange!)
        }
        
        label.attributedText = attributedString
        
        
    }
    
    // MARK: - TableView Data Source
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        switch selectedFilterCriteriaTag {
        case 1:
            return durationsArray.count
            
        case 2:
            return sitesArray.count
            
        case 3:
            return locationsArray.count
            
        case 4:
            return assetGroupsArray.count
            
        case 5:
            return assetsArray.count
            
        default:
            break
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let filterCell : FilterTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCellID", for: indexPath as IndexPath) as! FilterTableViewCell
        
        switch selectedFilterCriteriaTag {
        case 1:
            filterCell.filterCriteriaValueLabel.text = durationsArray[indexPath.row]
            
            if selectedDuration == durationsArray[indexPath.row]
            {
                filterCell.isSelectedImageView.image = UIImage(named: "ic_alert_check")
            }
            else
            {
                filterCell.isSelectedImageView.image = nil
            }
            break
        case 2:
            filterCell.filterCriteriaValueLabel.text = sitesArray[indexPath.row].siteName
            
            if selectedSitesArray.contains(sitesArray[indexPath.row])
            {
                filterCell.isSelectedImageView.image = UIImage(named: "ic_alert_check")
            }
            else
            {
                filterCell.isSelectedImageView.image = nil
            }
            break
        case 3:
            filterCell.filterCriteriaValueLabel.text = locationsArray[indexPath.row].name
            
            if selectedLocationsArray.contains(locationsArray[indexPath.row])
            {
                filterCell.isSelectedImageView.image = UIImage(named: "ic_alert_check")
            }
            else
            {
                filterCell.isSelectedImageView.image = nil
            }
            break
        case 4:
            filterCell.filterCriteriaValueLabel.text = assetGroupsArray[indexPath.row].name
            
            if selectedAssetGroupsArray.contains(assetGroupsArray[indexPath.row])
            {
                filterCell.isSelectedImageView.image = UIImage(named: "ic_alert_check")
            }
            else
            {
                filterCell.isSelectedImageView.image = nil
            }
            break
        case 5:
            filterCell.filterCriteriaValueLabel.text = assetsArray[indexPath.row].name
            
            if selectedDevicesArray.contains(assetsArray[indexPath.row])
            {
                filterCell.isSelectedImageView.image = UIImage(named: "ic_alert_check")
            }
            else
            {
                filterCell.isSelectedImageView.image = nil
            }
            break
        default:
            break
        }
        
        
        
        return filterCell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        switch selectedFilterCriteriaTag {
        case 1:
            if selectedDuration == durationsArray[indexPath.row]
            {
                
                selectedDuration = ""
                self.filterOptionsTableView.reloadData()
            }
            else
            {
                selectedDuration = durationsArray[indexPath.row]
                self.filterOptionsTableView.reloadData()
            }
            
        case 2:
            if selectedSitesArray.contains(sitesArray[indexPath.row])
            {
                
                if let index = selectedSitesArray.index(of: sitesArray[indexPath.row]) {
                    selectedSitesArray.remove(at: index)
                }
                self.filterOptionsTableView.reloadData()
            }
            else
            {
                selectedSitesArray.append(sitesArray[indexPath.row])
                self.filterOptionsTableView.reloadData()
            }
            
        case 3:
            if selectedLocationsArray.contains(locationsArray[indexPath.row])
            {
                
                if let index = selectedLocationsArray.index(of: locationsArray[indexPath.row]) {
                    selectedLocationsArray.remove(at: index)
                }
                self.filterOptionsTableView.reloadData()
            }
            else
            {
                selectedLocationsArray.append(locationsArray[indexPath.row])
                self.filterOptionsTableView.reloadData()
            }
            
        case 4:
            if selectedAssetGroupsArray.contains(assetGroupsArray[indexPath.row])
            {
                
                if let index = selectedAssetGroupsArray.index(of: assetGroupsArray[indexPath.row]) {
                    selectedAssetGroupsArray.remove(at: index)
                }
                self.filterOptionsTableView.reloadData()
            }
            else
            {
                selectedAssetGroupsArray.append(assetGroupsArray[indexPath.row])
                self.filterOptionsTableView.reloadData()
            }
            
        case 5:
            if selectedDevicesArray.contains(assetsArray[indexPath.row])
            {
                
                if let index = selectedDevicesArray.index(of: assetsArray[indexPath.row]) {
                    selectedDevicesArray.remove(at: index)
                }
                self.filterOptionsTableView.reloadData()
            }
            else
            {
                selectedDevicesArray.append(assetsArray[indexPath.row])
                self.filterOptionsTableView.reloadData()
            }
            
            
        default:
            break
        }
        
        
        updateFilterViewLabels()
        
    }
    
    
    func updateFilterViewLabels()
    {
        //        if selectedDurationIndexes.count > 0{
        //            let indexpath = selectedDurationIndexes
        //            self.filterDurationlabel.text = String(format: durationsArray[(indexpath?.row)!])
        //        }else{
        //            self.filterDurationlabel.text = String(format: durationsArray[(indexpath?.row)!])
        //        }
        
        //        self.filterDurationlabel.text = String(format: durationsArray[(selectedDurationIndexes.row)])
        //
        //        if selectedSiteIndexes.count > 0{
        //            self.filterSiteLabel.text = String(format: "%d Sites selected", selectedSiteIndexes.count)
        //        }else{
        //            self.filterSiteLabel.text = "Filter by Site"
        //        }
        //
        //        if selectedLocationIndexes.count > 0{
        //            self.filterLocationlabel.text = String(format: "%d Locations selected", selectedLocationIndexes.count)
        //        }else{
        //            self.filterLocationlabel.text = "Filter by Location"
        //        }
        //
        //
        //
        //        if selectedAssetGroupIndexes.count > 0{
        //            self.filterAssetGroupsLabel.text = String(format: "%d Asset Groups selected", selectedAssetGroupIndexes.count)
        //        }else{
        //            self.filterAssetGroupsLabel.text = "Filter by Asset Group"
        //        }
        //
        //        if selectedDevicesIndexes.count > 0{
        //            self.filterDevicesLabel.text = String(format: "%d Devices selected", selectedDevicesIndexes.count)
        //        }else{
        //            self.filterDevicesLabel.text = "Filter by Devices"
        //        }
        //
        //        self.view.layoutSubviews()
        
    }
    
    
    func filterTypeSelected(sender: UIButton!)
    {
        var height : Float = 0.0
        if sender.tag == 1
        {
            height = Float((durationsArray.count * 40) + 30)
        }
        else if sender.tag == 2
        {
            height = Float((locationsArray.count * 40) + 30)
        }
        else if sender.tag == 3
        {
            height = Float((sitesArray.count * 40) + 30)
        }
        else if sender.tag == 4
        {
            height = min(Float((assetGroupsArray.count * 40) + 30), 300)
        }
        else if sender.tag == 5
        {
            height = min(Float((assetsArray.count * 40) + 30), 300)
        }
        
        filterCriteriaSelectedLabel.text = filterTypes[sender.tag - 1]
        
        
        if filterOptionsViewHeightConstraint.constant  == 0.0
        {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                
                self.filterOptionsViewHeightConstraint.constant = 509.0
                
                
            }, completion: { (finished: Bool) -> Void in
                
                self.filterOptionsContainerViewHeightConstraint.constant = CGFloat(height)
                self.selectedFilterCriteriaTag = sender.tag
                self.filterOptionsTableView.reloadData()
                self.view.layoutSubviews()
            })
        }
        else
        {
            
            if selectedFilterCriteriaTag == sender.tag
            {
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                    
                    self.filterOptionsViewHeightConstraint.constant = 0.0
                    
                }, completion: { (finished: Bool) -> Void in
                    self.view.layoutSubviews()
                })
            }
            else
            {
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                    
                    self.filterOptionsViewHeightConstraint.constant = 500.0
                    
                }, completion: { (finished: Bool) -> Void in
                    
                    self.selectedFilterCriteriaTag = sender.tag
                    self.view.layoutSubviews()
                })
            }
            
        }
        
    }

    
    func filterAlertsFromServer()
    {
        
    }
    
    func filterAssetsForSelectedConstraints()
    {
        
        var locationPredicate: NSPredicate?
        var sitesPredicate: NSPredicate?
        var filterPredicate: NSPredicate = NSPredicate(format: "location != %@","")
        
        for location in selectedLocationsArray
        {
            if locationPredicate != nil
            {
                locationPredicate = NSCompoundPredicate.init(type: .or, subpredicates: [locationPredicate!,NSPredicate(format: "location == %@", location.name)])
            }
            else
            {
                locationPredicate = NSPredicate(format: "location == %@", location.name)
            }
        }
        
        if locationPredicate != nil
        {
            filterPredicate = NSCompoundPredicate.init(type: .and, subpredicates: [filterPredicate, locationPredicate!])
        }
        
        
        
        for site in selectedSitesArray
        {
            if sitesPredicate != nil
            {
                sitesPredicate = NSCompoundPredicate.init(type: .or, subpredicates: [sitesPredicate!, NSPredicate(format: "siteId == %d", site.siteId)])
            }
            else
            {
                sitesPredicate = NSPredicate(format: "siteId == %d", site.siteId)
            }
        }
        
        if sitesPredicate != nil
        {
            filterPredicate = NSCompoundPredicate.init(type: .and, subpredicates: [filterPredicate, sitesPredicate!])
        }
        
        
        
        assetGroupsArray = (tempAssetGroupsArray as NSArray).filtered(using: filterPredicate) as! Array<AssetGroup>
        
        self.filterCollectionView.reloadData()
    }
    
    
    
    @IBAction func doneButtonPressed(_ sender: UIButton)
    {
        
        if selectedFilterCriteriaTag == 3
        {
            filterAssetsForSelectedConstraints()
        }
        if selectedFilterCriteriaTag == 4
        {
            loadDevicesForAssets()
        }
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
            
            self.filterOptionsViewHeightConstraint.constant = 0.0
            
        }, completion: { (finished: Bool) -> Void in
            self.view.layoutSubviews()
            self.loadDataFromServer()
            self.filterAssetsForSelectedConstraints()
        })
        
    }
    
    @IBAction func resetButtonPressed(_ sender: UIButton) {
        
        selectedDuration = ""
        selectedLocationsArray = []
        selectedSitesArray = []
        selectedAssetGroupsArray = []
        selectedDevicesArray = []
        selectedTypesArray = []
        selectedAlertTypes = []
        
        filterCollectionView.reloadData()
        
        
    }
    
    
    
}
