//
//  AssetDetailsViewController.swift
//  URC
//
//  Created by Saran Mahadevan on 05/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AssetDetailsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIWebViewDelegate {
    
    
    var asset: Asset?
    var assetGroup: AssetGroup?
    
    @IBOutlet weak var assetImageView: UIImageView!
    @IBOutlet weak var assetNameLabel: UILabel!
    @IBOutlet weak var assetDescriptionLabel: UILabel!
    @IBOutlet weak var assetLocationLabel: UILabel!
    @IBOutlet weak var criticalAlertsCountLabel: UILabel!
    @IBOutlet weak var warningAlertsCountLabel: UILabel!
    @IBOutlet weak var maintanenceAlertCountLabel: UILabel!
    
    @IBOutlet weak var criticalAlertsCountLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var warningCountLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var maintanenceAlertsCountLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var alertCountContainerWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var assetDescHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lastUpdatedTimeLabel: UILabel!
    @IBOutlet weak var lastUpdatedValue: UILabel!
    
    @IBOutlet weak var cardOptionsCollectionView: UICollectionView!
    @IBOutlet weak var deviceparametersCollectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var graphContainerView: UIView!
    
    var graphURLs : Array<String> = ["http://indiadevmisc.cloudapp.net:3001/dashboard/db/temperature-graph?panelId=1&fullscreen&mobile=true",
                                     "http://indiadevmisc.cloudapp.net:3001/dashboard/db/temperature-graph?panelId=2&fullscreen&mobile=true",
                                     "http://indiadevmisc.cloudapp.net:3001/dashboard/db/temperature-graph?panelId=4&fullscreen&mobile=true"]
    
    
    var selectedParameter : AssetSpecMeasurementQuantity = AssetSpecMeasurementQuantity.init()
    var selectedParameterIndex : Int = 0
    var measurementsArray: Array<AssetMeasurementEvent> = []
//    var measurementsArray: Array<AssetMeasurementEvent> = []
    
    var cardOptionNames = ["Location","Alerts","Data","Control", "O&M", "Safety"]
    var cardOptionImages = ["ic_filter_location_active","ic_asset_alerts","ic_asset_location","ic_asset_data","ic_asset_oam","ic_asset_safety"]
    var isFavorite: Bool = false
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        
        lastUpdatedValue.text = (asset?.assetMeasurements[0].measurementReading)! + " " + (asset?.assetSpec.measuredQuantities[0].unitSymbol)!
        
        lastUpdatedTimeLabel.text = asset?.assetMeasurements[0].eventTime.formattedURCLocaleDateString
        
        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = Styles.Style.backgroundGrayColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        loadDataFromServer()
        updateUI()
        updateFavoritesButton()
    }
    
    func updateUI()
    {
        
        assetImageView.layer.cornerRadius = 35
        
        self.title = self.asset?.name
        
        if let data = NSData(contentsOf: asset?.assetSpec.displayImageRef as! URL) {
            self.assetImageView.image = UIImage(data: data as Data)
        }
        
        
        self.lastUpdatedTimeLabel.font = UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 14.0)
        self.lastUpdatedValue.font = UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 14.0)
        self.lastUpdatedValue.textColor = Styles.Style.criticalAlertColor
        
        
        self.assetNameLabel.font = UIFont(name: Styles.Style.boldStyleCustomFont, size: 14.0)
        self.assetDescriptionLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)
        self.assetLocationLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        
        assetNameLabel.text = asset?.name
        assetLocationLabel.text = assetGroup?.location
        assetDescriptionLabel.text = asset?.deviceDesc
        
        self.assetDescHeightConstraint.constant = heightForLabel(constraintedWidth: assetLocationLabel.frame.size.width, font: UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)!, text: self.assetDescriptionLabel.text!)

        
        let button1 = UIBarButtonItem(image: UIImage(named: "ic_favourite_nav_tablet"), style: .plain, target: self, action: #selector(AssetGroupDetailsViewController.action))
        self.navigationItem.rightBarButtonItem  = button1
        
        selectedParameter = (asset?.assetSpec.measuredQuantities.first)!
        
        deviceparametersCollectionView.reloadData()
        
        updateAlertLabels()
        
    }
    
    

    
    func updateFavoritesButton()
    {
        if URCManager.sharedInstance.favoriteAssets.contains(asset!)
        {
            let button1 = UIBarButtonItem(image: UIImage(named: "ic_favourite_active_nav_tablet"), style: .plain, target: self, action: #selector(AssetGroupDetailsViewController.action))
            self.navigationItem.rightBarButtonItem  = button1
            isFavorite = true
        }
        else
        {
            let button1 = UIBarButtonItem(image: UIImage(named: "ic_favourite_nav_tablet"), style: .plain, target: self, action: #selector(AssetGroupDetailsViewController.action))
            self.navigationItem.rightBarButtonItem  = button1
            isFavorite = false
            
        }
        
    }
    
    func action()
    {
        if !isFavorite
        {
            
            let button1 = UIBarButtonItem(image: UIImage(named: "ic_favourite_active_nav_tablet"), style: .plain, target: self, action: #selector(AssetGroupDetailsViewController.action))
            self.navigationItem.rightBarButtonItem  = button1
            isFavorite = true
            URCManager.sharedInstance.favoriteAssetGroups.append(self.assetGroup!)
        }
        else
        {
            let button1 = UIBarButtonItem(image: UIImage(named: "ic_favourite_nav_tablet"), style: .plain, target: self, action: #selector(AssetGroupDetailsViewController.action))
            self.navigationItem.rightBarButtonItem  = button1
            isFavorite = false
            
            let index = URCManager.sharedInstance.favoriteAssetGroups.index(of: assetGroup!)
            URCManager.sharedInstance.favoriteAssetGroups.remove(at: index!)
        }
        
    }
    
    func updateAlertLabels()
    {
        self.criticalAlertsCountLabel.layer.cornerRadius = 10
        self.warningAlertsCountLabel.layer.cornerRadius = 10
        self.maintanenceAlertCountLabel.layer.cornerRadius = 10
        self.assetImageView.layer.cornerRadius = 35
        
        
        
        if asset?.criticalAlertCount != 0
        {
            self.criticalAlertsCountLabel.text = String.init(format: "%d", (asset?.criticalAlertCount)!)
            self.criticalAlertsCountLabelWidthConstraint.constant = 20.0
        }
        else
        {
            self.criticalAlertsCountLabelWidthConstraint.constant = 0.0
        }
        
        
        if asset?.warningAlertCount != 0
        {
            self.warningAlertsCountLabel.text = String.init(format: "%d", (asset?.warningAlertCount)!)
            self.warningCountLabelWidthConstraint.constant = 20.0
        }
        else
        {
            self.warningCountLabelWidthConstraint.constant = 0.0
        }
        
        if asset?.maintenanceAlertCount != 0
        {
            self.maintanenceAlertCountLabel.text = String.init(format: "%d", (asset?.maintenanceAlertCount)!)
            self.maintanenceAlertsCountLabelWidthConstraint.constant = 20.0
        }
        else
        {
            self.maintanenceAlertsCountLabelWidthConstraint.constant = 0.0
        }
        
        
        let totalWidth = self.maintanenceAlertsCountLabelWidthConstraint.constant + self.criticalAlertsCountLabelWidthConstraint.constant + self.warningCountLabelWidthConstraint.constant
        
        if totalWidth == 60
        {
            self.alertCountContainerWidthConstraint.constant = 70.0
        }
        else if totalWidth == 40
        {
            self.alertCountContainerWidthConstraint.constant = 45.0
        }
        else if totalWidth == 20
        {
            self.alertCountContainerWidthConstraint.constant = 20.0
        }
        else
        {
            self.alertCountContainerWidthConstraint.constant = 0.0
        }
    }
    
    
    
    // MARK: - Collection View DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == cardOptionsCollectionView
        {
            return cardOptionNames.count
        }
        else
        {
            return (asset?.assetMeasurements.count)!
        }
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if collectionView == cardOptionsCollectionView
        {
            let cardOptionCell : CardOptionsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardOptionsCollectionViewCellID", for: indexPath as IndexPath) as! CardOptionsCollectionViewCell
            
            
            cardOptionCell.cardOptionNameLabel.text = cardOptionNames[indexPath.row]
            
            //        if cardOptionNames[indexPath.row] == "Alerts"
            //        {
            //            let alertCount = (self.assetGroup?.criticalAlertsCount)! + (self.assetGroup?.warningsCount)!
            //            cardOptionCell.cardOptionNameLabel.text = "Alerts (" + "\(alertCount)" + ")"
            //        }
            //        else
            //        {
            //            cardOptionCell.cardOptionNameLabel.text = cardOptionNames[indexPath.row]
            //        }
            
            cardOptionCell.cardOptionImageView.image = UIImage.init(named: cardOptionImages[indexPath.row])
            
            return cardOptionCell
            
        }
        else
        {
            let parametersCell : ParametersCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParametersCollectionViewCellID", for: indexPath as IndexPath) as! ParametersCollectionViewCell
            
            
            let parameter = asset?.assetSpec.measuredQuantities[indexPath.row]
            
            if parameter == selectedParameter
            {
                parametersCell.backgroundColor = Styles.Style.tabSelectionColor
                parametersCell.parameterLabel.textColor = UIColor.black
                parametersCell.parameterLabel.backgroundColor = Styles.Style.backgroundGrayColor
            }
            else
            {
                parametersCell.backgroundColor = UIColor.clear
                parametersCell.parameterLabel.backgroundColor = Styles.Style.backgroundGrayColor
                parametersCell.parameterLabel.textColor = UIColor.lightGray
            }
            
            parametersCell.parameterLabel.text = asset?.assetSpec.measuredQuantities[indexPath.row].name
            parametersCell.parameterLabel.font = UIFont(name: Styles.Style.boldStyleCustomFont, size: 14.0)
            return parametersCell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        if collectionView == self.cardOptionsCollectionView
        {
            if indexPath.row == 0
            {
                let storyboard = UIStoryboard(name: "UtilsStoryboard", bundle: nil)
                let vc : MapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewControllerID") as! MapViewController
                //                vc.assetGroup = assetGroup!
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 1
            {
                let storyboard = UIStoryboard(name: "UtilsStoryboard", bundle: nil)
                let vc : AssetAlertsViewController = storyboard.instantiateViewController(withIdentifier: "AssetAlertsViewControllerID") as! AssetAlertsViewController
                vc.asset = asset
                vc.assetGroup = assetGroup
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 4
            {
                let storyboard = UIStoryboard(name: "UtilsStoryboard", bundle: nil)
                let vc : PDFViewController = storyboard.instantiateViewController(withIdentifier: "PDFViewControllerID") as! PDFViewController
                vc.urlString = "https://appstore.waygum.io/Resources/Safety_Manual.pdf"
                vc.title = "Safety Manual"
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else if indexPath.row == 5
            {
                let storyboard = UIStoryboard(name: "UtilsStoryboard", bundle: nil)
                let vc : PDFViewController = storyboard.instantiateViewController(withIdentifier: "PDFViewControllerID") as! PDFViewController
                vc.urlString = "https://appstore.waygum.io/Resources/Temperature_Sensor_Datasheet.pdf"
                vc.title = "Manual"
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else
            {
                let storyboard = UIStoryboard(name: "UtilsStoryboard", bundle: nil)
                let vc : DeviceDataViewController = storyboard.instantiateViewController(withIdentifier: "DeviceDataViewControllerID") as! DeviceDataViewController
                vc.asset = asset
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else
        {
            
            selectedParameter = (asset?.assetSpec.measuredQuantities[indexPath.row])!
            selectedParameterIndex = indexPath.row
            self.deviceparametersCollectionView.reloadData()
            updateMeasurements()
            
        }

    }
    
    func loadDataFromServer()
    {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        let params = ["deviceId": "\(asset!.id)", "isLatestReading": "true"]
        
        
        
        let alertsWebService = MeasurementEventsWebServiceManager()
        alertsWebService.loadMeasurementEvents(parameters: params , success: {
            (response) in
            if let response = response{
                
                self.measurementsArray = response
                self.updateMeasurements()
                self.drawGraphView()
                
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
            }
        }, failure:
            {
                (error,statusCode) in
                
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
                if(statusCode != 0){
                    print("")
                }
        })
    }
    
    
    func updateMeasurements()
    {

        
        let measures = (measurementsArray as NSArray).filtered(using: NSPredicate.init(format: "id == %d", selectedParameter.id)) as! Array<AssetMeasurementEvent>
        
        if measures.count > 0
        {
//            lastUpdatedValue.text = (measures.first?.measurementReading)! + " " + (asset?.assetSpec.measuredQuantities[selectedParameterIndex].unitSymbol)!
            
            lastUpdatedValue.text = (measures.first?.measurementReading)!
            
            lastUpdatedTimeLabel.text = measures.first?.eventTime.formattedURCLocaleDateString
        }
        
    }
    
    
    func drawGraphView()
    {
        let graphSize = CGRect.init(x: 0, y: 0, width: self.graphContainerView.frame.size.width, height: self.graphContainerView.frame.size.height)
        let graph = LineGraphView.init(measurementsArray: self.measurementsArray, frame: graphSize)
        graph.backgroundColor = UIColor.red
        self.graphContainerView.backgroundColor = UIColor.green
        
        self.graphContainerView.addSubview(graph)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == cardOptionsCollectionView
        {
            return CGSize.init(width: 100, height: 80)
        }
        else
        {
            return CGSize.init(width: 120, height: 30)
        }
        
    }
    
    func heightForLabel(constraintedWidth width: CGFloat, font: UIFont, text: String) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView : UIWebView) {
        
        let contentSize: CGSize = webView.scrollView.contentSize
        let viewSize = webView.bounds.size
        
        let f = viewSize.width/contentSize.width
        
        webView.scrollView.minimumZoomScale = f
        webView.scrollView.maximumZoomScale = f
        webView.scrollView.zoomScale = 1.3
        
        
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
        
    }
    
    
}
