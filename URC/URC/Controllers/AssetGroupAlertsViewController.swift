//
//  AssetGroupAlertsViewController.swift
//  URC
//
//  Created by Saran Mahadevan on 07/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AssetGroupAlertsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    var assetGroup: AssetGroup?
    var activeAlertsList : Array<Alert> = []
    var resolvedAlertsList : Array<Alert> = []
    var isActiveAlertsTabSelected = true
    
    
    @IBOutlet weak var assetGroupImageView: UIImageView!
    @IBOutlet weak var assetGroupNameLabel: UILabel!
    @IBOutlet weak var assetGroupDescriptionLabel: UILabel!
    @IBOutlet weak var assetGroupLocationLabel: UILabel!
    @IBOutlet weak var alertsListCollectionView: UICollectionView!
    
    @IBOutlet weak var activeAlertsButton: UIButton!
    @IBOutlet weak var resolvedAlertsButton: UIButton!
    @IBOutlet weak var activeAlertsView: UIView!
    @IBOutlet weak var resolvedAlertsView: UIView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var alertCountsContainerView: UIView!
    @IBOutlet weak var criticalAlertCountLabel: UILabel!
    @IBOutlet weak var warningCountLabel: UILabel!
    @IBOutlet weak var maintanenceAlertCountLabel: UILabel!
    
    @IBOutlet weak var criticalAlertsCountLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var warningCountLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var maintanenceAlertsCountLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var alertCountContainerWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var assetGroupNameHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var assetGroupDescHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var assetGroupLocationHeightConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadActiveAlertsFromServer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - Collection View DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if isActiveAlertsTabSelected
        {
            return self.activeAlertsList.count
        }
        else
        {
            return self.resolvedAlertsList.count
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let alertCell : AlertCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlertCollectionViewCellID", for: indexPath as IndexPath) as! AlertCollectionViewCell
        
        if isActiveAlertsTabSelected
        {
            let alert = activeAlertsList[indexPath.row]
            alertCell.configureAlert(alert: alert)
        }
        else
        {
            let alert = resolvedAlertsList[indexPath.row]
            alertCell.configureAlert(alert: alert)
        }
        
        
        return alertCell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if isActiveAlertsTabSelected
        {
            let alert = activeAlertsList[indexPath.row]
            return CGSize(width: collectionView.frame.size.width, height: AlertCollectionViewCell.heightForItem(alert: alert))
        }
        else
        {
            let alert = resolvedAlertsList[indexPath.row]
            return CGSize(width: collectionView.frame.size.width, height: AlertCollectionViewCell.heightForItem(alert: alert))
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let vc : AlertsDetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertsDetailViewControllerID") as! AlertsDetailViewController
        
        let alert = activeAlertsList[indexPath.row]
        vc.alert = alert
        
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
    func updateUI()
    {
        
        self.title = "Alerts-" + (self.assetGroup?.name)!
        self.assetGroupNameLabel.text = self.assetGroup?.name
        self.assetGroupDescriptionLabel.text = self.assetGroup?.grpDescription
        self.assetGroupLocationLabel.text = self.assetGroup?.location
        
        if let data = NSData(contentsOf: assetGroup?.imageUrl as! URL) {
            self.assetGroupImageView.image = UIImage(data: data as Data)
        }
        
        
        
        self.assetGroupDescriptionLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)
        self.assetGroupNameLabel.font = UIFont(name: Styles.Style.boldStyleCustomFont, size: 14.0)
        self.assetGroupLocationLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let screenWidth = appDelegate.window?.frame.size.width
        let nameHeight = self.heightForLabel(constraintedWidth: screenWidth! - 120, font: assetGroupNameLabel.font, text: assetGroupNameLabel.text!)
        let descheight = self.heightForLabel(constraintedWidth: screenWidth! - 120, font: assetGroupDescriptionLabel.font, text: assetGroupDescriptionLabel.text!)
        let locationHeight = self.heightForLabel(constraintedWidth: screenWidth! - 145, font: assetGroupLocationLabel.font, text: assetGroupLocationLabel.text!)
        
        assetGroupNameHeightConstraint.constant = nameHeight
        assetGroupDescHeightConstraint.constant = descheight
        assetGroupLocationHeightConstraint.constant = locationHeight
        
        
        activeAlertsView.backgroundColor = Styles.Style.tabSelectionColor
        resolvedAlertsView.backgroundColor = UIColor.clear
        activeAlertsButton.titleLabel?.textColor = UIColor.black
        resolvedAlertsButton.titleLabel?.textColor = UIColor.lightGray
        
        
        self.updateAlertLabels()
        
        self.view.layoutSubviews()
        
    }
    
    
    func updateAlertLabels()
    {
        self.criticalAlertCountLabel.layer.cornerRadius = 10
        self.warningCountLabel.layer.cornerRadius = 10
        self.maintanenceAlertCountLabel.layer.cornerRadius = 10
        self.assetGroupImageView.layer.cornerRadius = 35
        
        
        
        if assetGroup?.criticalAlertsCount != 0
        {
            self.criticalAlertCountLabel.text = String.init(format: "%d", (assetGroup?.criticalAlertsCount)!)
            self.criticalAlertsCountLabelWidthConstraint.constant = 20.0
        }
        else
        {
            self.criticalAlertsCountLabelWidthConstraint.constant = 0.0
        }
        
        
        if assetGroup?.warningsCount != 0
        {
            self.warningCountLabel.text = String.init(format: "%d", (assetGroup?.warningsCount)!)
            self.warningCountLabelWidthConstraint.constant = 20.0
        }
        else
        {
            self.warningCountLabelWidthConstraint.constant = 0.0
        }
        
        if assetGroup?.maintenanceAlertsCount != 0
        {
            self.maintanenceAlertCountLabel.text = String.init(format: "%d", (assetGroup?.maintenanceAlertsCount)!)
            self.maintanenceAlertsCountLabelWidthConstraint.constant = 20.0
        }
        else
        {
            self.maintanenceAlertsCountLabelWidthConstraint.constant = 0.0
        }
        
        
        let totalWidth = self.maintanenceAlertsCountLabelWidthConstraint.constant + self.criticalAlertsCountLabelWidthConstraint.constant + self.warningCountLabelWidthConstraint.constant
        
        if totalWidth == 60
        {
            self.alertCountContainerWidthConstraint.constant = 70.0
        }
        else if totalWidth == 40
        {
            self.alertCountContainerWidthConstraint.constant = 45.0
        }
        else if totalWidth == 20
        {
            self.alertCountContainerWidthConstraint.constant = 20.0
        }
        else
        {
            self.alertCountContainerWidthConstraint.constant = 0.0
        }
    }
    
    
    
    func updateTabCounts()
    {
        if activeAlertsList.count > 0
        {
            activeAlertsButton.setTitle(String.init(format: "ACTIVE (%d)",activeAlertsList.count), for: .normal)
        }
        else
        {
            activeAlertsButton.setTitle(String.init(format: "ACTIVE (%d)",activeAlertsList.count), for: .normal)
        }

        self.view.layoutSubviews()
    }
    
    
    @IBAction func activeAlertsButtonClicked()
    {
        activeAlertsView.backgroundColor = Styles.Style.tabSelectionColor
        resolvedAlertsView.backgroundColor = UIColor.clear
        isActiveAlertsTabSelected = true
        self.alertsListCollectionView.reloadData()
        activeAlertsButton.titleLabel?.textColor = UIColor.black
        resolvedAlertsButton.titleLabel?.textColor = UIColor.lightGray
        self.view.layoutSubviews()
    }
    
    
    @IBAction func resolvedAlertsButtonClicked()
    {
        resolvedAlertsView.backgroundColor = Styles.Style.tabSelectionColor
        activeAlertsView.backgroundColor = UIColor.clear
        isActiveAlertsTabSelected = false
        self.alertsListCollectionView.reloadData()
        resolvedAlertsButton.titleLabel?.textColor = UIColor.black
        activeAlertsButton.titleLabel?.textColor = UIColor.lightGray
        self.view.layoutSubviews()
    }
    
    
    func loadActiveAlertsFromServer()
    {
        
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        
        
        let params = ["alertStatus":"alertStatus=ACKNOWLEDGED,GENERATED", "assetGroupId": "assetGroupId=\(assetGroup!.id)"]
        let alertsWebService = AlertsWebServiceManager()
        alertsWebService.loadAlerts(parameters: params as [String : AnyObject]!, success: {
            (response) in
            if let response = response{
                
                
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                
                
                self.activeAlertsList = response
                
                self.updateTabCounts()
                
                self.alertsListCollectionView.reloadData()
                self.loadResolvedAlertsFromServer()
            }
        }, failure:
            {
                (error,statusCode) in
                if(statusCode != 0){
                    print("")
                }
                
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                
        })
    }
    
    func loadResolvedAlertsFromServer()
    {
        let params = ["alertStatus":"alertStatus=CLOSED", "assetGroupId": "assetGroupId=\(assetGroup!.id)"]
        let alertsWebService = AlertsWebServiceManager()
        alertsWebService.loadAlerts(parameters: params as [String : AnyObject]!, success: {
            (response) in
            if let response = response{
                
                self.resolvedAlertsList = response
                
                self.updateTabCounts()
                
                self.alertsListCollectionView.reloadData()
            }
        }, failure:
            {
                (error,statusCode) in
                if(statusCode != 0){
                    print("")
                }
        })
    }
    
    func heightForLabel(constraintedWidth width: CGFloat, font: UIFont, text: String) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
    
    
}
