//
//  AssetGroupDetailsViewController.swift
//  URC
//
//  Created by Saran Mahadevan on 30/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AssetGroupDetailsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var assetGroup: AssetGroup?
    var assetsArray: Array<Asset>=[]
    
    
    @IBOutlet weak var assetGroupImageView: UIImageView!
    @IBOutlet weak var assetGroupNameLabel: UILabel!
    @IBOutlet weak var assetGroupDescriptionLabel: UILabel!
    @IBOutlet weak var assetGroupLocationLabel: UILabel!
    @IBOutlet weak var devicesCountlabel: UILabel!
    @IBOutlet weak var devicesCollectionView: UICollectionView!
    @IBOutlet weak var cardOptionsCollectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var isFavorite: Bool = false
    
    
    var cardOptionNames = ["Location","Alerts","O&M","Safety"]
    var cardOptionImages = ["ic_map_view_active","ic_asset_alerts","ic_asset_oam","ic_asset_safety"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 100, height: 80)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        cardOptionsCollectionView!.collectionViewLayout = layout
        
        
        setNavigationBarStyle()
        updateFavoritesButton()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateUI()
        loadDataFromServer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setNavigationBarStyle(){
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationController!.navigationBar.barTintColor = Styles.Style.appNavigationBarColor
        self.navigationController!.navigationBar.tintColor = Styles.Style.appNavigationContentColor
        
        //        let navFontDescriptor = UIFont(name: kFontAwesomeFamilyName, size: 17.0 )!.fontDescriptor.withSymbolicTraits(UIFontDescriptorSymbolicTraits.traitBold)
        //        let titleDict: NSDictionary = [NSForegroundColorAttributeName: Styles.Style.appNavigationTitleColor, NSFontAttributeName : UIFont(descriptor: navFontDescriptor!, size: 17.0)]
        //        self.navigationController!.navigationBar.titleTextAttributes = (titleDict as! [String : AnyObject])
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        
        //        let rightMenuButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: Constants.ImagesName.WHITE_CLOSE_ICON.rawValue), style: .Plain, target: self, action: #selector(CollaborationViewController.closeCollaborationController(_:)))
        //        self.navigationItem.rightBarButtonItem = rightMenuButton
        
    }
    
    
    func updateUI()
    {
        self.assetGroupNameLabel.text = self.assetGroup?.name
        self.assetGroupDescriptionLabel.text = self.assetGroup?.grpDescription
        self.assetGroupLocationLabel.text = assetGroup?.location // NSString.attributedIconString(IconType.FAMapMarker, withTitle: assetGroup?.location, fontSize: 13.0)
        
        
        self.assetGroupNameLabel.font = UIFont(name: Styles.Style.boldStyleCustomFont, size: 14.0)
        self.assetGroupDescriptionLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)
        self.assetGroupLocationLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        
        self.assetGroupImageView.layer.cornerRadius = 35
        
        if let data = NSData(contentsOf: assetGroup?.imageUrl as! URL) {
            self.assetGroupImageView.image = UIImage(data: data as Data)
        }
        
        self.title = self.assetGroup?.name
        
        if assetGroup != nil
        {
            
        }
    }
    
    
    func updateFavoritesButton()
    {
        if URCManager.sharedInstance.favoriteAssetGroups.contains(assetGroup!)
        {
            let button1 = UIBarButtonItem(image: UIImage(named: "ic_favourite_active_nav_tablet"), style: .plain, target: self, action: #selector(AssetGroupDetailsViewController.action))
            self.navigationItem.rightBarButtonItem  = button1
            isFavorite = true
        }
        else
        {
            let button1 = UIBarButtonItem(image: UIImage(named: "ic_favourite_nav_tablet"), style: .plain, target: self, action: #selector(AssetGroupDetailsViewController.action))
            self.navigationItem.rightBarButtonItem  = button1
            isFavorite = false
            
        }
        
    }
    
    func action()
    {
        if !isFavorite
        {
            
            let button1 = UIBarButtonItem(image: UIImage(named: "ic_favourite_active_nav_tablet"), style: .plain, target: self, action: #selector(AssetGroupDetailsViewController.action))
            self.navigationItem.rightBarButtonItem  = button1
            isFavorite = true
            URCManager.sharedInstance.favoriteAssetGroups.append(self.assetGroup!)
        }
        else
        {
            let button1 = UIBarButtonItem(image: UIImage(named: "ic_favourite_nav_tablet"), style: .plain, target: self, action: #selector(AssetGroupDetailsViewController.action))
            self.navigationItem.rightBarButtonItem  = button1
            isFavorite = false
            
            let index = URCManager.sharedInstance.favoriteAssetGroups.index(of: assetGroup!)
            URCManager.sharedInstance.favoriteAssetGroups.remove(at: index!)
        }
        
    }
    
    
    // MARK: - Collection View DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == cardOptionsCollectionView
        {
            return 4
        }
        return self.assetsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if collectionView == cardOptionsCollectionView
        {
            let cardOptionCell : CardOptionsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardOptionsCollectionViewCellID", for: indexPath as IndexPath) as! CardOptionsCollectionViewCell
            
            
            if cardOptionNames[indexPath.row] == "Alerts"
            {
                let alertCount = (self.assetGroup?.criticalAlertsCount)! + (self.assetGroup?.warningsCount)!
                cardOptionCell.cardOptionNameLabel.text = "Alerts (" + "\(alertCount)" + ")"
            }
            else
            {
                cardOptionCell.cardOptionNameLabel.text = cardOptionNames[indexPath.row]
            }
            
            cardOptionCell.cardOptionImageView.image = UIImage.init(named: cardOptionImages[indexPath.row])
            
            return cardOptionCell
            
        }
        else
        {
            let assetCell : DevicesCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DevicesCollectionViewCellID", for: indexPath as IndexPath) as! DevicesCollectionViewCell
            
            let asset = assetsArray[indexPath.row]
            
            
            assetCell.deviceImageView.layer.cornerRadius = 35
            assetCell.deviceMeasuredValuesLabel.numberOfLines = asset.assetSpec.measuredQuantities.count
            
            assetCell.deviceNameLabel.font = UIFont(name: Styles.Style.boldStyleCustomFont, size: 14.0)
            assetCell.deviceDesciptionLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)
            assetCell.deviceMeasurementsLabel.font = UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)
            assetCell.deviceMeasuredValuesLabel.font = UIFont(name: Styles.Style.boldStyleCustomFont, size: 13.0)
            
            
            assetCell.criticalAlertsCount.layer.cornerRadius = 10
            assetCell.warningsCount.layer.cornerRadius = 10
            assetCell.resolvedAlertsCount.layer.cornerRadius = 10
            
            assetCell.deviceNameLabel.text = asset.name
            assetCell.deviceDesciptionLabel.text = asset.deviceDesc
            assetCell.deviceDescLabelHeightConstraint.constant = heightForLabel(constraintedWidth: collectionView.frame.size.width - 120, font: UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)!, text: asset.deviceDesc)
            
            
            if let data = NSData(contentsOf: asset.assetSpec.displayImageRef as! URL) {
                assetCell.deviceImageView.image = UIImage(data: data as Data)
            }
            
            let quantities: NSMutableAttributedString = NSMutableAttributedString()
            let measurementValues: NSMutableAttributedString = NSMutableAttributedString()
            
            
            
            for index in 0...asset.assetSpec.measuredQuantities.count - 1
            {
                if index != 0
                {
                    let lineBreak = NSAttributedString(string: "\n", attributes: [NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)!])
                    quantities.append(lineBreak)
                    measurementValues.append(lineBreak)
                }
                
                let alertAsset = NSAttributedString(string: asset.assetSpec.measuredQuantities[index].name + " ", attributes: [ NSForegroundColorAttributeName: UIColor.black , NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)!])
                quantities.append(alertAsset)
                
                
                if asset.assetSpec.measuredQuantities[index].alertLevel == "critical"
                {
                    
                    let measurementValue = NSAttributedString(string: asset.assetMeasurements[index].measurementReading + " " + asset.assetSpec.measuredQuantities[index].unitSymbol, attributes: [ NSForegroundColorAttributeName: Styles.Style.criticalAlertColor , NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)!])
                    measurementValues.append(measurementValue)
                }
                    
                else if asset.assetSpec.measuredQuantities[index].alertLevel == "warning"
                {
                    
                    let measurementValue = NSAttributedString(string: asset.assetMeasurements[index].measurementReading + " " + asset.assetSpec.measuredQuantities[index].unitSymbol, attributes: [ NSForegroundColorAttributeName: Styles.Style.warningAlertColor , NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)!])
                    measurementValues.append(measurementValue)
                    
                }
                else
                {
                    
                    let measurementValue = NSAttributedString(string: asset.assetMeasurements[index].measurementReading + " " + asset.assetSpec.measuredQuantities[index].unitSymbol, attributes: [ NSForegroundColorAttributeName: Styles.Style.resolvedAlertColor , NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)!])
                    measurementValues.append(measurementValue)
                }
                
            }
            
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 8
            paragraphStyle.alignment = .right
            measurementValues.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, measurementValues.length))
            
            
            let paragraphStyleRight = NSMutableParagraphStyle()
            paragraphStyleRight.lineSpacing = 8
            paragraphStyleRight.alignment = .left
            quantities.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyleRight, range:NSMakeRange(0, quantities.length))
            
            assetCell.deviceMeasurementsLabel.attributedText = quantities
            assetCell.deviceMeasuredValuesLabel.attributedText = measurementValues
            
            assetCell.criticalAlertsCount.text = String(asset.criticalAlertCount)
            assetCell.warningsCount.text = String(asset.warningAlertCount)
            assetCell.resolvedAlertsCount.text = String(asset.maintenanceAlertCount)
            
            if asset.criticalAlertCount > 0
            {
                assetCell.criticalAlertsLabelWidthConstraint.constant = 20.0
            }
            else{
                assetCell.criticalAlertsLabelWidthConstraint.constant = 0.0
            }
            
            if asset.warningAlertCount > 0
            {
                assetCell.warningAlertsLabelWidthConstraint.constant = 20.0
            }
            else{
                assetCell.warningAlertsLabelWidthConstraint.constant = 0.0
            }
            
            if asset.maintenanceAlertCount > 0
            {
                assetCell.resolvedAlertsCount.isHidden = false
            }
            else{
                assetCell.resolvedAlertsCount.isHidden = true
            }
            
            assetCell.alertsCountContainerViewWidthConstraint.constant = 20
            //            if asset.criticalAlertCount > 0
            //            {
            //
            //                assetCell.deviceMeasuredValuesLabel.textColor = assetCell.criticalAlertsCount.backgroundColor
            //            }
            //            else if asset.warningAlertCount > 0
            //            {
            //                assetCell.deviceMeasuredValuesLabel.textColor = assetCell.warningsCount.backgroundColor
            //            }
            //            else
            //            {
            //                assetCell.deviceMeasuredValuesLabel.textColor = assetCell.resolvedAlertsCount.backgroundColor
            //            }
            
            
            return assetCell
        }
    }
    
    // MARK: - Collection View DataSource
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        if collectionView == self.cardOptionsCollectionView
        {
            if indexPath.row == 0
            {
                let storyboard = UIStoryboard(name: "UtilsStoryboard", bundle: nil)
                let vc : MapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewControllerID") as! MapViewController
                vc.assetGroup = assetGroup!
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 1
            {
                let storyboard = UIStoryboard(name: "UtilsStoryboard", bundle: nil)
                let vc : AssetGroupAlertsViewController = storyboard.instantiateViewController(withIdentifier: "AssetGroupAlertsViewControllerID") as! AssetGroupAlertsViewController
                vc.assetGroup = assetGroup
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 2
            {
                let storyboard = UIStoryboard(name: "UtilsStoryboard", bundle: nil)
                let vc : PDFViewController = storyboard.instantiateViewController(withIdentifier: "PDFViewControllerID") as! PDFViewController
                vc.urlString = "https://appstore.waygum.io/Resources/Safety_Manual.pdf"
                vc.title = "Safety Manual"
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else
            {
                let storyboard = UIStoryboard(name: "UtilsStoryboard", bundle: nil)
                let vc : PDFViewController = storyboard.instantiateViewController(withIdentifier: "PDFViewControllerID") as! PDFViewController
                vc.urlString = "https://appstore.waygum.io/Resources/Temperature_Sensor_Datasheet.pdf"
                vc.title = "Manual"
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
        
        collectionView.deselectItem(at: indexPath as IndexPath, animated: true)
        
        //        let storyboard = UIStoryboard(name: "AlertsStoryboard", bundle: nil)
        //        let alertDetailViewController : AlertsDetailViewController = storyboard.instantiateViewControllerWithIdentifier("AlertsDetailViewControllerID") as! AlertsDetailViewController
        //
        //        alertDetailViewController.alert = self.alertsArray[indexPath.row]
        //        self.navigationController?.pushViewController(alertDetailViewController, animated: false)
    }
    
    //    private func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    //        return CGSize(width: 375, height: 100)
    //    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.cardOptionsCollectionView
        {
            return CGSize.init(width: 100, height: 80)
        }
        else
        {
            return CGSize.init(width: 375, height: DevicesCollectionViewCell.heightForItem(asset: assetsArray[indexPath.row]))
        }
        
    }
    
    
    
    
    func loadDataFromServer()
    {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        let webAction = ManagedDevicesWebServiceManager()
        let params = ["assetGroupId":"\(assetGroup!.id)"]
        webAction.loadDevices(parameters: params as [String : String]!, success: {
            (response) in
            if let response = response{
                
                self.assetsArray = response
                self.devicesCollectionView.reloadData()
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
            }
        }, failure: {
            (error,statusCode) in
            if(statusCode != 0){
                //                    self.didLoadDataServiceSucceed = false
                //                    self.showErrorFromService(statusCode)
            }
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
        })
    }
    
    
    
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    
    
    //        let storyboard = UIStoryboard(name: "UtilsStoryboard", bundle: nil)
    //        let vc : PDFViewController = storyboard.instantiateViewController(withIdentifier: "PDFViewControllerID") as! PDFViewController
    //        self.navigationController?.pushViewController(vc, animated: true)
    
    //        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil , action: nil )
    //
    //        var index: IndexPath?
    //        if(segue.identifier == "AssetGroupDetails"){
    //            if let cell = sender as? UICollectionViewCell{
    //                index = self.assetsCollectionView.indexPath(for: cell)
    //
    //            }else if let cell = sender as? UICollectionViewCell{
    //                index = self.assetsCollectionView?.indexPath(for: cell)
    //            }
    //
    //            let destinationVC = segue.destination as! AssetGroupDetailsViewController
    //            destinationVC.assetGroup = self.assetGroupsArray[(index?.row)!]
    //
    //        }
    
    //    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil , action: nil )
        
        var index: IndexPath?
        if(segue.identifier == "AssetDetailID"){
            if let cell = sender as? UICollectionViewCell{
                index = self.devicesCollectionView.indexPath(for: cell)
                
            }else if let cell = sender as? UICollectionViewCell{
                index = self.devicesCollectionView?.indexPath(for: cell)
            }
            
            let destinationVC = segue.destination as! AssetDetailsViewController
            destinationVC.asset = self.assetsArray[(index?.row)!]
            destinationVC.assetGroup = assetGroup
            
        }
        
    }
    
    func heightForLabel(constraintedWidth width: CGFloat, font: UIFont, text: String) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
    
    
    
    
}
