//
//  CollaborationViewController.swift
//  URC
//
//  Created by Saran Mahadevan on 30/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class CollaborationViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var rocketChatWebview: UIWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Collaboration"
        setNavigationBarStyle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadRocketChatWebView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNavigationBarStyle(){
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationController!.navigationBar.barTintColor = Styles.Style.appNavigationBarColor
        self.navigationController!.navigationBar.tintColor = Styles.Style.appNavigationContentColor
        
        
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        
        
        
    }
    
    
    func loadRocketChatWebView(){
        
        let url = NSURL(string: Constants.getRocketChatUrl())
        let reqObject = NSMutableURLRequest(url: url! as URL)
        
        self.rocketChatWebview?.loadRequest(reqObject as URLRequest)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // Mark - WebView Delegates
    
    
//    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
//        if (navigationType == UIWebViewNavigationType.linkClicked)
//        {
//            if(request.URL?.absoluteString.rangeOfString("rocketchat_uploads") != nil){
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let webViewCntrl : CommonWebViewController = storyboard.instantiateViewControllerWithIdentifier("CommonWebViewController") as! CommonWebViewController
//                webViewCntrl.setUrlTitleAndZoom((request.URL?.absoluteString)!,title: Constants.Labels.kDEFAULT_WEBVIEW_TITLE.rawValue,enableZoom:false)
//                self.navigationController?.pushViewController(webViewCntrl, animated: false)
//                return false
//            }
//        }
//        
//        return true;
//    }
//    func webViewDidStartLoad(webView: UIWebView) {
//        
//        self.view.addSubview(self.activityIndicatorView.getViewActivityIndicatorWithTitle(Constants.ActivityIndicatorTexts.LoadingCollaboration.rawValue))
//        self.activityIndicatorView.startAnimatingWithInteractionsAllowed()
//    }
//    func webViewDidFinishLoad(webView: UIWebView) {
//        self.activityIndicatorView.stopAnimatingWithInteractionsAllowed()
//    }
//    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
//        self.activityIndicatorView.stopAnimatingWithInteractionsAllowed()
//    }

}
