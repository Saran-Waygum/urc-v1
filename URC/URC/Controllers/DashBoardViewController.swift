//
//  DashBoardViewController.swift
//  URC
//
//  Created by Saran Mahadevan on 11/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class DashBoardViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var notificationsCollectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    var notificationsArray : Array<Notification> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateUI()
        
        self.headerView.backgroundColor = Styles.Style.appNavigationBarColor

    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavigationBarStyle()
        
        loadNotificationsData()
        
//        loadDataFromJSON()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadNotificationsData()
    {
        let notificationsWebServiceManager = NotificationsWebServiceManager()
        
        
        notificationsWebServiceManager.loadNotifications(success: {
            (response) in
            if let response = response{
                
                self.notificationsArray = response
                
                self.notificationsCollectionView.reloadData()
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
            }
        }, failure:
            {
                (error,statusCode) in
                if(statusCode != 0){
                    print("")
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                    
                }
        })
    }
    
    
    func loadDataFromJSON()
    {
        let url = Bundle.main.url(forResource: "Notifications", withExtension: "json")
        let data = NSData(contentsOf: url!)
        
        do {
            let object = try JSONSerialization.jsonObject(with: data! as Data, options: .allowFragments)
            if let dictionary = object as? Array<Dictionary<String, AnyObject>> {
                
                var locationName: String = ""
                
                
                for object in dictionary
                {
                    
                    if let notificationInfo = object as? Dictionary<String, AnyObject>
                    {
                        
                        if (notificationInfo["type"] as? String) == "Alert.Generated"
                        {
                            let newAlertNotification = AlertNotification.init()
                            
                            newAlertNotification.alertDescription = object["alertDescription"] as! String
                            
                            newAlertNotification.id = object["id"] as! Int
                            newAlertNotification.alertState = object["alertState"] as! String
                            newAlertNotification.generatedBy = object["generatedBy"] as! String
                            
                            newAlertNotification.assetName = object["assetName"] as! String
                            newAlertNotification.alertMessage = object["message"] as! String
                            
                            newAlertNotification.alertId = object["alertId"] as! Int
                            newAlertNotification.alertState = object["alertState"] as! String
                            newAlertNotification.alertLevel = object["alertLevel"] as! String
                            newAlertNotification.isRead = object["read"] as! Bool
                            
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX"
                            
                            
                            let dateGeneratedString = dateFormatter.date(from: notificationInfo["generatedAt"] as! String)!
                            
                            dateFormatter.dateFormat = "hh:mm a"
                            dateFormatter.amSymbol = "AM"
                            dateFormatter.pmSymbol = "PM"
                            
                            newAlertNotification.generatedTime = (dateGeneratedString as NSDate) as Date
                            
                            //                        newAlert.dateGenerated = dateFormatter.string(from: dateGeneratedString)
                            
                            
                            self.notificationsArray.append(newAlertNotification)
                        }
                    }
                    
                    
                    
                }
                
                self.notificationsCollectionView.reloadData()
                
            }
        } catch {
            // Handle Error
        }
        
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func updateUI()
    {
        
        let userDefaults = UserDefaults.standard
        
        if userDefaults.string(forKey: "firstName") != "" || userDefaults.string(forKey: "lastName") != ""
        {
            self.userNameLabel.text = userDefaults.string(forKey: "firstName")! + " " + userDefaults.string(forKey: "lastName")!
        }
        
        if userDefaults.string(forKey: "email") != ""
        {
            self.emailLabel.text = userDefaults.string(forKey: "email")!
        }
        
        if userDefaults.string(forKey: "profileImageUrl") != ""
        {
            if let data = NSData(contentsOf: URL.init(string: userDefaults.string(forKey: "profileImageUrl")!)!), URCManager.sharedInstance.user.profileImageUrl != "" {
                userImageView.image = UIImage(data: data as Data)
            }
        }
        
        
        self.userImageView.layer.cornerRadius = 30.0
        
        self.userNameLabel.font = UIFont(name: Styles.Style.boldStyleCustomFont, size: 14.0)
        self.emailLabel.font = UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 12.0)
        self.locationLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
        
//        self.userNameLabel.font = 
        
        // ic_nav_settings
        self.title = "Dashboard"
        
        
        let button: UIButton = UIButton()
        button.setImage(UIImage(named: "ic_nav_settings"), for: .normal)
        button.frame = CGRect.init(x: 0, y: 0, width: 45, height: 45)
        button.addTarget(self, action: #selector(DashBoardViewController.settingsButtonClicked), for: UIControlEvents.touchUpInside)

        
        let rightItem:UIBarButtonItem = UIBarButtonItem()
        rightItem.customView = button
        
        navigationItem.rightBarButtonItem = rightItem
        
        

        
    }
    
    func addTapped()
    {
        
    }
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    func setNavigationBarStyle(){
        
        self.navigationController?.navigationItem.leftBarButtonItem = nil
//        self.navigationController!.navigationBar.barTintColor = Styles.Style.appNavigationBarColor
//        self.navigationController!.navigationBar.tintColor = Styles.Style.appNavigationContentColor
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 16.0)!]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
//        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view.backgroundColor = Styles.Style.appNavigationBarColor
//        
//        self.navigationController!.navigationBar.backgroundColor = Styles.Style.appNavigationBarColor
//        

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        
    }
    
    
    
    // MARK: - Collection View DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.notificationsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let notificationCell : DashBoardCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashBoardCollectionViewCellID", for: indexPath as IndexPath) as! DashBoardCollectionViewCell
        
        let notification = notificationsArray[indexPath.row]
        
        
        if notification is AlertNotification
        {
            let alertNotification = notification as! AlertNotification
            
            
            if alertNotification.alertLevel == "critical"
            {
                notificationCell.notificationImageView.image = UIImage.init(named: "critical")
            }
            else
            {
                notificationCell.notificationImageView.image = UIImage.init(named: "warning")
            }
            
            notificationCell.notificationTextLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)
            notificationCell.notificationTimeLabel.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 12.0)
            
            
            
            let alertAsset = NSAttributedString(string: alertNotification.assetName + " ", attributes: [ NSForegroundColorAttributeName: Styles.Style.tabSelectionColor , NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)!])
            let alertDesc = NSAttributedString(string: alertNotification.alertDescription, attributes: [ NSForegroundColorAttributeName: UIColor.black , NSFontAttributeName: UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)!])
            let alertState = NSAttributedString(string: ", " + alertNotification.alertState.camelCase(), attributes: [ NSForegroundColorAttributeName: UIColor.black , NSFontAttributeName: UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)!])
            
            let combination = NSMutableAttributedString()
            combination.append(alertAsset)
            combination.append(alertDesc)
            combination.append(alertState)
            
            if alertNotification.generatedBy != "" && alertNotification.alertState != "GENERATED"
            {
                let alertState = NSAttributedString(string: " by ", attributes: [ NSForegroundColorAttributeName: UIColor.black , NSFontAttributeName: UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)!])
                let alertAsset = NSAttributedString(string: alertNotification.generatedBy + " ", attributes: [ NSForegroundColorAttributeName: Styles.Style.tabSelectionColor , NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)!])
                
                combination.append(alertState)
                combination.append(alertAsset)
            }
            
            notificationCell.notificationTextLabel.attributedText = combination
            
            
            let date = Date()
            let interval = date.timeIntervalSince(alertNotification.generatedTime as Date)
            
            
            let minutes = Int(interval/60)
            
            
            
            if minutes < 60
            {
                notificationCell.notificationTimeLabel.text = String.init(format: "%d minutes ago", minutes)
            }
            else if minutes < 1440
            {
                notificationCell.notificationTimeLabel.text = String.init(format: "%d hours ago", Int(minutes/60))
            }
            else
            {
                notificationCell.notificationTimeLabel.text = alertNotification.generatedTime.formattedURCLocaleDateString
            }

            notificationCell.notificationTextLabelHeight.constant = DashBoardCollectionViewCell.heightForItem(notification: notification) - 40.0
            
        }
        
        
        
        if notification.isRead
        {
            notificationCell.readViewWidthConstraint.constant = 0
            notificationCell.readView.backgroundColor = UIColor.clear
        }
        else
        {
            notificationCell.readViewWidthConstraint.constant = 2
            notificationCell.readView.backgroundColor = Styles.Style.tabSelectionColor
        }
        
        return notificationCell
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let notification = notificationsArray[indexPath.row]
        
        if notification is AlertNotification
        {
            let alertNotification = notification as! AlertNotification
            loadAlertDataFromServer(alertNotification: alertNotification)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

            let alert = notificationsArray[indexPath.row]
            return CGSize(width: collectionView.frame.size.width, height: DashBoardCollectionViewCell.heightForItem(notification: alert))

    }
    
    
    func loadAlertDataFromServer(alertNotification: AlertNotification)
    {
        
        let params = ["alertId":"\(alertNotification.alertId)"]
        let alertsWebService = AlertsWebServiceManager()
        
        alertsWebService.loadAlertData(parameters: params, success: {
            (response) in
            if let response = response{
                
                self.navigateToAlertDetail(alert: response)
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                self.updateReadMessageStatus(alertNotification: alertNotification)

            }
        }, failure:
            {
                (error,statusCode) in
                if(statusCode != 0){
                    print("")
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                    
                }
        })
    }
    
    
    func updateReadMessageStatus(alertNotification: AlertNotification)
    {
        let notificationsWebServiceManager = NotificationsWebServiceManager()
        
        let params = ["alertId":"\(alertNotification.id)"]
        notificationsWebServiceManager.readNotification(parameters: params, success: {
            (response) in
            if let response = response{
                
//                self.activityIndicator.stopAnimating()
//                self.activityIndicator.isHidden = true
            }
        }, failure:
            {
                (error,statusCode) in
                if(statusCode != 0){
                    print("")
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                    
                }
        })
    }
    
    func navigateToAlertDetail(alert:Alert)
    {
        
        let vc : AlertsDetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertsDetailViewControllerID") as! AlertsDetailViewController
        vc.alert = alert
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func settingsButtonClicked()
    {
        
//        let vc : SettingsViewController = UIStoryboard(name: "UtilsStoryboard", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewControllerID") as! SettingsViewController
//        
//        self.navigationController?.pushViewController(vc, animated: true)

        
        let vc : NotesListViewController = UIStoryboard(name: "UtilsStoryboard", bundle: nil).instantiateViewController(withIdentifier: "NotesListViewControllerID") as! NotesListViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    func heightForLabel(constraintedWidth width: CGFloat, font: UIFont, text: String) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
    
}
