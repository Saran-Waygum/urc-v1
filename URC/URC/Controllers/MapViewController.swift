//
//  MapViewController.swift
//  URC
//
//  Created by Saran Mahadevan on 07/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    let regionRadius: CLLocationDistance = 1000
    
    
    var assetGroup : AssetGroup = AssetGroup.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    
        let initialLocation = CLLocation(latitude: assetGroup.geoLocationInfo.geoLocationCenterLatitude, longitude: assetGroup.geoLocationInfo.geoLocationCenterLongitude)
        
        centerMapOnLocation(location: initialLocation)
        
        addAllSiteAnnotations()
        // Do any additional setup after loading the view.
    }
    
    
    func centerMapOnLocation(location: CLLocation) {
        
        
        
        
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addAllSiteAnnotations(){
//        let sites = URCManager.sharedInstance.sites
//        
        var annotations : Array<SiteMKAnnotation> = []
//        for (_,site) in sites.enumerated(){
//            
//            let site = SiteMKAnnotation(title: site.siteName , id: site.siteId,
//                                        coordinate: CLLocationCoordinate2D(latitude: site.location.latitude, longitude: site.location.longitude))
//            annotations.append(site)
//            //            sitesMarked.append(site)
//            
//            
//            
//            self.mapView!.addAnnotation(site as MKAnnotation)
//        }
        //annotations.append(SiteMKAnnotation(title: assetGroup.name , id: 2,
        //coordinate: CLLocationCoordinate2D(latitude: assetGroup.geoLocationInfo.geoLocationCenterLatitude , longitude: assetGroup.geoLocationInfo.geoLocationCenterLongitude)))
        
        let lat: Double = assetGroup.geoLocationInfo.geoLocationCenterLatitude
        let long: Double = assetGroup.geoLocationInfo.geoLocationCenterLongitude
//        annotations.append(SiteMKAnnotation(title: assetGroup.name , id: 2,
//                                            coordinate: CLLocationCoordinate2D.init(latitude: lat, longitude: long)))
//        
//        self.mapView?.addAnnotations(annotations)
        self.mapView.addAnnotation(SiteMKAnnotation(title: assetGroup.name , id: 2,
                                                    coordinate: CLLocationCoordinate2D.init(latitude: lat, longitude: long)))
        
    }
    
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        if(annotation.isKind(of: MKUserLocation.self) == true){
            return nil
        }
        if (annotation.isKind(of: SiteMKAnnotation.self) == true) {
            let identifier = "customPin"
            var view: MKAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            { // 2
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                // 3
                view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
                //let imageName = annotation.imageName
                view.image = UIImage(named: Constants.ImagesName.MAP_PIN_GREEN.rawValue)
                
            }
            
            return view
        }
        var view: MKAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: "pin")
        { // 2
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            // 3
            view = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            //let imageName = annotation.imageName
            view.image = UIImage(named: Constants.ImagesName.MAP_PIN_GREEN.rawValue)
            
        }
        
        return view
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
