//
//  PDFViewController.swift
//  URC
//
//  Created by Saran Mahadevan on 06/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class PDFViewController: UIViewController {

    var urlString : String = ""
    @IBOutlet weak var pdfWebView: UIWebView!
    let pageTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebView()
        self.title = pageTitle
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func loadWebView(){
        if urlString != "" {
            let url = NSURL(string: urlString)
            let reqObject = NSMutableURLRequest(url: url! as URL)
            self.pdfWebView?.loadRequest(reqObject as URLRequest)
        }
        
    }
    

}
