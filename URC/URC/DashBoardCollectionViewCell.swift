//
//  DashBoardCollectionViewCell.swift
//  URC
//
//  Created by Saran Mahadevan on 11/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class DashBoardCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var notificationImageView: UIImageView!
    @IBOutlet weak var notificationTimeLabel: UILabel!
    @IBOutlet weak var notificationTextLabel: UILabel!
        
    @IBOutlet weak var notificationTextLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var readView: UIView!
    @IBOutlet weak var readViewWidthConstraint: NSLayoutConstraint!

    
    class func heightForItem(notification: Notification) -> CGFloat
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let screenWidth = appDelegate.window?.frame.size.width
        
        if notification is AlertNotification
        {
            
            let alertNotification = notification as! AlertNotification
            let label =  UILabel(frame: CGRect(x: 0, y: 0, width: screenWidth!-80, height: .greatestFiniteMagnitude))
            label.numberOfLines = 0
            
            let alertAsset = NSAttributedString(string: alertNotification.assetName + " ", attributes: [ NSForegroundColorAttributeName: Styles.Style.tabSelectionColor , NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 13.0)!])
            let alertDesc = NSAttributedString(string: alertNotification.alertDescription, attributes: [ NSForegroundColorAttributeName: UIColor.black , NSFontAttributeName: UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)!])
            
            let alertState = NSAttributedString(string: ", " + alertNotification.alertState, attributes: [ NSForegroundColorAttributeName: UIColor.black , NSFontAttributeName: UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)!])
            
            let combination = NSMutableAttributedString()
            combination.append(alertAsset)
            combination.append(alertDesc)
            combination.append(alertState)
            
            label.attributedText = combination
            label.sizeToFit()
            return 40 + label.frame.height
            
        }
        else
        {
            return 0.0
        }
        
        
    }
    
}

