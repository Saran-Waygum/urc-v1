//
//  DeviceDataViewController.swift
//  URC
//
//  Created by Saran Mahadevan on 06/01/17.
//  Copyright © 2017 Waygum. All rights reserved.
//

import UIKit

class DeviceDataViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate
{
    
    var asset: Asset?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var measurementsCollectionView: UICollectionView!
    @IBOutlet weak var parametersCollectionView: UICollectionView!
    @IBOutlet weak var durationPickerButton: UIButton!
    
    var selectedParameter : AssetSpecMeasurementQuantity = AssetSpecMeasurementQuantity.init()
    var measurementsArray: Array<AssetMeasurementEvent> = []
    var measurementsArrayForParameter: Array<AssetMeasurementEvent> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        selectedParameter = (asset?.assetSpec.measuredQuantities.first)!
        
        self.title = (asset?.name)! + " Data"
    }
    
    func filterMeasurementsForParameter()
    {
        
        measurementsArrayForParameter = (measurementsArray as NSArray).filtered(using: NSPredicate.init(format: "id == %d", selectedParameter.id)) as! Array<AssetMeasurementEvent>
        
        self.measurementsCollectionView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadDataFromServer()
    }
    
    
    func loadDataFromServer()
    {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        let params = ["deviceId": "\(asset!.id)", "isLatestReading": "false"]
        
        
        
        let alertsWebService = MeasurementEventsWebServiceManager()
        alertsWebService.loadMeasurementEvents(parameters: params , success: {
            (response) in
            if let response = response{
                
                self.measurementsArray = response
                self.measurementsCollectionView.reloadData()
                
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                self.filterMeasurementsForParameter()
                
            }
        }, failure:
            {
                (error,statusCode) in
                
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
                if(statusCode != 0){
                    print("")
                }
        })
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == parametersCollectionView
        {
            return (asset?.assetSpec.measuredQuantities.count)!
        }
        else
        {
            return measurementsArrayForParameter.count
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if collectionView == parametersCollectionView
        {
            let cardOptionCell : DeviceParametersCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DeviceParametersCollectionViewCellID", for: indexPath as IndexPath) as! DeviceParametersCollectionViewCell
            
            let parameter = asset?.assetSpec.measuredQuantities[indexPath.row]
            
            if parameter == selectedParameter
            {
                cardOptionCell.backgroundColor = Styles.Style.tabSelectionColor
                cardOptionCell.parameterLabel.textColor = UIColor.black
                
            }
            else
            {
                cardOptionCell.backgroundColor = UIColor.clear
                cardOptionCell.parameterLabel.textColor = UIColor.lightGray
                
            }
            
            cardOptionCell.parameterLabel.backgroundColor = Styles.Style.grayBackgroundColor
            
            
            
            
            cardOptionCell.parameterLabel.text = asset?.assetSpec.measuredQuantities[indexPath.row].name
            
            return cardOptionCell
            
        }
        else
        {
            let cardOptionCell : DeviceMeasurementsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DeviceMeasurementsCollectionViewCellID", for: indexPath as IndexPath) as! DeviceMeasurementsCollectionViewCell
            
            let measure = measurementsArrayForParameter[indexPath.row]
            cardOptionCell.measurementTimeLabel.text = measure.eventTime.formattedURCLocaleDateString
            cardOptionCell.measurementValueLabel.text = measure.measurementReading
            cardOptionCell.measurementTimeLabel.font = UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 14.0)
            cardOptionCell.measurementValueLabel.font = UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 14.0)
            
            cardOptionCell.measurementValueLabel.textColor = Styles.Style.resolvedAlertColor
            
            if (indexPath.row % 2) == 1
            {
                cardOptionCell.backgroundColor = Styles.Style.grayBackgroundColor
            }
            else
            {
                cardOptionCell.backgroundColor = UIColor.white
            }
            
            if measure.deviceAlertLevel == "Green"
            {
                cardOptionCell.measurementValueLabel.textColor = Styles.Style.resolvedAlertColor
            }
            else if measure.deviceAlertLevel == "Orange"
            {
                cardOptionCell.measurementValueLabel.textColor = Styles.Style.warningAlertColor
            }
            else
            {
                cardOptionCell.measurementValueLabel.textColor = Styles.Style.criticalAlertColor
            }
            
            
            return cardOptionCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedParameter = (asset?.assetSpec.measuredQuantities[indexPath.row])!
        filterMeasurementsForParameter()
        self.parametersCollectionView.reloadData()
        
    }
    
}
