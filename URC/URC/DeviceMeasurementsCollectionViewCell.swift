//
//  DeviceMeasurementsCollectionViewCell.swift
//  URC
//
//  Created by Saran Mahadevan on 07/01/17.
//  Copyright © 2017 Waygum. All rights reserved.
//

import UIKit

class DeviceMeasurementsCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var measurementTimeLabel: UILabel!
    @IBOutlet weak var measurementValueLabel: UILabel!
    
    
}
