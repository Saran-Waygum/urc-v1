//
//  DeviceRegistrationWebServiceManager.swift
//  URC
//
//  Created by Saran Mahadevan on 08/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class DeviceRegistrationWebServiceManager: URCWebServiceManager {
    
    var deviceRegistrationURL = "http://indiadevmisc.cloudapp.net:8080/api/userSubjects/registerLoginDevice/1"
    var alertsArray: Array<AssetMeasurementEvent> = []
    
    internal func loadMeasurementEvents(parameters:[String:AnyObject]!, success : @escaping (_ response: Array<AssetMeasurementEvent>?)->Void, failure :@escaping (_ error : NSError?, _ statusCode: Int) -> Void){
        
        
        loadDataFromService(url: deviceRegistrationURL, parameters: nil , headers: nil , success: {
            (response,resHeaders) in
            
            
            print(response)
            
            success (self.alertsArray)
            
        }, failure: { (error,code) in
            failure(error, code)
        })
        
    }

}
