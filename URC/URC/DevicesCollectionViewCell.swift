//
//  DevicesCollectionViewCell.swift
//  URC
//
//  Created by Saran Mahadevan on 30/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class DevicesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var deviceImageView: UIImageView!
    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var deviceDesciptionLabel: UILabel!
    @IBOutlet weak var deviceMeasurementsLabel: UILabel!
    @IBOutlet weak var deviceMeasuredValuesLabel: UILabel!
    
    @IBOutlet weak var criticalAlertsCount: UILabel!
    @IBOutlet weak var warningsCount: UILabel!
    @IBOutlet weak var resolvedAlertsCount: UILabel!

    @IBOutlet weak var criticalAlertsLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var warningAlertsLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var deviceDescLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var maintanenceAlertsLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var alertsCountContainerViewWidthConstraint: NSLayoutConstraint!
    
    
    class func heightForItem(asset: Asset) -> CGFloat
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let screenWidth = appDelegate.window?.frame.size.width
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: screenWidth!-120, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = asset.deviceDesc
        label.sizeToFit()
        return 120 + label.frame.height
    }
    
    func heightForLabel(constraintedWidth width: CGFloat, font: UIFont, text: String) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
}
