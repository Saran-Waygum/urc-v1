//
//  Extensions.swift
//  URC
//
//  Created by Saran Mahadevan on 30/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import Foundation

extension Date {
    struct Date {
        static let formatterISO8601: DateFormatter = {
            let formatter = DateFormatter()
            formatter.calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.ISO8601) as Calendar!
            formatter.locale = NSLocale.current //NSLocale(localeIdentifier: "en_US_POSIX")
            formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX" //"yyyy-MM-dd'T'HH:mm:ss.SSSX"
            return formatter
        }()
        static let localeURCDateFormatter: DateFormatter = {
            let formatter = DateFormatter()
            let twelveHrLocale = NSLocale.current //NSLocale(localeIdentifier: "en_US_POSIX")
            formatter.locale = twelveHrLocale;
            formatter.dateFormat = "MMM d hh:mm a"
            return formatter
        }()
    }
    var formattedISO8601: String { return Date.formatterISO8601.string(from: self) }
    var formattedURCLocaleDateString : String { return Date.localeURCDateFormatter.string(from: self)}
}


extension String {
    func camelCase() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst()).lowercased()
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.camelCase()
    }
}

extension NSMutableAttributedString
{
    
    
    class func attributedStringWithHTMLString(htmlString:String?, attributes:[String : AnyObject]?) ->NSAttributedString?
    {
        if htmlString?.isEmpty == true
        {
            return nil
        }
        
        let htmlAttrString = try? NSMutableAttributedString(data: htmlString!.data(using: String.Encoding.utf8)!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType], documentAttributes: nil)
        
        if attributes != nil
        {
            htmlAttrString?.addAttributes(attributes!, range: NSMakeRange(0, htmlAttrString?.length ?? 0))
            
            if attributes![NSParagraphStyleAttributeName] == nil
            {
                let style:NSMutableParagraphStyle = NSMutableParagraphStyle.init()
                style.lineBreakMode = NSLineBreakMode.byWordWrapping
                htmlAttrString?.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSMakeRange(0, htmlAttrString?.length ?? 0))
            }
        }
        
        return htmlAttrString
    }
}
