//
//  FilterTableViewCell.swift
//  URC
//
//  Created by Saran Mahadevan on 11/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var filterCriteriaValueLabel: UILabel!
    @IBOutlet weak var isSelectedImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
