//
//  FilterWebServiceManager.swift
//  URC
//
//  Created by Saran Mahadevan on 07/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class FilterWebServiceManager: URCWebServiceManager {
    let fetchGeolocationAndSitesUrl = Constants.getGeoLocationsAndSitesUrl()
    let fetchFilteredAssetGroupsUrl = Constants.getFiteredAssetGroups()
    var geoLocations :Array<Dictionary<String,AnyObject>> = []
    var sites :Array<Site> = []
    var filteredAssetGroupsArray: Array<AssetGroup> = []
    var sitesUrlForOrg = Constants.getOrgBasedSitesUrl()
    
    
    internal func fetchGeolocationAndSitesForFilter(success:@escaping (_ sites:Array<Site>?) -> Void, failure:@escaping (_ error: NSError, _ statusCode: Int) -> Void) {
        
        loadDataFromService(url: fetchGeolocationAndSitesUrl, parameters: nil, headers: nil , success: { (response,resHeaders) -> Void in
            for obj in response {
                self.mapGeolocationAndSiteData(obj: obj as! Dictionary<String, AnyObject>)
            }
            success(self.sites)
            
        }, failure: {
            (error,code) in
            failure(error!,code)
        })
        
    }
    
    internal func fetchSites(orgId: String, success:@escaping (_ sites:Array<Site>?) -> Void, failure:@escaping (_ error: NSError, _ statusCode: Int) -> Void) {
        
        let url = sitesUrlForOrg + orgId
        loadDataFromService(url: url, parameters: nil, headers: nil , success: { (response,resHeaders) -> Void in
            for obj in response {
                self.mapGeolocationAndSiteData(obj: obj as! Dictionary<String, AnyObject>)
            }
            success(self.sites)
            
        }, failure: {
            (error,code) in
            failure(error!,code)
        })
        
    }
    
    internal func getFilteredData(parameters:[String: AnyObject], success:@escaping (_ response: Array<AssetGroup>?) -> Void , failure: @escaping (_ error: NSError, _ statusCode: Int, _ failureReason:String) -> Void){
        postDataToService(url: fetchFilteredAssetGroupsUrl, parameters: parameters, headers: nil, success:
            { (response) -> Void in
                for obj in response {
                    var locationName: String = ""
                    let loc = obj["geoLocationInfo"]
                    if let location = loc as? Dictionary<String, AnyObject> {
                        locationName = location["geoLocationName"] != nil ? location["geoLocationName"] as! String : "Unknown"
                    }
                    
                    self.filteredAssetGroupsArray.append(AssetGroup(
                        name: (obj.value(forKey: "assetGrpName") as? String) == nil ? "Name" : (obj.value(forKey: "assetGrpName") as! String),
                        id: obj.value(forKey: "id") == nil ? 0 : ((obj.value(forKey: "id") as AnyObject).integerValue)!,
                        grpDescription: (obj.value(forKey: "assetGrpDesc") as? String) == nil ? " " : (obj.value(forKey: "assetGrpDesc") as! String),
                        location: locationName,
                        siteId: (obj.value(forKey: "managedSiteId") as? Int) == nil ? 0 : (obj.value(forKey: "managedSiteId") as! Int),
                        imageUrl:(obj.value(forKey: "assetGrpDisplayImageRef") as? String) == nil ? NSURL() : NSURL(string: (obj.value(forKey: "assetGrpDisplayImageRef") as! String))!,
                        criticalAlertsCount: 0,
                        warningsCount: 0,
                        resolvedAlertsCount: 0))
                    
                }
                success (self.filteredAssetGroupsArray)
                
        }, failure: {
            (error,code,failureReason) in
            failure(error!,code,failureReason!)
        })
        
    }
    
    
    func mapGeolocationAndSiteData(obj: Dictionary<String,AnyObject>){
        
        var locName : String = ""
        var locId : Int = 0
        var locLattitude : Double = 0.0
        var locLongitude : Double = 0.0
        var siteId : Int = 0
        var siteName : String = ""
        
        if let geoLocInfo = obj["geoLocationInfo"] {
            if let geoLocName = geoLocInfo["geoLocationName"] as? String{
                locName = geoLocName
            }
            if let geoLocId = geoLocInfo["id"] as? Int {
                locId = geoLocId
            }
            
            if let geoLocLattitude = geoLocInfo["geoLocationCenterLatitude"] as? Double {
                locLattitude = geoLocLattitude
            }
            if let geoLocLongitude = geoLocInfo["geoLocationCenterLongitude"] as? Double {
                locLongitude = geoLocLongitude
            }
        }
        
        if let sId = obj["id"] as? Int {
            siteId = sId
        }
        if let sName = obj["siteName"] as? String{
            siteName = sName
        }
        
        if(siteId != 0){
            self.sites.append(Site(siteId: siteId, siteName: siteName, location: locName, locationId: locId, latitude: locLattitude, longitude: locLongitude))
        }
        
    }
    
}
