//
//  GeoLocationInfo.swift
//  URC
//
//  Created by Saran Mahadevan on 07/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class GeoLocationInfo: NSObject {
    
    
    var geoLocationId : Int = 0
    var geoLocationUUId : String = ""
    var geoLocationName : String = ""
    var geoLocationDesc : String = ""
    var geoLocationPolygon : String = ""
    var geoLocationCenterLatitude : Double = 0.0
    var geoLocationCenterLongitude : Double = 0.0
    
    
    
    
    
    override init() {
        super.init()
        
    }
    
    init(geoLocationId: Int) {
        self.geoLocationId = geoLocationId
    }
}
