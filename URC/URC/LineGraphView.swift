//
//  LineGraphView.swift
//  URC
//
//  Created by Saran Mahadevan on 01/02/17.
//  Copyright © 2017 Waygum. All rights reserved.
//

import UIKit

@IBDesignable class LineGraphView: UIView, UICollectionViewDataSource {


    @IBInspectable var startColor: UIColor = UIColor.white
    @IBInspectable var endColor: UIColor = UIColor.white

    var graphPoints:[Int] = [4, 2, 6, 4, 5, 8, 10]
    var measurementsArray: Array<AssetMeasurementEvent>?
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    */

    override init(frame: CGRect) {
        
        super.init(frame:frame)
        
        backgroundColor = UIColor.red
    }
    
    
    
    init(measurementsArray: Array<AssetMeasurementEvent>, frame: CGRect) {
        
        super.init(frame:frame)
        
        self.measurementsArray = measurementsArray
        
        let screenWidth = frame.size.width
        let cellWidth = floor(screenWidth / CGFloat(measurementsArray.count))
        let margin = (screenWidth - (cellWidth * CGFloat(measurementsArray.count)))/2
        
        
        for i in 0...measurementsArray.count - 1
        {
            let measure = measurementsArray[i]
            let label = UILabel.init(frame: CGRect.init(x: (i * Int.init(cellWidth)) + Int.init(margin) , y: Int(frame.size.height - 20), width: Int(cellWidth), height: 20))
            label.backgroundColor = UIColor.gray
            label.text = measure.displayData
            label.font = UIFont(name: Styles.Style.lightStyleCustomFont, size: 13.0)
            label.textAlignment = .center
            addSubview(label)
        }
        
        
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        
        let width = rect.width
        let height = rect.height
        
        
        
        //set up background clipping area
        let path = UIBezierPath(roundedRect: rect,
                                byRoundingCorners: UIRectCorner.allCorners,
                                cornerRadii: CGSize(width: 8.0, height: 8.0))
        path.addClip()
        
        //2 - get the current context
        let context = UIGraphicsGetCurrentContext()
        let colors = [startColor.cgColor, endColor.cgColor]
        
        //3 - set up the color space
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        //4 - set up the color stops
        let colorLocations:[CGFloat] = [0.0, 1.0]
        
        //5 - create the gradient
        let gradient = CGGradient(colorsSpace: colorSpace,
                                  colors: colors as CFArray,
                                  locations: colorLocations)
        
        //6 - draw the gradient
        let startPoint = CGPoint.zero
        let endPoint = CGPoint(x:0, y:self.bounds.height)
        context!.drawLinearGradient(gradient!,
                                    start: startPoint,
                                    end: endPoint,
                                    options: CGGradientDrawingOptions(rawValue: 0))
        
        
        let margin:CGFloat = 20.0
        var columnXPoint = { (column:Int) -> CGFloat in
            //Calculate gap between points
            let spacer = (width - margin*2 - 4) /
                CGFloat((self.graphPoints.count - 1))
            var x:CGFloat = CGFloat(column) * spacer
            x += margin + 2
            return x
        }
        
        let topBorder:CGFloat = 60
        let bottomBorder:CGFloat = 50
        let graphHeight = height - topBorder - bottomBorder
        let maxValue = graphPoints.max()
        let columnYPoint = { (graphPoint:Int) -> CGFloat in
            var y:CGFloat = CGFloat(graphPoint) /
                CGFloat(maxValue!) * graphHeight
            y = graphHeight + topBorder - y // Flip the graph
            return y
        }
        

        
        //set up the points line
        let graphPath = UIBezierPath()
        //go to start of line
        
        let a = columnXPoint(0)
        let b = columnYPoint(graphPoints[0])
        graphPath.move(to: CGPoint(x: a,
                                   y:b))
        
        //add points for each item in the graphPoints array
        //at the correct (x, y) for the point
        
//        graphPath.lineWidth = 2.0
//        graphPath.stroke()
        
        for i in 1..<graphPoints.count {
            let nextPoint = CGPoint(x:columnXPoint(i),
                                    y:columnYPoint(graphPoints[i]))
            graphPath.addLine(to: nextPoint)
            
            if i%2 == 0
            {
                
                Styles.Style.resolvedAlertColor.setFill()
                Styles.Style.resolvedAlertColor.setStroke()
            }
            else
            {
                UIColor.white.setFill()
                UIColor.white.setStroke()
            }
            graphPath.stroke()
            
        }
        
        
        //draw the line on top of the clipped gradient
        graphPath.lineWidth = 2.0
        graphPath.stroke()
        
        //Draw the circles on top of graph stroke
        for i in 0..<graphPoints.count {
            var point = CGPoint(x:columnXPoint(i), y:columnYPoint(graphPoints[i]))
            point.x -= 5.0/2
            point.y -= 5.0/2
            
            let circle = UIBezierPath(ovalIn:
                CGRect(origin: point,
                       size: CGSize(width: 5.0, height: 5.0)))
            circle.fill()
        }
        
        
        //        graphPath.stroke()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        return UICollectionViewCell()
    }


}
