//
//  LoginViewController.swift
//  URC
//
//  Created by Saran Mahadevan on 27/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

import UIKit
import MessageUI
protocol LoginViewControllerDelegate{
    func backToActiveViewController()
}
class LoginViewController: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginActivity: UIActivityIndicatorView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var scrollView : UIScrollView?
    
    var username: String = ""
    var password: String = ""
    var delegate : LoginViewControllerDelegate?
    
    @IBAction func loginPressed(_ sender: Any) {
        
        
        self.username = self.userNameTextField.text!
        self.password = self.passwordTextField.text!
        
        if (self.username.isEmpty || self.password.isEmpty){
            errorLabel.isHidden = false
            self.errorLabel.text = " " //Constants.Labels.kUSER_PASSWORD_EMPTY.rawValue
        }else{
            errorLabel.isHidden = true;
            self.errorLabel.text = ""
            self.loadLoginDetailsFromServer(username: self.username, password: self.password)
            self.loginActivity.isHidden = false
            self.loginActivity.startAnimating()
            self.loginButton.isEnabled = false
        }
        
    }

    
    @IBAction func SignUp (sender: UIButton){
        
        /*Tracking Event - Google Analytics -Start*/
//        let tracker = GAI.sharedInstance().defaultTracker
//        let builder = GAIDictionaryBuilder.createEventWithCategory("App", action: "Sign Up", label: "Application Sign up", value: nil)
//        tracker.send(builder.build() as [NSObject : AnyObject])
        /*Tracking Event - Google Analytics -End*/
        
        //If mail account is configured, open up mail, if not show an alert with the message needed for signup
        if (MFMailComposeViewController.canSendMail()){
            let mailComposeView = MFMailComposeViewController()
            mailComposeView.mailComposeDelegate = self
            mailComposeView.setSubject(Constants.SignUpMailOptoins.Subject)
            mailComposeView.setToRecipients(Constants.SignUpMailOptoins.ToRecipients)
            mailComposeView.setMessageBody(Constants.SignUpMailOptoins.MessageBody, isHTML: false)
            present(mailComposeView, animated: true, completion: nil)
            
        }else{
            let sendMailErrorAlert = UIAlertController(title: Constants.SignUpMailOptoins.Subject, message: Constants.SignUpMailOptoins.AlternativeMessage, preferredStyle: .alert)
            sendMailErrorAlert.addAction(UIAlertAction(title: Constants.Labels.kALERT_OK_LABEL.rawValue, style: .default, handler: nil))
            self.present(sendMailErrorAlert, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loginActivity.isHidden = true
        errorLabel.textColor = Styles.Style.appErrorLabelTextColor
        if(URCManager.sharedInstance.sessionExpired){
            errorLabel.isHidden = false
            errorLabel.text = Constants.Labels.kSESSION_EXPIRED_MES.rawValue
        }else{
            errorLabel.isHidden = true
        }
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(LoginViewController.hideKeyboard(recognizer:)))
        tapGesture.delegate = self
        tapGesture.cancelsTouchesInView = false
        self.scrollView!.addGestureRecognizer(tapGesture)
        self.scrollView!.contentInset = UIEdgeInsets.zero
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(notification:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide(notification:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        userNameTextField.delegate = self
        passwordTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        userNameTextField.text = ""
        passwordTextField.text = ""
    }
    
    // MARK: - Login Service Call
    func loadLoginDetailsFromServer(username:String, password:String){
        
        let loginServiceManager = LoginWebServiceManager()
        
        DispatchQueue.global().async { [weak self] () -> Void in
            
            loginServiceManager.loadLoginDetails(username: username,password: password,success: {
                (response) in
                
                
                DispatchQueue.main.async { () -> Void in
                    
                    self!.loginActivity.isHidden = true
                    self!.loginActivity.stopAnimating()
                    self!.loginButton.isEnabled = true
                    
                    self!.presentCurrentActiveViewController()
                }
            }, failure: {
                (error,code) in
                
                var errorMes : String = ""
                if(code == Constants.StatusCodes.Unauthorized.rawValue)
                {
                    errorMes = Constants.Labels.kINVALID_LOGIN_CREDENTIALS.rawValue
                }
                else{
                    if Reachability.isConnectedToNetwork() == true {
                        errorMes = Constants.Labels.kSERVER_ERROR_MESSAGE.rawValue
                    } else {
                        errorMes = Constants.Labels.kNO_ITERNET_CONNECTION_MESSAGE.rawValue
                    }
                }
                
                DispatchQueue.main.async { () -> Void in
                    self!.errorLabel.text = errorMes
                    self!.errorLabel.isHidden = false
                    self!.errorLabel.textColor = Styles.Style.appErrorLabelTextColor
                    self!.loginActivity.isHidden = true
                    self!.loginActivity.stopAnimating()
                    self!.loginButton.isEnabled = true
                }
            })
        }
        
    }
    
    
    func presentCurrentActiveViewController(){
        //For login from first present the container view controller .
        // If the session expired when the user is using the app - redirect to login page and on login redirect the user back to page they were on.
        if(URCManager.sharedInstance.sessionExpired){
            URCManager.sharedInstance.sessionExpired = false
            self.dismiss(animated: true, completion: nil)
            if delegate != nil {
                delegate?.backToActiveViewController()
            }
        }
        else{
            presentDashboardViewController()
        }
    }
    
    func presentDashboardViewController(){
//        let containerViewController = LoginViewController()
//        self.present(containerViewController, animated: true, completion: nil )
//        let main = MainTabBarController()
//        self.present(main, animated: true, completion: nil )
        
        let vc : MainTabBarController = self.storyboard!.instantiateViewController(withIdentifier: "MainTabBarControllerID") as! MainTabBarController
        self.present(vc, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView!.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView!.contentInset = contentInset
        
        
    }
    
    func keyboardWillHide(notification:NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        var contentInset:UIEdgeInsets = self.scrollView!.contentInset
        contentInset.bottom = contentInset.bottom - (keyboardFrame.size.height)
        self.scrollView!.contentInset = contentInset
        
    }
    
    func hideKeyboard(recognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }
    
}



//MARK: Mail Delegate
extension LoginViewController: MFMailComposeViewControllerDelegate{
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        dismiss(animated: true, completion: nil)
    }
}
