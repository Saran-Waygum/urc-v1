//
//  LogoutWebServiceManager.swift
//  URC
//
//  Created by Saran Mahadevan on 12/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class LogoutWebServiceManager: URCWebServiceManager {
    
    let logoutServiceURL = Constants.getLogoutUrl()
    let userDefaults = UserDefaults.standard
    
    var headersDictionary = [String:String]()
    
    var parametersDictionary = [String:String]()
    
    internal func logoutUser(success :  @escaping (_ response: String?)->Void, failure :@escaping (_ error: NSError?,_ statusCode: Int) -> Void){
        
        
        let token = userDefaults.string(forKey: "COM_WAYGUM_URC_APP_IDENTITY_ACCESS_TOKEN") == nil ? "" : userDefaults.string(forKey: "COM_WAYGUM_URC_APP_IDENTITY_ACCESS_TOKEN")!
        let refreshToken = userDefaults.string(forKey: "COM_WAYGUM_URC_APP_IDENTITY_REFRESH_TOKEN") == nil ? "" : userDefaults.string(forKey: "COM_WAYGUM_URC_APP_IDENTITY_REFRESH_TOKEN")!
        
        parametersDictionary.updateValue(refreshToken, forKey: "refreshToken")
        
        headersDictionary.updateValue("Content-Type", forKey: "application/json")
        headersDictionary.updateValue("Authorization", forKey: "Bearer \(token)")
        
        userDefaults.removeObject(forKey: "COM_WAYGUM_URC_APP_IDENTITY_ACCESS_TOKEN")
        userDefaults.removeObject(forKey: "COM_WAYGUM_URC_APP_IDENTITY_REFRESH_TOKEN")
        userDefaults.removeObject(forKey: "COM_WAYGUM_URC_APP_USER_DATA")
        
        logoutFromService(url: logoutServiceURL, parameters: parametersDictionary , headers:headersDictionary, success: {
            (response) in
            
            success ("success")
            
        }, failure: { (error,code) in
            failure(error, code)
        })
    }
    
}
