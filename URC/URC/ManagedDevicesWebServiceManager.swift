//
//  ManagedDevicesWebServiceManager.swift
//  URC
//
//  Created by Saran Mahadevan on 09/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class ManagedDevicesWebServiceManager: URCWebServiceManager {
    
    var assetsServiceURL = Constants.getAssetGroupUrl()
    var deviceServiceURL = Constants.getAssetsUrl()
    var assetsArray: Array<Asset> = []
    
    
    
    internal func loadDevices(parameters:[String:String]!, success : @escaping (_ response: Array<Asset>?)->Void, failure :@escaping (_ error : NSError?, _ statusCode: Int) -> Void){
        
        
        if parameters["assetGroupId"] != ""
        {
            assetsServiceURL = assetsServiceURL + "/" + parameters["assetGroupId"]! + "/managedDevices"
        }

        
        loadDataFromService(url: assetsServiceURL, parameters: nil , headers: nil , success: {
            (response,resHeaders) in
            
            for assetObj in response{
                var assetSpec : AssetSpecification = AssetSpecification()
                var measurementEvent : Array<AssetMeasurementEvent> = []
                
                if let assetMeaseurementObj = assetObj["measurementEventDTO"]{
                    measurementEvent = self.mapMeasurementEvent(assetObj: assetMeaseurementObj as! Dictionary<String, AnyObject>)
                }
                if let assetSpecObj = assetObj["deviceSpecification"]{
                    if let alertData = assetObj["readingNames"]
                    {
                        assetSpec = self.mapAssetSpec(assetSpecObj: assetSpecObj as! Dictionary<String, AnyObject>, readingNames: alertData as! Array<Dictionary<String, Any>>)
                    }
                    else
                    {
                        assetSpec = self.mapAssetSpec(assetSpecObj: assetSpecObj as! Dictionary<String, AnyObject>, readingNames: [])
                    }
                    
                }
                let assetExtraInfo : AssetExtraInfo = self.mapAssetExtraInfo(assetObj: assetObj as! Dictionary<String, AnyObject>)
                
                self.assetsArray.append(Asset(
                    name: (assetObj["deviceName"] as? String) == nil ? "Name" : (assetObj["deviceName"] as! String),
                    id: assetObj["id"] == nil ? 0 : assetObj["id"] as! Int,
                    assetUUId: (assetObj["deviceUUId"] as? String) == nil ? "" : (assetObj["deviceUUId"] as! String),
                    deviceDesc: (assetObj["deviceDesc"] as? String) == nil ? "" : (assetObj["deviceDesc"] as! String),
                    assetMeasurementEvent: measurementEvent,
                    assetSpec: assetSpec,
                    assetExtraInfo: assetExtraInfo,
                    assetExternalId: (assetObj["deviceExternalId"] as? String) == nil ? "" : (assetObj["deviceExternalId"] as! String),
                    assetGroupId: assetObj["managedAssetGroupId"] == nil ? 0 : (assetObj["managedAssetGroupId"] as! Int),
                    criticalAlertCount:assetObj["criticalAlertCount"] == nil ? 0 : (assetObj["criticalAlertCount"] as! Int),
                    warningAlertCount:assetObj["warningAlertCount"] == nil ? 0 : (assetObj["warningAlertCount"] as! Int),
                    resolvedAlertCount:assetObj["resolvedAlertCount"] == nil ? 0 : (assetObj["resolvedAlertCount"] as! Int)))
                
            }
            success (self.assetsArray)
            
        }, failure: { (error,code) in
            failure(error, code)
        })
        
    }
    
    
    
    internal func loadDevicesForAssets(parameters:[String:String]!, success : @escaping (_ response: Array<Asset>?)->Void, failure :@escaping (_ error : NSError?, _ statusCode: Int) -> Void){
 
        
        if let assets = parameters["assetGrpId"]
        {
            deviceServiceURL = deviceServiceURL + "?" + assets
        }
        
        
        loadDataFromService(url: deviceServiceURL, parameters: nil , headers: nil , success: {
            (response,resHeaders) in
            
            for assetObj in response{
                var assetSpec : AssetSpecification = AssetSpecification()
                var measurementEvent : Array<AssetMeasurementEvent> = []
                
                if let assetMeaseurementObj = assetObj["measurementEventDTO"]{
                    measurementEvent = self.mapMeasurementEvent(assetObj: assetMeaseurementObj as! Dictionary<String, AnyObject>)
                }
                if let assetSpecObj = assetObj["deviceSpecification"]{
                    if let alertData = assetObj["readingNames"]
                    {
                        assetSpec = self.mapAssetSpec(assetSpecObj: assetSpecObj as! Dictionary<String, AnyObject>, readingNames: alertData as! Array<Dictionary<String, Any>>)
                    }
                    else
                    {
                        assetSpec = self.mapAssetSpec(assetSpecObj: assetSpecObj as! Dictionary<String, AnyObject>, readingNames: [])
                    }
                    
                }
                let assetExtraInfo : AssetExtraInfo = self.mapAssetExtraInfo(assetObj: assetObj as! Dictionary<String, AnyObject>)
                
                
                self.assetsArray.append(Asset(
                    name: (assetObj["deviceName"] as? String) == nil ? "Name" : (assetObj["deviceName"] as! String),
                    id: assetObj["id"] == nil ? 0 : assetObj["id"] as! Int,
                    assetUUId: (assetObj["deviceUUId"] as? String) == nil ? "" : (assetObj["deviceUUId"] as! String),
                    deviceDesc: (assetObj["deviceDesc"] as? String) == nil ? "" : (assetObj["deviceDesc"] as! String),
                    assetMeasurementEvent: measurementEvent,
                    assetSpec: assetSpec,
                    assetExtraInfo: assetExtraInfo,
                    assetExternalId: (assetObj["deviceExternalId"] as? String) == nil ? "" : (assetObj["deviceExternalId"] as! String),
                    assetGroupId: assetObj["managedAssetGroupId"] == nil ? 0 : (assetObj["managedAssetGroupId"] as! Int),
                    criticalAlertCount:assetObj["criticalAlertCount"] == nil ? 0 : (assetObj["criticalAlertCount"] as! Int),
                    warningAlertCount:assetObj["warningAlertCount"] == nil ? 0 : (assetObj["warningAlertCount"] as! Int),
                    resolvedAlertCount:assetObj["resolvedAlertCount"] == nil ? 0 : (assetObj["resolvedAlertCount"] as! Int)))
                
                
            }
            success (self.assetsArray)
            
        }, failure: { (error,code) in
            failure(error, code)
        })
        
    }
    
    
    func mapMeasurementEvent (assetObj : Dictionary<String, AnyObject>) -> Array<AssetMeasurementEvent>{
        
        var measurementReading : String = ""
        var signalStrength : String = ""
        var batteryStrength : String = ""
        var eventTime : Date = Date()
        var displayData : String = ""
        
        var measurementEventsArray : Array<AssetMeasurementEvent> = []
        
        if let mr = assetObj["measurementReadings"] as? Array<Dictionary<String, Any>> {
            
            for readings in mr
            {
                let readin = readings["readingValue"] as! Float
                
                measurementEventsArray.append(AssetMeasurementEvent(eventTime: eventTime, batteryStrength: batteryStrength, signalStrength: signalStrength, measurementReading: String(format: "%.2f", readin), displayData: displayData))
            }
        }
        
        
        //        if let mr = assetObj["measurementReading"] as? Int {
        //            measurementReading = String(Float(mr))
        //        }
        //        if let dd = assetObj["displayData"] as? String{
        //            let result = dd.replacingOccurrences(of: "+", with: " ")
        //            //            let decodedResponse = result.stringByRemovingPercentEncoding
        //            let decodedResponse = result
        //            if( decodedResponse != ""){
        //                displayData = decodedResponse
        //            }
        //        }
        //        if let ss = assetObj["signalStrength"] as? Int{
        //            signalStrength = String(ss)
        //        }
        //
        //        if let bs = assetObj["batteryStrength"] as? Int{
        //            batteryStrength = String(bs)
        //        }
        //        //        if let et = assetObj["eventTime"] as? String {
        //        //            let time = Date.Date().formattedISO8601.dateFromString(et)!
        //        //            eventTime = time.formattedURCLocaleDateString
        //        //        }
        //
        //        let measurementEvent = AssetMeasurementEvent(eventTime: eventTime, batteryStrength: batteryStrength, signalStrength: signalStrength, measurementReading: measurementReading, displayData: displayData)
        
        return measurementEventsArray
    }
    
    func mapAssetSpec(assetSpecObj : Dictionary<String, AnyObject>, readingNames: Array<Dictionary<String, Any>>) -> AssetSpecification{
        
        
        var assetImageUrl : NSURL = NSURL()
        var assetGraphUrl:String = ""
        var dataTableUrl: String = ""
        var assetPlatform : String = ""
        var assetType : String = ""
        var oemContact : AssetContact = AssetContact(name:"", address: "", website: "", phoneNumbers: [], emailIds: [])
        var manuals : Array<AssetDocuments> = []
        var extraInfo: [String: AnyObject] = [:]
        var measuredQuantityName :String = ""
        var measuredQuantitySymbol :String = ""
        var measuredQuantities : Array<AssetSpecMeasurementQuantity> = []
        var specExtraInfo : AssetSpecificationExtraInfo = AssetSpecificationExtraInfo()
        var id : Int = 0
        var uuid: String = ""
        var name: String = ""
        
        if let specId = assetSpecObj["id"] {
            id = specId as! Int
        }
        if let specUUID = assetSpecObj["deviceSpecUUId"] {
            uuid = (specUUID as? String != nil) ? specUUID as! String : ""
        }
        if let specName = assetSpecObj["deviceSpecName"] {
            name = specName as! String
        }
        
        
        if let deviceMeasuredQnantities = assetSpecObj["deviceMeasuredQuantities"] as? Array<Dictionary<String, Any>>
        {
            
            for deviceData in deviceMeasuredQnantities
            {
                if let mq = deviceData["measuredQuantity"] as? Dictionary<String, Any> {
                    measuredQuantityName = ( (deviceData["readingName"] != nil) &&  ((deviceData["readingName"] as! String) != "None")) ? deviceData["readingName"] as! String : ""
                    measuredQuantitySymbol = ((mq["quantityUnitSymbol"] != nil) && ((mq["quantityUnitSymbol"] as! String) != "None" )) ? mq["quantityUnitSymbol"] as! String : ""
                    
                    let aq = AssetSpecMeasurementQuantity(name: measuredQuantityName, unitSymbol: measuredQuantitySymbol)
                    
                    for alert in readingNames
                    {
                        if alert["measuredReadingName"] as? String == deviceData["readingName"] as? String
                        {
                            aq.alertLevel = alert["alertLevel"]! as! String
                        }
                    }
                    aq.id = deviceData["id"] as! Int
                    
                    measuredQuantities.append(aq)
                }
                
                
            }
        }
        
        
        //        if let mq = assetSpecObj["measuredQuantity"] {
        //            measuredQuantityName = ( (mq["quantityName"] != nil) &&  ((mq["quantityName"] as! String) != "None")) ? mq["quantityName"] as! String : ""
        //            measuredQuantitySymbol = ((mq["quantityUnitSymbol"] != nil) && ((mq["quantityUnitSymbol"] as! String) != "None" )) ? mq["quantityUnitSymbol"] as! String : ""
        //            measuredQuantity = AssetSpecMeasurementQuantity(name: measuredQuantityName, unitSymbol: measuredQuantitySymbol)
        //        }
        
        
        if let info = assetSpecObj["deviceSpecExtraInfo"] as? String{
            if(info != ""){
                let data = info.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                
                do {
                    extraInfo = try (JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject])
                } catch {
                    print("Failed to load: \(error)")
                }
                
                if let platform = extraInfo["platform"]{
                    assetPlatform = platform as! String
                }
                if let type = extraInfo["type"]{
                    assetType = type as! String
                }
                if let graph = extraInfo["Graph"]{
                    assetGraphUrl = graph["URL"] == nil ? "" : graph["URL"] as! String
                }
                if let data = extraInfo["Data"]{
                    dataTableUrl = data["URL"] == nil ? "" : data["URL"] as! String
                }
                if let oem = extraInfo["OEMHelpline"]{
                    oemContact = self.mapAssetContactDetails(contactObj: oem as! Dictionary<String, AnyObject>)
                }
                if let assetManual = extraInfo["Manuals"]{
                    manuals = self.mapManualsObj(manualObj: assetManual as! Array<Dictionary<String,AnyObject>>)
                }
                specExtraInfo = AssetSpecificationExtraInfo(platform: assetPlatform, type: assetType, graphUrl: assetGraphUrl, dataTableUrl: dataTableUrl, assetOEMContactDetails: oemContact, assetManuals: manuals)
            }
        }
        
        assetImageUrl = (assetSpecObj["deviceDisplayImageRef"] as? String) == nil ? NSURL() : NSURL(string: (assetSpecObj["deviceDisplayImageRef"] as! String))!
        
        let assetSpec = AssetSpecification(id: id,name: name, UUID: uuid, displayImageRef: assetImageUrl, measuredQuantities: measuredQuantities, extraInfo: specExtraInfo)
        
        return assetSpec
        
    }
    
    
    func mapAssetExtraInfo(assetObj : Dictionary<String, AnyObject>) -> AssetExtraInfo{
        
        var assetExtraInfo : AssetExtraInfo = AssetExtraInfo()
        var ownerContact: AssetContact = AssetContact()
        var assetConfiguration : AssetConfiguration = AssetConfiguration()
        var assetMachineDetails : AssetMachineDetails = AssetMachineDetails()
        var extraInfo: [String: AnyObject] = [:]
        
        if let info = assetObj["deviceExtraInfo"] as? String{
            
            let data = info.data(using: String.Encoding.utf8, allowLossyConversion: false)!
            
            do {
                extraInfo = try (JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject])
            } catch {
                print("Failed to load: \(error)")
            }
            
            
            if let owner = extraInfo["OwnershipDetails"]{
                ownerContact = self.mapAssetContactDetails(contactObj: owner as! Dictionary<String, AnyObject>)
            }
            
            if let assetConfig = extraInfo["Configuration"]{
                
                assetConfiguration = mapAssetConfiguration(assetConfig: assetConfig as! Dictionary<String, AnyObject>)
            }
            
            if let machineDetails = extraInfo["EquipmentDetails"]{
                assetMachineDetails = mapAssetMachineDetails(machineDetails: machineDetails as! Dictionary<String, AnyObject>)
            }
        }
        
        assetExtraInfo = AssetExtraInfo(extraInfoObj: extraInfo, assetOwnerContactDetails: ownerContact, assetConfiguration: assetConfiguration, assetMachineDetails: assetMachineDetails)
        
        return assetExtraInfo
    }
    
    
    func mapAssetMachineDetails(machineDetails : Dictionary<String, AnyObject>) ->AssetMachineDetails{
        
        let assetMachDetails = AssetMachineDetails(
            make: (machineDetails["Make"] as? String) == nil ? "" : (machineDetails["Make"] as! String),
            model: (machineDetails["Model"] as? String) == nil ? "" : (machineDetails["Model"] as! String),
            serialNo: (machineDetails["SerialNumber"] as? String) == nil ? "" : (machineDetails["SerialNumber"] as! String),
            location: (machineDetails["SensorLocation"] as? String) == nil ? "" : (machineDetails["SensorLocation"] as! String),
            assetdescriptionId: (machineDetails["SensorDescription"] as? Int) == nil ? 0 : (machineDetails["SensorDescription"] as! Int),
            notes: (machineDetails["Notes"] as? String) == nil ? "" : (machineDetails["Notes"] as! String))
        return assetMachDetails
    }
    
    
    
    func mapAssetConfiguration(assetConfig : Dictionary<String, AnyObject>) ->AssetConfiguration{
        
        var hbInterval: Int = 0
        var awareSateHeartbeat : Int = 0
        let checkDigit : String = ""
        if let heartBeatInterval = assetConfig["HeartBeatInterval"] as? Int{
            hbInterval = heartBeatInterval
        }
        if let awareStateHB = assetConfig["AwareStateHeartbeat"] as? Int{
            awareSateHeartbeat = awareStateHB
        }
        
        let assetConfiguration : AssetConfiguration = AssetConfiguration(assetHeartbeatInterval: hbInterval, assetAwareStateHB: awareSateHeartbeat, checkDigit: checkDigit)
        
        return assetConfiguration
        
    }
    
    func mapAssetContactDetails(contactObj:Dictionary<String,AnyObject>) -> AssetContact{
        let name = contactObj["Name"] == nil ? "" : contactObj["Name"] as! String
        let add = contactObj["Address"] == nil ? "" : contactObj["Address"] as! String
        let website = contactObj["Website"] == nil ? "" : contactObj["Website"] as! String
        var phone : Array<String> = []
        var mail : Array<String> = []
        if let phoneNumbers = contactObj["Phone"] as? Array<String>{
            phone = phoneNumbers
        }
        if let mailIds = contactObj["Email"] as? Array<String>{
            mail = mailIds
        }
        let contact : AssetContact = AssetContact(name:name,address: add, website: website, phoneNumbers: phone, emailIds: mail)
        return contact
    }
    
    
    func mapManualsObj(manualObj : Array<Dictionary<String,AnyObject>>)-> Array<AssetDocuments>{
        var manuals : Array<AssetDocuments> = []
        var docName: String = ""
        var docType: String = ""
        var docUrl: String = ""
        var purpose: String = ""
        for(_,manual) in manualObj.enumerated()
        {
            if let name = manual["Name"] as? String{
                docName = name
            }
            if let type = manual["Type"] as? String{
                docType = type
            }
            if let url = manual["URL"] as? String{
                docUrl = url
            }
            if let docpurpose = manual["Purpose"] as? String{
                purpose = docpurpose
            }
            manuals.append(AssetDocuments(name: docName, type: docType, docUrl: docUrl, purpose: purpose))
        }
        return manuals
    }

    
    
}
