//
//  MeasurementEventsWebServiceManager.swift
//  URC
//
//  Created by Saran Mahadevan on 08/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class MeasurementEventsWebServiceManager: URCWebServiceManager {
    
    var measuredEventURL = "http://indiadevmisc.cloudapp.net:8080/api/measurementEvents"
    var measurementsArray: Array<AssetMeasurementEvent> = []
    var isLatestReading : Bool = false
    
    
    
    internal func loadMeasurementEvents(parameters:[String:String]!, success : @escaping (_ response: Array<AssetMeasurementEvent>?)->Void, failure :@escaping (_ error : NSError?, _ statusCode: Int) -> Void){
        
        
        if let deviceId = parameters["deviceId"]
        {
            measuredEventURL = measuredEventURL + "?deviceId=" + deviceId
        }
        if let isLatest = parameters["isLatestReading"]
        {
            if isLatest == "true"
            {
                isLatestReading = true
            }
            else
            {
                isLatestReading = false
            }
            
        }

        
        loadDataFromService(url: measuredEventURL, parameters: nil , headers: nil , success: {
            (response,resHeaders) in
            
            
            for measure in response
            {
                
                let signalStrength : String = ""
                let batteryStrength : String = ""
                var eventTime : Date = Date()
                let displayData : String = ""
                
                let managedDevice = measure["managedDevice"] as! Dictionary<String, Any>
                let deviceSpecification = managedDevice["deviceSpecification"] as! Dictionary<String, Any>
                let deviceMeasuredQuantities = deviceSpecification["deviceMeasuredQuantities"] as! Array<Dictionary<String, Any>>
                
   
                
                
                let measurementEvents = measure["measurementEvents"] as! Array<Dictionary<String, Any>>
                
                if self.isLatestReading
                {
                    let latestEvent = measurementEvents[0]
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX"
                    
                    eventTime = dateFormatter.date(from: latestEvent["eventTime"] as! String)!
                    
                    let readings = latestEvent["measurementReadings"] as! Array<Dictionary<String, Any>>
                    
                    
                    
                    for index in 0...deviceMeasuredQuantities.count - 1
                    {
                        
                        let reading = readings[index]
                        let measure = deviceMeasuredQuantities[index]
                        let meas = measure["measuredQuantity"] as! Dictionary<String, Any>
                        
                        let measureId = measure["id"] as! Int
                        
                        let measureEvent = AssetMeasurementEvent(eventTime: eventTime, batteryStrength: batteryStrength, signalStrength: signalStrength, measurementReading: String.init(format: "%@ %@", reading["readingValue"] as! CVarArg, meas["quantityUnitSymbol"] as! String), displayData: meas["quantityName"] as! String)
                        measureEvent.id = measureId
                        measureEvent.deviceAlertLevel = latestEvent["measurementState"]! as! String
                        
                        self.measurementsArray.append(measureEvent)
                    }
                }
                else
                {
                    
                    for latestEvent in measurementEvents
                    {
                        let readings = latestEvent["measurementReadings"] as! Array<Dictionary<String, Any>>
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX"
                        
                         eventTime = dateFormatter.date(from: latestEvent["eventTime"] as! String)!
                        
                        for index in 0...deviceMeasuredQuantities.count - 1
                        {
                            
                            let reading = readings[index]
                            let measure = deviceMeasuredQuantities[index]
                            let meas = measure["measuredQuantity"] as! Dictionary<String, Any>
                            
                            let measureId = measure["id"] as! Int
                            let measureEvent = AssetMeasurementEvent(eventTime: eventTime, batteryStrength: batteryStrength, signalStrength: signalStrength, measurementReading: String.init(format: "%@ %@", reading["readingValue"] as! CVarArg, meas["quantityUnitSymbol"] as! String), displayData: meas["quantityName"] as! String)
                            measureEvent.id = measureId
                            measureEvent.deviceAlertLevel = latestEvent["measurementState"]! as! String
                            
                            
                            self.measurementsArray.append(measureEvent)
                        }
                    }
                    
                    
                    
                }
                
                
            }

            success (self.measurementsArray)
            
        }, failure: { (error,code) in
            failure(error, code)
        })
        
    }

}
