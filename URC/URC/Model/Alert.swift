//
//  Alert.swift
//  URC
//
//  Created by Saran Mahadevan on 07/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class Alert: NSObject {
    
    
    var alertId : Int = 0
    var alertUUId : String = ""
    var alertLevel : String = ""
    var alertExternalId : String = ""
    var alertExtraInfo : String = ""
    var alertValue : String = ""
    var state : String = ""
    var dateGenerated : String = ""
    var dateUpdated : String = ""
    var organizationId : Int = 0
    var alertRule: AlertRule = AlertRule()
    var managedAssetGroup: AssetGroup = AssetGroup()
    var managedDevice: Asset = Asset()
    
    var generatedDate : NSDate = NSDate()
    var updatedDate : NSDate = NSDate()
    
    // Extra Stuff
    
    var deviceName: String = ""
    
    
    
    
    override init() {
        super.init()
        
    }
    
    init(alertId: Int) {
        self.alertId = alertId
    }
}
