//
//  Asset.swift
//  URC
//
//  Created by Saran Mahadevan on 28/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class Asset: NSObject {
    var name : String = ""
    var id : Int = 0
    var assetUUId : String = ""
    var deviceDesc : String = ""
    var assetMeasurements : Array<AssetMeasurementEvent>  = []
    var assetSpec : AssetSpecification = AssetSpecification()
    var assetExtraInfo : AssetExtraInfo = AssetExtraInfo()
    var assetExternalId : String = "";
    var assetGroupId : Int = 0;
    var criticalAlertCount : Int = 0
    var warningAlertCount : Int = 0
    var resolvedAlertCount : Int = 0
    var maintenanceAlertCount : Int = 0
    var deviceDisplayImageRef : String = ""
    
    override init(){
        super.init()
    }
    
    init(name : String, id : Int, assetUUId : String, deviceDesc : String, assetMeasurementEvent : Array<AssetMeasurementEvent>, assetSpec : AssetSpecification?, assetExtraInfo : AssetExtraInfo?, assetExternalId : String, assetGroupId : Int, criticalAlertCount: Int, warningAlertCount: Int, resolvedAlertCount: Int){
        super.init()
        self.name = name
        self.id = id
        self.assetUUId = assetUUId
        self.deviceDesc = deviceDesc
        self.assetMeasurements = assetMeasurementEvent
        self.assetSpec = assetSpec!
        self.assetExtraInfo = assetExtraInfo!
        self.assetExternalId = assetExternalId
        self.assetGroupId = assetGroupId
        self.resolvedAlertCount = resolvedAlertCount
        self.criticalAlertCount = criticalAlertCount
        self.warningAlertCount = warningAlertCount
    }
}

class AssetSpecMeasurementQuantity : NSObject {
    var name : String = ""
    var unitSymbol : String = ""
    var id : Int = 0
    var alertLevel : String = ""
    
    override init(){
        super.init()
    }
    
    init(name : String, unitSymbol : String) {
        super.init()
        self.name = name
        self.unitSymbol = unitSymbol
    }
}
class AssetSpecificationExtraInfo : NSObject{
    var platform : String = ""
    var type : String = ""
    var graphUrl : String = ""
    var dataTableUrl: String = ""
    var assetOEMContactDetails : AssetContact = AssetContact()
    var assetManuals : Array<AssetDocuments>?
    
    
    override init(){
        super.init()
    }
    
    init(platform : String, type : String, graphUrl : String, dataTableUrl: String, assetOEMContactDetails : AssetContact?, assetManuals : Array<AssetDocuments>?) {
        self.platform = platform
        self.type = type
        self.graphUrl = graphUrl
        self.dataTableUrl = dataTableUrl
        self.assetOEMContactDetails = assetOEMContactDetails!
        self.assetManuals = assetManuals
    }
    
}
class AssetDocuments: NSObject {
    var name:String = ""
    var type:String = ""
    var docUrl:String = ""
    var purpose:String = ""
    
    override init(){
        super.init()
    }
    
    init(name:String, type:String, docUrl:String, purpose: String){
        super.init()
        self.name = name
        self.type = type
        self.docUrl = docUrl
        self.purpose = purpose
    }
}

class AssetContact: NSObject {
    var name:String = ""
    var address:String = ""
    var website:String = ""
    var phoneNumbers: Array<String> = []
    var emailIds: Array<String> = []
    
    override init(){
        super.init()
    }
    
    init(name:String, address:String, website:String, phoneNumbers: Array<String>, emailIds:Array<String>){
        super.init()
        self.name = name
        self.address = address
        self.website = website
        self.phoneNumbers = phoneNumbers
        self.emailIds = emailIds
    }
}

class AssetMeasurementEvent : NSObject {
    var batteryStrength : String = "";
    var signalStrength : String = "";
    var measurementReading : String = ""
    var batteryStrengthImage : UIImage?
    var signalStrengthImage : UIImage?
    var eventTime : Date = Date()
    var displayData : String = ""
    var id: Int = 0
    var deviceAlertLevel : String = ""
    
    override init(){
        super.init()
    }
    
    init(eventTime: Date, batteryStrength : String, signalStrength : String, measurementReading : String, displayData : String) {
        super.init()
        self.eventTime = eventTime
        self.measurementReading = measurementReading
        self.batteryStrength = ((batteryStrength == "") ? Constants.Labels.kWIRED_BATTERY_LABEL.rawValue : batteryStrength + Constants.Symbols.Percentage.rawValue)
        self.signalStrength = ((signalStrength == "") ? Constants.Labels.kWIRED_SIGNAL_LABEL.rawValue : signalStrength + Constants.Symbols.Percentage.rawValue)
        self.batteryStrengthImage = self.setBatteryImage(batteryStrength: batteryStrength)
        self.signalStrengthImage = self.setSignalImage(signalStrength: signalStrength)
        self.displayData = displayData
    }

    
    internal func setBatteryImage(batteryStrength: String) -> UIImage{
        var batteryImage:UIImage?
        var batteryImageName: String?
        //let strengthValue: Int! = Int(batteryStrength)
        if let strengthValue = Int(batteryStrength){
            switch(strengthValue){
            case 0...5:
                batteryImageName = Constants.ImagesName.BATTERY_STRENGTH_05.rawValue
            case 6...20:
                batteryImageName = Constants.ImagesName.BATTERY_STRENGTH_20.rawValue
            case 21...50:
                batteryImageName = Constants.ImagesName.BATTERY_STRENGTH_50.rawValue
            case 51...75:
                batteryImageName = Constants.ImagesName.BATTERY_STRENGTH_75.rawValue
            case 76...100:
                batteryImageName = Constants.ImagesName.BATTERY_STRENGTH_FULL.rawValue
            default:
                batteryImageName = Constants.ImagesName.BATTERY_STRENGTH_FULL.rawValue
            }
            
        }else{
            batteryImageName = Constants.ImagesName.WIRED_BATTERY_STRENGTH.rawValue
        }
        
        batteryImage = UIImage(named: batteryImageName!)
        
        return batteryImage!
    }
    
    internal func setSignalImage(signalStrength: String) -> UIImage{
        var signalImage:UIImage?
        var signalImageName: String?
        if let strengthValue = Int(signalStrength){
            switch(strengthValue){
            case 0:
                signalImageName = Constants.ImagesName.SIGNAL_STRENGTH_NONE.rawValue
            case 1...33:
                signalImageName = Constants.ImagesName.SIGNAL_STRENGTH_33.rawValue
            case 34...66:
                signalImageName = Constants.ImagesName.SIGNAL_STRENGTH_66.rawValue
            case 67...100:
                signalImageName = Constants.ImagesName.SIGNAL_STRENGTH_FULL.rawValue
            default:
                signalImageName = Constants.ImagesName.SIGNAL_STRENGTH_FULL.rawValue
            }
        }else{
            signalImageName = Constants.ImagesName.WIRED_SIGNAL_STRENGTH.rawValue
        }
        
        signalImage = UIImage(named: signalImageName!)
        
        return signalImage!
    }
    
}

class AssetExtraInfo : NSObject {
    var extraInfoObj : Dictionary<String,AnyObject> = [:]
    var assetOwnerContactDetails : AssetContact = AssetContact()
    var assetConfiguration : AssetConfiguration  = AssetConfiguration()
    var assetMachineDetails : AssetMachineDetails = AssetMachineDetails()
    
    override init(){
        super.init()
    }
    
    init(extraInfoObj : Dictionary<String,AnyObject>, assetOwnerContactDetails : AssetContact, assetConfiguration : AssetConfiguration, assetMachineDetails : AssetMachineDetails){
        super.init()
        self.extraInfoObj = extraInfoObj
        self.assetOwnerContactDetails = assetOwnerContactDetails
        self.assetConfiguration = assetConfiguration
        self.assetMachineDetails = assetMachineDetails
    }
}

class AssetConfiguration : NSObject {
    var assetHeartbeatInterval : Int = 0;
    var assetAwareStateHB : Int = 0;
    var checkDigit : String = ""
    
    override init(){
        super.init()
    }
    
    init(assetHeartbeatInterval : Int, assetAwareStateHB : Int, checkDigit : String){
        super.init()
        self.assetHeartbeatInterval = assetHeartbeatInterval
        self.assetAwareStateHB = assetAwareStateHB
        self.checkDigit = checkDigit
    }
}

class AssetMachineDetails : NSObject {
    var make : String = ""
    var model : String = ""
    var serialNo : String = ""
    var location : String = ""
    var assetdescriptionId : Int = 0
    var notes : String = ""
    
    override init(){
        super.init()
    }
    
    init(make : String, model : String, serialNo : String, location : String, assetdescriptionId : Int, notes : String) {
        super.init()
        self.make = make
        self.model = model
        self.serialNo = serialNo
        self.location = location
        self.assetdescriptionId = assetdescriptionId
        self.notes = notes
    }
    
}
