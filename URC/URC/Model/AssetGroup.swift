//
//  AssetGroup.swift
//  URC
//
//  Created by Saran Mahadevan on 28/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AssetGroup: NSObject {
    
    var name : String = "";
    var id : Int = 0;
    var grpDescription : String = "";
    var location : String = "";
    var siteId : Int = 0
    var notificationCount : String = "";
    var imageUrl : NSURL?
    var criticalAlertsCount: Int = 0
    var warningsCount: Int = 0
    var resolvedAlertsCount: Int = 0
    var maintenanceAlertsCount: Int = 0
    
    var geoLocationInfo: GeoLocationInfo = GeoLocationInfo.init()
    
    override init(){
        super.init()
    }
    
    init(name: String, id: Int, grpDescription: String, location:String, siteId: Int, imageUrl: NSURL, criticalAlertsCount: Int, warningsCount: Int, resolvedAlertsCount: Int ) {
        self.name = name
        self.id = id
        self.grpDescription = grpDescription
        self.location = location
        self.siteId = siteId
        self.imageUrl = imageUrl
        self.criticalAlertsCount = criticalAlertsCount
        self.warningsCount = warningsCount
        self.resolvedAlertsCount = resolvedAlertsCount
    }
}
