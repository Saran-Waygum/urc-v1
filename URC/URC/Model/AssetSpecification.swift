//
//  AssetSpecification.swift
//  URC
//
//  Created by Saran Mahadevan on 28/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AssetSpecification: NSObject {
    var id : Int = 0
    var name : String = ""
    var UUID : String = ""
    var displayImageRef : NSURL?
    var measuredQuantities : Array<AssetSpecMeasurementQuantity> = []
    var extraInfo : AssetSpecificationExtraInfo = AssetSpecificationExtraInfo()
    
    override init(){
        super.init()
    }
    
    init(id : Int,name : String, UUID: String, displayImageRef : NSURL, measuredQuantities :Array<AssetSpecMeasurementQuantity>?, extraInfo : AssetSpecificationExtraInfo?) {
        super.init()
        self.id = id
        self.name = name
        self.UUID = UUID
        self.displayImageRef = displayImageRef
        self.extraInfo = extraInfo!
        self.measuredQuantities = measuredQuantities!
    }
}
