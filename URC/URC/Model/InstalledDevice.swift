//
//  InstalledDevice.swift
//  URC
//
//  Created by Saran Mahadevan on 28/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class InstalledDevice: NSObject {
    
    var selectedOrganization : Organization = Organization()
    var selectedManagedSite : Site = Site()
    var selectedManagedDevice : AssetGroup = AssetGroup.init(name: "", id: 0, grpDescription: "", location: "", siteId: 0, imageUrl: NSURL(), criticalAlertsCount: 0, warningsCount: 0, resolvedAlertsCount: 0)
    
    
    var selectedDeviceSpecification : AssetSpecification = AssetSpecification()
    
    var scannedCode : String = ""
    var sensorName : String = ""
    var sensorCode : String = ""
    var sensorHeartbeatInterval : Int = 0
    var sensorAwareStateHeartbeat : Int = 0
    
    override init() {
        
    }
    
}
