//
//  Location.swift
//  URC
//
//  Created by Saran Mahadevan on 28/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class Location: NSObject {
    var id: Int = 0;
    var name: String = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    
    init(id: Int, name: String, latitude:Double, longitude:Double){
        self.id = id
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
    }
    
    override init() {
        super.init()
    }
}
