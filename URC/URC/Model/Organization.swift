//
//  Organization.swift
//  URC
//
//  Created by Saran Mahadevan on 28/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class Organization: NSObject {
    
    var orgID: Int = 0;
    var orgDesc : String = "";
    var orgName : String = "";
    var orgUUID: String = "";
    var parentOrgID : Int = 0;
    var extraInfo : OrganizationExtraInfo = OrganizationExtraInfo()
    var childOrganisations : Array<Organization> = []
    var parentOrganisation: Organization?
    
    override init() {
        super.init()
    }
    
    init(orgID: Int, orgDesc: String, orgName: String, orgUUID:String) {
        super.init()
        self.orgID = orgID
        self.orgDesc = orgDesc
        self.orgName = orgName
        self.orgUUID = orgUUID
    }
    
    init(orgID: Int, orgDesc: String, orgName: String, orgUUID:String, parentOrgID : Int, extraInfo : OrganizationExtraInfo) {
        super.init()
        self.orgID = orgID
        self.orgDesc = orgDesc
        self.orgName = orgName
        self.orgUUID = orgUUID
        self.parentOrgID = parentOrgID
        self.extraInfo = extraInfo
    }
    
}
