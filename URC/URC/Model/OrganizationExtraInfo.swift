//
//  OrganizationExtraInfo.swift
//  URC
//
//  Created by Saran Mahadevan on 28/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class OrganizationExtraInfo: NSObject {
    
    var accNumber : String = ""
    var timeZoneId : Int = 0
    var address : String = ""
    var address2 : String = ""
    var city : String = ""
    var state : String = ""
    var postalCode : String = ""
    var country : String = ""
    
    override init() {
        super.init()
    }
    
    init(accNumber : String, timeZoneId : Int, address : String, address2 : String, city : String, state : String, postalCode : String, country : String) {
        super.init()
        
        self.accNumber = accNumber
        self.timeZoneId = timeZoneId
        self.address = address
        self.address2 = address2
        self.city = city
        self.state = state
        self.postalCode = postalCode
        self.country = country
    }
}
