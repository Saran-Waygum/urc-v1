//
//  Site.swift
//  URC
//
//  Created by Saran Mahadevan on 28/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class Site: NSObject {
    
    var siteId: Int = 0;
    var siteName: String = ""
    var location: Location = Location()
    var siteDesc : String = "";
    var siteUUID: String = "";
    
    
    init(siteId: Int, siteName: String, location:String, locationId:Int, latitude:Double, longitude: Double){
        self.siteId = siteId
        self.siteName = siteName
        self.location = Location(id: locationId, name: location, latitude: latitude, longitude: longitude)
    }
    
    override init() {
        super.init()
    }
    
    init(id: Int, siteDesc: String, siteName: String, siteUUID:String) {
        self.siteId = id
        self.siteDesc = siteDesc
        self.siteName = siteName
        self.siteUUID = siteUUID
    }
    
}
