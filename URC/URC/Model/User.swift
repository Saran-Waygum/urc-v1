//
//  User.swift
//  URC
//
//  Created by Saran Mahadevan on 28/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class User: NSObject {
    var firstName : String = ""
    var lastName : String = ""
    var emailId : String = ""
    var phone : String = ""
    var subjectId : Int = 0
    var subjectUUID : String = ""
    var identityAccess : UserIdentityAccess = UserIdentityAccess()
    var loginId : String = ""
    var password : String = ""
    var extraInfoObj : UserExtraInfo = UserExtraInfo()
    var profileImageUrl : String = ""
    
    override init() {
        super.init()
    }
    
    
    init(firstName: String, lastName: String, emailId:String, phone : String, subjectId: Int, subjectUUID : String, identityAccess : UserIdentityAccess, loginId : String, password : String, extraInfoObj: UserExtraInfo) {
        super.init()
        self.firstName = firstName
        self.lastName = lastName
        self.emailId = emailId
        self.subjectId = subjectId
        self.subjectUUID = subjectUUID
        self.identityAccess = identityAccess
        self.loginId = loginId
        self.password = password
        self.extraInfoObj = extraInfoObj
        self.phone = phone
    }
}
