//
//  UserExtraInfo.swift
//  URC
//
//  Created by Saran Mahadevan on 28/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class UserExtraInfo : NSObject{
    
    var smsCarrierId : Int  = 0
    
    override init() {
        super.init()
    }
    
    init(smsCarrierId: Int) {
        super.init()
        self.smsCarrierId = smsCarrierId
    }
    
}
