//
//  UserIdentityAccess.swift
//  URC
//
//  Created by Saran Mahadevan on 28/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class UserIdentityAccess : NSObject {
    var orgId : Int = 0
    var assetGroupId : Int = 0
    var isSuperAdmin : Bool = false
    var isUserAdmin : Bool = false
    var roles : Array<String> = []
    override init() {
        super.init()
    }
    init(orgId : Int, roles : Array<String>) {
        super.init()
        self.orgId = orgId
        self.roles = roles
        mapUserRoles(roles: roles)
    }
    
    func mapUserRoles(roles: Array<String>){
        for role in roles {
            switch(role){
            case Constants.UserRoles.CreateSubOrg:
                self.isSuperAdmin = true
            case Constants.UserRoles.ManageUsersInSubOrg,Constants.UserRoles.ManageUsersInOrg:
                self.isUserAdmin = true
            default:
                break
            }
        }
    }
}
