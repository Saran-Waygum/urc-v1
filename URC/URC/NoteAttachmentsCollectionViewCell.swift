//
//  NoteAttachmentsCollectionViewCell.swift
//  URC
//
//  Created by Saran Mahadevan on 19/01/17.
//  Copyright © 2017 Waygum. All rights reserved.
//

import UIKit

class NoteAttachmentsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var attachmentPreviewImageView: UIImageView!
    @IBOutlet weak var removeUploadedAttachment: UIButton!
    
}
