//
//  NotesListViewController.swift
//  URC
//
//  Created by Saran Mahadevan on 16/01/17.
//  Copyright © 2017 Waygum. All rights reserved.
//

import UIKit
import MobileCoreServices

class NotesListViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var notesCollectionView: UICollectionView!
    @IBOutlet weak var createNotesViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var attachmentsCollectionView: UICollectionView!
    @IBOutlet weak var notesTextView: UITextView!
    @IBOutlet weak var characterCountLabel: UILabel!
    
    
    
    var imagesPicked : Array<UIImage> = []
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(NotesListViewController.addNewNotes)), animated: true)
        
        self.title = "Alert History"
        
        notesTextView.layer.borderWidth = 0.5
        notesTextView.layer.borderColor = UIColor.black.cgColor
        
        imagePicker.delegate = self
        notesTextView.delegate = self
        
//        self.title = "Create Note"
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        notesTextView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notesTextView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        let textView = object as! UITextView
        var topCorrect = (textView.bounds.size.height - textView.contentSize.height * textView.zoomScale) / 2
        topCorrect = topCorrect < 0.0 ? 0.0 : topCorrect;
        textView.contentInset.top = topCorrect
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func addNewNotes()
    {
        
        if self.createNotesViewHeightConstraint.constant == 0.0
        {
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                
                self.createNotesViewHeightConstraint.constant = 300.0
                
            }, completion: { (finished: Bool) -> Void in
                self.view.layoutSubviews()
            })
            
        }
        else
        {
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
                
                self.createNotesViewHeightConstraint.constant = 0.0
                
            }, completion: { (finished: Bool) -> Void in
                self.view.layoutSubviews()
            })
            
        }
    }
    
    
    // MARK: - Attachment Actions
    
    @IBAction func addPhotoButtonPressed(_ sender: UIButton) {
    }
    
    
    @IBAction func addVideoButtonPressed(_ sender: UIButton) {
    }
    
    
    @IBAction func addAudioFileButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func addDocumentButtonPressed(_ sender: UIButton) {
    }
    
    
    
    
    // MARK: - Navigation
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return imagesPicked.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let attachmentCell : NoteAttachmentsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoteAttachmentsCollectionViewCellID", for: indexPath as IndexPath) as! NoteAttachmentsCollectionViewCell
        
        attachmentCell.attachmentPreviewImageView.image = imagesPicked[indexPath.row]
        
        return attachmentCell
        
    }
    
    
    func addImageFromGallery()
    {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func addImageFromCamera()
    {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    func addVideoPressed()
    {
        startCameraFromViewController(viewController: self, withDelegate: self)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imagesPicked.append(pickedImage)
            
            self.attachmentsCollectionView.reloadData()
            
        }
        else
        {
            let mediaType = info[UIImagePickerControllerMediaType] as! NSString
            
//            if mediaType == kUTTypeMovie {
//                guard let path = (info[UIImagePickerControllerMediaURL] as! NSURL).path else { return }
//                if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(path) {
//                    //                UISaveVideoAtPathToSavedPhotosAlbum(path, self, #selector(CreateNotesViewController.video(_:didFinishSavingWithError:contextInfo:)), nil)
//                }
//            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        characterCountLabel.text = String.init(format: "%d/500", (500 - (textView.text?.characters.count)!))
        return true
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
    }
    
    
    func video(videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        var title = "Success"
        var message = "Video was saved"
        if let _ = error {
            title = "Error"
            message = "Video failed to save"
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    
    func startCameraFromViewController(viewController: UIViewController, withDelegate delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate) -> Bool {
        if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
            return false
        }
        
//        let cameraController = UIImagePickerController()
//        cameraController.sourceType = .camera
//        cameraController.mediaTypes = [kUTTypeMovie as NSString as String]
//        cameraController.allowsEditing = false
//        cameraController.delegate = delegate
//        
//        present(cameraController, animated: true, completion: nil)
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
