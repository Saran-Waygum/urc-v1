//
//  Notification.swift
//  URC
//
//  Created by Saran Mahadevan on 03/01/17.
//  Copyright © 2017 Waygum. All rights reserved.
//

import UIKit

class Notification: NSObject {
    
    var id : Int = 0
    var entityId : Int = 0
    var generatedDate : NSDate = NSDate()
    var isRead : Bool = false
    var generatedTime : Date = Date.init()
    var generatedBy: String = ""

}
