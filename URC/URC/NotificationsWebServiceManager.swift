//
//  NotificationsWebServiceManager.swift
//  URC
//
//  Created by Saran Mahadevan on 03/01/17.
//  Copyright © 2017 Waygum. All rights reserved.
//

import UIKit

class NotificationsWebServiceManager: URCWebServiceManager {
    
    var notificationsUrl : String = Constants.getNotificationURL()
    var notificationsArray : Array<Notification> = []
    var acknowledgeNotificationsURL : String = Constants.getAcknowledgeNotificationURL()
    
    
    func loadNotifications(success : @escaping (_ response: Array<Notification>?)->Void, failure :@escaping (_ error : NSError?, _ statusCode: Int) -> Void){
        
        
        
        loadDataFromService(url: notificationsUrl, parameters: nil , headers: nil , success: {
            (response,resHeaders) in
            
            for object in response
            {
                
                if let notificationInfo = object as? Dictionary<String, AnyObject>
                {
                    
                    let newAlertNotification = AlertNotification.init()
                    
                    newAlertNotification.alertDescription = object.value(forKey: "alertDescription") as! String
                    newAlertNotification.id = object.value(forKey: "id") as! Int
                    newAlertNotification.alertState = object.value(forKey: "alertState") as! String
                    
                    newAlertNotification.assetName = object.value(forKey: "assetName") as! String
                    newAlertNotification.alertMessage = object.value(forKey: "message") as! String
                    
                    newAlertNotification.alertId = object.value(forKey: "alertId") as! Int
                    newAlertNotification.alertState = object.value(forKey: "alertState") as! String
                    newAlertNotification.alertLevel = object.value(forKey: "alertLevel") as! String
                    newAlertNotification.isRead = object.value(forKey: "read") as! Bool
                    newAlertNotification.generatedBy = object.value(forKey: "generatedBy") as! String
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX"
                    
                    
                    let dateGeneratedString = dateFormatter.date(from: notificationInfo["generatedAt"] as! String)!
                    
                    dateFormatter.dateFormat = "hh:mm a"
                    dateFormatter.amSymbol = "AM"
                    dateFormatter.pmSymbol = "PM"
                    
                    newAlertNotification.generatedTime = (dateGeneratedString as NSDate) as Date
                    
                    
                    
                    self.notificationsArray.append(newAlertNotification)
                }
            }
            
            success (self.notificationsArray)
            
        }, failure: { (error,code) in
            failure(error, code)
        })
        
    }
    
    
    func silenceNotification(notificationId: Int, success : @escaping (_ response: Array<Notification>?)->Void, failure :@escaping (_ error : NSError?, _ statusCode: Int) -> Void){
        
        
        
        loadDataFromService(url: notificationsUrl, parameters: nil , headers: nil , success: {
            (response,resHeaders) in
            
            for object in response
            {
                
                if let notificationInfo = object as? Dictionary<String, AnyObject>
                {
                    
                    if (notificationInfo["type"] as? String) == "Alert.Generated"
                    {
                        let newAlertNotification = AlertNotification.init()
                        
                        newAlertNotification.alertDescription = object.value(forKey: "alertDescription") as! String
                        newAlertNotification.id = object.value(forKey: "id") as! Int
                        newAlertNotification.alertState = object.value(forKey: "alertState") as! String
                        
                        newAlertNotification.assetName = object.value(forKey: "assetName") as! String
                        newAlertNotification.alertMessage = object.value(forKey: "message") as! String
                        
                        newAlertNotification.alertId = object.value(forKey: "alertId") as! Int
                        newAlertNotification.alertState = object.value(forKey: "alertState") as! String
                        newAlertNotification.alertLevel = object.value(forKey: "alertLevel") as! String
                        newAlertNotification.isRead = object.value(forKey: "read") as! Bool
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX"
                        
                        
                        let dateGeneratedString = dateFormatter.date(from: notificationInfo["generatedAt"] as! String)!
                        
                        dateFormatter.dateFormat = "hh:mm a"
                        dateFormatter.amSymbol = "AM"
                        dateFormatter.pmSymbol = "PM"
                        
                        newAlertNotification.generatedTime = (dateGeneratedString as NSDate) as Date
                        
                        //                        newAlert.dateGenerated = dateFormatter.string(from: dateGeneratedString)
                        
                        
                        self.notificationsArray.append(newAlertNotification)
                    }
                }
                
                
                
            }
            
            success (self.notificationsArray)
            
        }, failure: { (error,code) in
            failure(error, code)
        })
        
    }
    
    

    func readNotification(parameters:[String:String]!, success : @escaping (_ response: Bool?)->Void, failure :@escaping (_ error : NSError?, _ statusCode: Int) -> Void){
        
        
        if parameters["alertId"] != nil
        {
            acknowledgeNotificationsURL = acknowledgeNotificationsURL + "/" + parameters["alertId"]!
        }
        
        print(acknowledgeNotificationsURL)
        postDataToService(url: acknowledgeNotificationsURL, parameters: nil, headers: nil, success:
            { (response) -> Void in
                for obj in response {
                    
                    print(obj)
                    
                }
                success (true)
                
                
        }, failure: {
            (error,code,failureReason) in
            print("failed")
        })
        
    }
    
}
