//
//  OrganizationsWebServiceManager.swift
//  URC
//
//  Created by Saran Mahadevan on 29/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class OrganizationsWebServiceManager: URCWebServiceManager {

    
    var createOrgUrl : String = Constants.getOrganizationURL()
    var orgDetailsByIdUrl : String = Constants.getOrganizationByIdURL()
    var orgDetailsByUUIDUrl : String = Constants.getOrganizationByUUIdURL()
    var organizationsArray : Array<Organization> = []
    
    
    func loadOrganizations(success : @escaping (_ response: Array<Organization>?)->Void, failure :@escaping (_ error : NSError?, _ statusCode: Int) -> Void){
        
//        let subjectId = URCManager.sharedInstance.user.subjectId
//        let orgUrl = ("\(Constants.getSubIdentityBasedOrgsFirstPart())\(subjectId)\(Constants.getSubIdentityBasedOrgsLastPart())")
        let orgUrl = "http://indiadevmisc.cloudapp.net:8080/api/identitySubjects/52/organizations"
        
        loadDataFromService(url: orgUrl, parameters: nil , headers: nil , success: {
            (response,resHeaders) in
            
            self.organizationsArray = self.createChildOrganizations(data: response as! Array<Dictionary<String, Any>>, forParent: nil)
            
            success (self.organizationsArray)
            
        }, failure: { (error,code) in
            failure(error, code)
        })
        
    }
    
    
    func createChildOrganizations(data: Array<Dictionary<String, Any>>, forParent: Organization? ) -> Array<Organization>
    {
        
        var newOrgs : Array<Organization> = []
        for orgData in data
        {
            
            if let obj = orgData["orgInfo"] as? Dictionary<String, Any>
            {
                let name =  (obj["orgName"] as? String) == nil ? "" : obj["orgName"] as! String
                let id =  (obj["id"] as? Int)  == nil ? 0 : obj["id"] as! Int
                let orgDesc =  (obj["orgDesc"] as? String) == nil ? "" : obj["orgDesc"] as! String
                let orgUUID =  (obj["orgUUId"] as? String) == nil ? "" : obj["orgUUId"] as! String
                
                
                let orgCreated = Organization.init()
                orgCreated.orgName = name
                orgCreated.orgID = id
                orgCreated.orgUUID = orgUUID
                orgCreated.orgDesc = orgDesc
                orgCreated.parentOrganisation = forParent
                
                if let childOrgs = orgData["childOrgs"] as? Array<Dictionary<String, Any>>
                {
                    orgCreated.childOrganisations = self.createChildOrganizations(data: childOrgs, forParent: orgCreated)
                }
                
                newOrgs.append(orgCreated)
                
            }
        }
        
        return newOrgs
        
    }
    
    
    
    
    
    
}
