//
//  SettingsViewController.swift
//  URC
//
//  Created by Saran Mahadevan on 25/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var settingsTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var settingsOptionsArray: Array<String> = ["Profile","Help","Sign Out"]
    var settingsOptionsImagesArray: Array<String> = ["ic_settings_profile","ic_settings_help","ic_settings_signout"]
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.title = "Settings"
        settingsTableView.tableHeaderView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
        settingsTableView.tableFooterView = UIView()
        activityIndicator.isHidden = true
        
        self.setNavigationBarStyle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNavigationBarStyle(){
        
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        //        self.navigationController!.navigationBar.barTintColor = Styles.Style.appNavigationBarColor
        //        self.navigationController!.navigationBar.tintColor = Styles.Style.appNavigationContentColor
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont(name: Styles.Style.semiBoldStyleCustomFont, size: 16.0)!]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        //        self.navigationController?.navigationBar.isTranslucent = true
        //        self.navigationController?.view.backgroundColor = Styles.Style.appNavigationBarColor
        //
        //        self.navigationController!.navigationBar.backgroundColor = Styles.Style.appNavigationBarColor
        //
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = Styles.Style.appNavigationBarColor
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Table View DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 2
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingsTableViewCellID", for: indexPath)
        
        if indexPath.section == 0
        {
            cell.textLabel?.text = settingsOptionsArray[indexPath.row]
            cell.imageView?.image = UIImage.init(named: settingsOptionsImagesArray[indexPath.row])
        }
        else
        {
            cell.textLabel?.text = "Sign Out"
            cell.imageView?.image = UIImage.init(named: "ic_settings_signout")
            cell.accessoryType = UITableViewCellAccessoryType.none
        }
        
        
        return cell
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let footerView = UIView(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        footerView.backgroundColor = UIColor.white
        
        return footerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 1
        {
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            
            let logoutWebServiceManager = LogoutWebServiceManager()
            
            logoutWebServiceManager.logoutUser(success: {
                (response) in
                
                let vc : LoginViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                
                self.present(vc, animated: true, completion: nil)
                
            }, failure: {
                (error,code) in
                
                
            })

        }
    }
    
}
