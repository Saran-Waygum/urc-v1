//
//  SiteMKAnnotation.swift
//  URC
//
//  Created by Saran Mahadevan on 09/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit
import MapKit

class SiteMKAnnotation: NSObject, MKAnnotation {
    let title : String?
    let id : Int?
    let coordinate : CLLocationCoordinate2D
    var imageName : String = Constants.ImagesName.MAP_PIN_GREEN.rawValue
    init(title: String, id: Int, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.id = id
        self.coordinate = coordinate
        super.init()
        self.imageName = Constants.ImagesName.MAP_PIN_GREEN.rawValue
    }
    
}
