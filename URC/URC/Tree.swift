//
//  Tree.swift
//  URC
//
//  Created by Saran Mahadevan on 29/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import Foundation
import UIKit

class Tree : NSObject, UITableViewDataSource {
    var model : NSMutableDictionary?
    var directModel : NSMutableDictionary?
    var rootItemsCount : Int = 0
    var tableView:UITableView?
    @IBOutlet weak var dataSource: AnyObject?
    var expandingAnimation:UITableViewRowAnimation!
    var closingAnimation:UITableViewRowAnimation!
    
    override init(){
        super.init()
        self.model = NSMutableDictionary()
        self.directModel = NSMutableDictionary()
        self.closingAnimation = UITableViewRowAnimation.automatic
        self.expandingAnimation = UITableViewRowAnimation.automatic
        
    }
    
    func forwardingTargetForSelector (selector: Selector) -> AnyObject? {
        if self.dataSource!.responds(selector) {
            return self.dataSource!
        }
        return nil
    }
    override func respondsToSelector(selector: Selector) -> Bool {
        let responds: Bool = super.respondsToSelector(selector)
        if responds {
            return responds
        }
        return self.dataSource!.respondsToSelector(selector)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let itemPath: NSIndexPath? = self.tableView!.treeIndexPathFromTablePath(indexPath)
        if(itemPath == nil){
            return UITableViewCell()
        }
        return self.dataSource!.tableView(self.tableView!, cellForRowAtIndexPath: itemPath!)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        let rows: Int = self.dataSource!.tableView(tableView, numberOfRowsInSection: section)
        var totalRowsCount: Int = rows
        let ip: NSIndexPath = NSIndexPath(index: section)
        self.directModel![ip] = rows
        for i in 0 ..< rows {
            let ip: NSIndexPath = NSIndexPath(forRow: i, inSection: section)
            totalRowsCount += Int(self.tableView!.numberOfSubitems(ip))
        }
        self.model![ip] = totalRowsCount
        return totalRowsCount
    }
    
    
    
}


protocol TreeTableDataSource : UITableViewDataSource{
    func tableView(tableView: UITableView, isCellExpanded indexPath: NSIndexPath) -> Bool
    func tableView(tableView: UITableView, numberOfSubCellsForCellAtIndexPath indexPath: NSIndexPath) -> UInt
    
}

extension UITableView {
    
    private struct AssociatedKeys {
        static var dismissRows:Array<NSIndexPath> = []
        static var insertRows:Array<NSIndexPath> = []
    }
    
    internal func expand (indexPath : NSIndexPath) {
        AssociatedKeys.insertRows.removeAll()
        if(self.isExpanded(indexPath)){
            return
        }
        self.numberOfSubitems(indexPath) //commented
        
        //                var insertRows:Array<NSIndexPath> = []
        self.expand(indexPath, rows: AssociatedKeys.insertRows)//commented
        
        self.insertRowsAtIndexPaths(AssociatedKeys.insertRows, withRowAnimation: UITableViewRowAnimation.None)
        
        
    }
    
    internal func expand(indexPath: NSIndexPath, rows: Array<NSIndexPath>){
        let section: Int = indexPath.section
        var count: Int = 0
        if(self.treeProxy.directModel![indexPath] != nil){
            count = Int((self.treeProxy.directModel![indexPath]?.unsignedIntegerValue)!)
        }
        
        if(count > 0){
            for i in 0 ..< count {
                let ip:NSIndexPath = indexPath.indexPathByAddingIndex(i)
                self.expand(ip, rows: AssociatedKeys.insertRows)
            }
            var insertRows:Array<NSIndexPath>=[]
            for j in 0 ..< count {
                let ip:NSIndexPath = indexPath.indexPathByAddingIndex(j)
                let row: Int = Int(self.rowOffsetForIndexPath(ip))
                insertRows.append(NSIndexPath(forRow: row, inSection: section))
            }
            AssociatedKeys.insertRows.appendContentsOf(insertRows)
            
        }
    }
    
    
    internal func isExpanded (indexPath: NSIndexPath) -> Bool{
        
        return ((self.treeProxy.directModel![indexPath]?.integerValue != 0) && (nil != self.treeProxy.directModel![indexPath]));
    }
    internal func collapse (indexPath: NSIndexPath){
        AssociatedKeys.dismissRows.removeAll()
        
        self.collapse(indexPath, rows: AssociatedKeys.dismissRows)
        self.deleteRowsAtIndexPaths(AssociatedKeys.dismissRows, withRowAnimation: UITableViewRowAnimation.None)
        
    }
    internal func collapse (indexPath: NSIndexPath,  rows: Array<NSIndexPath>){
        let section: Int = indexPath.section
        var count: Int = 0
        if(self.treeProxy.directModel![indexPath] != nil){
            count = Int((self.treeProxy.directModel![indexPath]?.unsignedIntegerValue)!)
        }
        
        if(count > 0){
            var row: Int = 0
            var dismissRows :Array<NSIndexPath> = []
            for i in 0 ..< count {
                let ip:NSIndexPath = indexPath.indexPathByAddingIndex(i)
                row = Int(self.rowOffsetForIndexPath(ip))
                dismissRows.append(NSIndexPath(forRow: row, inSection: section))
            }
            AssociatedKeys.dismissRows.appendContentsOf(dismissRows)
            //for var j = (count-1); j >= 0; j -= 1 {
            var j = (count-1)
            while j >= 0 {
                let ip:NSIndexPath = indexPath.indexPathByAddingIndex(j)
                self.collapse(ip, rows: AssociatedKeys.dismissRows)
                j -= 1
            }
            
        }
        self.treeProxy.model!.removeObjectForKey(indexPath)
        self.treeProxy.directModel!.removeObjectForKey(indexPath)
        
    }
    
    
    
    internal func siblings (indexPath: NSIndexPath) -> Array<AnyObject>{
        let parent: NSIndexPath? = self.parent(indexPath)
        var arr: Array<NSIndexPath> = Array(count: 20, repeatedValue: NSIndexPath())
        
        for i in 0 ..< self.numberOfSections {
            var ip:NSIndexPath = NSIndexPath()
            if(parent != nil){
                ip = parent!.indexPathByAddingIndex(i)
            }else{
                ip = NSIndexPath(index: i)
            }
            if(NSComparisonResult.OrderedSame == ip.compare(indexPath)){
                continue
            }
            arr.append(ip)
        }
        return arr
    }
    
    internal func parent (indexPath: NSIndexPath) -> NSIndexPath?{
        if (indexPath.length > 1) {
            return indexPath.indexPathByRemovingLastIndex()
        } else {
            return nil;
        }
    }
    
    internal func itemForTreeIndexPath (indexPath: NSIndexPath) -> UITableViewCell{
        let ip: NSIndexPath = self.tableIndexPathFromTreePath(indexPath)
        return self.cellForRowAtIndexPath(ip)!
    }
    internal func treeIndexPathForItem (item: UITableViewCell) -> NSIndexPath?{
        let tableIndexPath: NSIndexPath = self.indexPathForCell(item)!
        return self.treeIndexPathFromTablePath(tableIndexPath)
        
    }
    
    /**
     Coverts multidimensional indexPath into 2d UITableView-like indexPath.
     
     This method is required to prepare indexPath parameter when calling original UITableView's methods.
     */
    
    internal func tableIndexPathFromTreePath (indexPath: NSIndexPath) -> NSIndexPath{
        let row: Int = Int( self.rowOffsetForIndexPath(indexPath))
        return NSIndexPath(forRow: row, inSection: 0)
    }
    
    /**
     Converts TreeTable indexPath to TableView row index.
     */
    
    internal func rowOffsetForIndexPath (indexPath: NSIndexPath) -> UInt{
        let section : Int = indexPath.indexAtPosition(0)
        let ip : NSIndexPath = NSIndexPath(index: section)
        return self.rowOffsetForIndexPath(indexPath, root: ip)
    }
    
    //    lazy internal var treeProy :Tree = self.dataSource as! Tree
    var treeProxy : Tree{
        return self.dataSource as! Tree
    }
    
    
    internal func rowOffsetForIndexPath (indexPath: NSIndexPath, root: NSIndexPath) -> UInt{
        if (NSComparisonResult.OrderedSame == indexPath.compare(root)) {
            return 0;
        }
        var totalCount: UInt = 0
        
        if(root.length > 1){
            totalCount += 1
        }
        var subItemsCount : Int = 0
        //let num :NSNumber = (self.treeProxy.directModel![root] as? NSNumber)!
        if(self.treeProxy.directModel![root] != nil){
            let num :NSNumber = (self.treeProxy.directModel![root] as? NSNumber)!
            subItemsCount = Int(num.intValue)
        }else{
            let delegate = self.treeProxy.dataSource as! TreeTableDataSource
            if(delegate.tableView(self, isCellExpanded: root) == true){
                subItemsCount = Int(delegate.tableView(self, numberOfSubCellsForCellAtIndexPath: root))
            }
        }
        
        for i in 0 ..< subItemsCount {
            let ip : NSIndexPath = root.indexPathByAddingIndex(i)
            if(NSComparisonResult.OrderedAscending != ip.compare(indexPath)){
                break
            }
            if(ip.length < indexPath.length){
                let count : UInt = self.rowOffsetForIndexPath(indexPath, root: ip)
                totalCount += count
            }else{
                let count : UInt = self.rowOffsetForIndexPath(indexPath, root: ip)
                totalCount += count
            }
        }
        return totalCount
    }
    internal func treeIndexOfRow (row: UInt, root: NSIndexPath, offset: UInt) -> NSIndexPath{
        var count : UInt = 0
        
        var ip: NSIndexPath?
        let num: UInt = (self.treeProxy.model![root]?.unsignedIntegerValue)!
        if(num == 0 ){
            return root
        }
        for i in 0 ..< Int(num) {
            if(row == count){
                return root.indexPathByAddingIndex(i)
            }
            ip = root.indexPathByAddingIndex(i)
            let numValue : UInt = (self.treeProxy.model![ip!]?.unsignedIntegerValue)!
            count += 1;
            if(row < (numValue + count)){
                return self.treeIndexOfRow((row - count), root: ip!, offset: count)
            }
            count += numValue
        }
        return ip!
    }
    
    
    /**
     Converts UITableTable 2d indexPath into multidimentional indexPath.
     
     @param indexPath 2d UITableView-like index path
     
     @return multidimantional TreeView-like indexPath.
     */
    internal func treeIndexPathFromTablePath (indexPath: NSIndexPath) -> NSIndexPath?{
        
        var count : Int = 0
        let section : Int = indexPath.section
        let row : Int = indexPath.row
        
        let ip:NSIndexPath = NSIndexPath(index: section)
        let rowsCount: UInt = (self.treeProxy.directModel![ip]?.unsignedIntegerValue)!
        
        for r in 0 ..< Int(rowsCount) {
            if(row == count){
                return NSIndexPath(forRow: r, inSection: section)
            }
            let ip: NSIndexPath = NSIndexPath(forRow: r, inSection: section)
            let numValue : UInt = (self.treeProxy.model![ip]?.unsignedIntegerValue)!
            count += 1
            
            if(row < (Int(numValue) + count)){
                let ip: NSIndexPath = NSIndexPath(forRow: r, inSection: section)
                return self.treeIndexOfRow(UInt(row - count), root: ip, offset: UInt(count))
            }
            count += Int(numValue)
        }
        
        //        NSLog(@"ERR. Error while converting tableIndexPath into treeIndexPath. %s", __PRETTY_FUNCTION__);
        return nil
        
    }
    
    private func numberOfSubitems (indexPath: NSIndexPath) -> UInt{
        var count : UInt = 0
        let delegate = self.treeProxy.dataSource as? TreeTableDataSource
        var isExpanded : Bool = false
        
        if(indexPath.length == 1){
            isExpanded = true
        }else{
            isExpanded = (delegate?.tableView(self, isCellExpanded: indexPath))!
        }
        
        if(isExpanded){
            let subItemsCount : UInt = (delegate?.tableView(self, numberOfSubCellsForCellAtIndexPath: indexPath))!
            for i in 0 ..< Int(subItemsCount) {
                let subItemPath : NSIndexPath = indexPath.indexPathByAddingIndex(i)
                count += self.numberOfSubitems(subItemPath)
            }
            
            count += subItemsCount
            self.treeProxy.directModel!.setObject(subItemsCount, forKey: indexPath)
        }
        
        self.treeProxy.model!.setObject(count, forKey: indexPath)
        
        return count;
    }
    
}
