//
//  URCManager.swift
//  URC
//
//  Created by Saran Mahadevan on 28/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit
import Foundation
import MapKit

class URCManager {
    
    var accessToken: String = ""
    var refreshToken: String = ""
    var geolocations: Array<Location> = []
    var sites: Array<Site> = []
    var locationManager = CLLocationManager()
    var currentLocation = CLLocation()
    var user : User = User()
    var installedDevice: InstalledDevice = InstalledDevice()
    var sessionExpired : Bool = false
    var favoriteAssetGroups : Array<AssetGroup> = []
    var favoriteAssets : Array<Asset> = []
    
    class var sharedInstance: URCManager {
        struct Static {
            static let instance = URCManager()
        }
        return Static.instance
    }
    
    
}
