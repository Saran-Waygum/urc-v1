//
//  Styles.swift
//  URC
//
//  Created by Saran Mahadevan on 28/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import Foundation
import UIKit
class Styles: NSObject{
    struct Style {
        static var customFontForApp = "OpenSans"
        static var boldStyleCustomFont = "\(customFontForApp)-Bold"
        static var semiBoldStyleCustomFont = "\(customFontForApp)-Semibold"
        static var extraBoldStyleCustomFont = "\(customFontForApp)-ExtraBold"
        static var lightStyleCustomFont = "\(customFontForApp)-Light"
        static var regularStyleCustomFont = "\(customFontForApp)-Regular"
        
        static var blueTitleLabelColor =  UIColor(red: 28.0/255.0, green: 132.0/255.0, blue: 198.0/255.0, alpha: 1)
        
        static var sideMenuBackgroundColor = UIColor(red: 47.0/255.0, green: 64.0/255.0, blue: 80.0/255.0, alpha: 1.0)
        static var sideMenuUnselectedItemBackgroundColor = UIColor(white: 1.0, alpha: 0.0)
        static var sideMenuSelectedItemBackgroundColor = UIColor(red: 41.0/255.0, green: 56.0/255.0, blue: 70.0/255.0, alpha: 1)
        static var sideMenuSelectedItemBorderColor = UIColor(red: 28.0/255.0, green: 132.0/255.0, blue: 198.0/255.0, alpha: 1)
//        static var appNavigationBarColor = UIColor(red: 47.0/255.0, green: 64.0/255.0, blue: 80.0/255.0, alpha: 1.0)
        static var appNavigationBarColor = UIColor(red: 42.0/255.0, green: 56.0/255.0, blue: 69.0/255.0, alpha: 1.0)
        static var appNavigationTitleColor = UIColor.white
        static var appNavigationContentColor = UIColor.white
        static var alternateTableItemsRowColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1)
        static var tableItemsRowColor = UIColor.white
        static var lightAlternateTableItemsRowColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1)
        static var appPopUpViewBackgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        static var appPopUpViewShadowColor = UIColor.black
        static var appBlockingViewBackgroundColor = UIColor.black
        
        static var appButtonNormalStateTitleColor = UIColor(red: 95.0/255.0, green: 95.0/255.0, blue: 95.0/255.0, alpha: 1.0)
        static var appButtonHighlightedStateTitleColor = UIColor(red: 28.0/255.0, green: 132.0/255.0, blue: 198.0/255.0, alpha: 1)
        static var appDisabledButtonBackgroundColor = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1)
        static var appEnabledButtonBackgroundColor = UIColor(red: 28.0/255.0, green: 132.0/255.0, blue: 198.0/255.0, alpha: 1)
        static var appEnabledButtonGreenBackgroundColor = UIColor(red: 52.0/255.0, green: 163.0/255.0, blue: 134.0/255.0, alpha: 1)
        struct appActivityIndicatorStyle {
            static var backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1)
            static var titleColor = UIColor.black
            static var indicatorColor = UIColor.black
        }
        static var appErrorLabelTextColor = UIColor(red: 255/255.0, green: 128.0/255.0, blue: 36.0/255.0, alpha: 1)
        
        
        static var criticalAlertColor = UIColor(red: 229/255, green: 91/255, blue: 91/255, alpha: 1)
        static var warningAlertColor = UIColor(red: 1, green: 184/255, blue: 73/255, alpha: 1)
        static var resolvedAlertColor = UIColor(red: 25/255, green: 170/255, blue: 141/255, alpha: 1)
        static var tabSelectionColor = UIColor(red: 59/255, green: 148/255, blue: 205/255, alpha: 1)
        static var grayBackgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        static var backgroundGrayColor = UIColor(red: 0.949949, green: 0.949949, blue: 0.949949, alpha: 1)
        
        
        
    }
}
