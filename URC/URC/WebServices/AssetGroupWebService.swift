//
//  AssetGroupWebService.swift
//  URC
//
//  Created by Saran Mahadevan on 29/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class AssetGroupWebService: URCWebServiceManager {
    
    
    var assetGroupsArray: Array<AssetGroup> = []
    let assetGroupServiceURL =  Constants.getAssetGroupUrl()
    
    func loadAssetGroups(success : @escaping (_ response: Array<AssetGroup>?)->Void, failure :@escaping (_ error : NSError?, _ statusCode: Int) -> Void){
        
        
        
        loadDataFromService(url: assetGroupServiceURL, parameters: nil , headers: nil , success: {
            (response,resHeaders) in
            var locationName: String = ""
            for obj in response{
                let loc = obj["geoLocationInfo"]
                let geoLocation = GeoLocationInfo.init()
                if let location = loc as? Dictionary<String, AnyObject> {
                    locationName = location["geoLocationName"] != nil ? location["geoLocationName"] as! String : "Unknown"
                    
                    
                    geoLocation.geoLocationCenterLongitude = location["geoLocationCenterLongitude"] as! Double
                    geoLocation.geoLocationCenterLatitude = location["geoLocationCenterLatitude"] as! Double
                }
                
                
                var imageUrl: String = ""
                if (obj["assetGrpDisplayImageRef"] as? String) != nil
                {
                    imageUrl = obj["assetGrpDisplayImageRef"] as! String
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                }
                
                let assetGroupCreated = AssetGroup(
                    name: (obj["assetGrpName"] as? String) == nil ? "Name" : (obj["assetGrpName"] as! String),
                    id: obj["id"] == nil ? 0 : ((obj["id"] as AnyObject).integerValue)!,
                    grpDescription: (obj["assetGrpDesc"] as? String) == nil ? "" : (obj["assetGrpDesc"] as! String),
                    location: locationName,
                    siteId: (obj["managedSiteId"] as? Int) == nil ? 0 : (obj["managedSiteId"] as! Int),
                    imageUrl:(obj["assetGrpDisplayImageRef"] as? String) == nil ? NSURL() : NSURL(string: imageUrl)!,
                    criticalAlertsCount: (obj["criticalAlertCount"] as? Int) == 0 ? 0 : (obj["criticalAlertCount"] as! Int),
                    warningsCount: (obj["warningAlertCount"] as? Int) == 0 ? 0 : (obj["warningAlertCount"] as! Int),
                    resolvedAlertsCount: (obj["resolvedAlertCount"] as? Int) == 0 ? 0 : (obj["resolvedAlertCount"] as! Int)
                )
                assetGroupCreated.geoLocationInfo = geoLocation
                
                self.assetGroupsArray.append(assetGroupCreated)
                
            }
            success (self.assetGroupsArray)
            
        }, failure: { (error,code) in
            failure(error, code)
        })
        
    }
    
    
    func loadAssetGroupsForSite(siteId: String, success : @escaping (_ response: Array<AssetGroup>?)->Void, failure :@escaping (_ error : NSError?, _ statusCode: Int) -> Void){
        
        let managedAssetsUrl = Constants.getSiteBasedAssetGroups() + siteId
        
        loadDataFromService(url: managedAssetsUrl, parameters: nil , headers: nil , success: {
            (response,resHeaders) in
            var locationName: String = ""
            for obj in response{
                let loc = obj["geoLocationInfo"]
                let geoLocation = GeoLocationInfo.init()
                if let location = loc as? Dictionary<String, AnyObject> {
                    locationName = location["geoLocationName"] != nil ? location["geoLocationName"] as! String : "Unknown"
                    
                    
                    geoLocation.geoLocationCenterLongitude = location["geoLocationCenterLongitude"] as! Double
                    geoLocation.geoLocationCenterLatitude = location["geoLocationCenterLatitude"] as! Double
                }
                
                
                var imageUrl: String = ""
                if (obj["assetGrpDisplayImageRef"] as? String) != nil
                {
                    imageUrl = obj["assetGrpDisplayImageRef"] as! String
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                }
                
                let assetGroupCreated = AssetGroup(
                    name: (obj["assetGrpName"] as? String) == nil ? "Name" : (obj["assetGrpName"] as! String),
                    id: obj["id"] == nil ? 0 : ((obj["id"] as AnyObject).integerValue)!,
                    grpDescription: (obj["assetGrpDesc"] as? String) == nil ? "" : (obj["assetGrpDesc"] as! String),
                    location: locationName,
                    siteId: (obj["managedSiteId"] as? Int) == nil ? 0 : (obj["managedSiteId"] as! Int),
                    imageUrl:(obj["assetGrpDisplayImageRef"] as? String) == nil ? NSURL() : NSURL(string: imageUrl)!,
                    criticalAlertsCount: (obj["criticalAlertCount"] as? Int) == 0 ? 0 : (obj["criticalAlertCount"] as! Int),
                    warningsCount: (obj["warningAlertCount"] as? Int) == 0 ? 0 : (obj["warningAlertCount"] as! Int),
                    resolvedAlertsCount: (obj["resolvedAlertCount"] as? Int) == 0 ? 0 : (obj["resolvedAlertCount"] as! Int)
                )
                assetGroupCreated.geoLocationInfo = geoLocation
                
                self.assetGroupsArray.append(assetGroupCreated)
                
            }
            success (self.assetGroupsArray)
            
        }, failure: { (error,code) in
            failure(error, code)
        })
        
    }
    
    //
    
}
