//
//  LoginWebServiceManager.swift
//  URC
//
//  Created by Saran Mahadevan on 02/12/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import UIKit

class LoginWebServiceManager: URCWebServiceManager {
    
    
    let userDefaults = UserDefaults.standard
    let loginServiceURL = Constants.getLoginUrl()
    //var assetGroupsArray: Array<AssetGroup> = []
    var loginKey: String = ""
    var loginName:String = ""
    var password:String = ""
    
    let headersPOST = ["Content-Type": "application/json"]
    
    var loginDictionary: NSDictionary = NSDictionary()
    var parametersDictionary = [String:String]()
    
    internal func loadLoginDetails(username:String, password:String, success : @escaping (_ response: NSDictionary?)->Void, failure :@escaping (_ error: NSError?, _ statusCode: Int) -> Void){
        
        parametersDictionary.updateValue(username, forKey: "login")
        parametersDictionary.updateValue(password, forKey: "password")
        
        loginService(url: loginServiceURL, parameters: parametersDictionary , headers:headersPOST, success: {
            (response) in
            
            
            self.loginDictionary = response
            
            self.userDefaults.set(self.loginDictionary.value(forKey: "identityAccessToken"), forKey: "COM_WAYGUM_URC_APP_IDENTITY_ACCESS_TOKEN")
            
            self.userDefaults.set(self.loginDictionary.value(forKey: "refreshToken"), forKey: "COM_WAYGUM_URC_APP_IDENTITY_REFRESH_TOKEN")
            
            
            
            let userServiceManager = UserServiceManager()
            URCManager.sharedInstance.user = userServiceManager.mapUserSubjectDetails(userObj: self.loginDictionary as! Dictionary<String, AnyObject>)
            
            
            //Save the user details alone in the keychain as we need it if we bypass login for already logged in user
            var userDetails : [String:AnyObject] = response as! [String : AnyObject]
            userDetails.removeValue(forKey: "identityAccessToken")
            userDetails.removeValue(forKey: "refreshToken")
            let userData : NSData = NSKeyedArchiver.archivedData(withRootObject: userDetails) as NSData
            
            self.userDefaults.set(userData, forKey: "COM_WAYGUM_URC_APP_USER_DATA")
            
            success (self.loginDictionary)
            
        }, failure: { (error,code) in
            failure(error,code)
        })
        
    }
    
}
