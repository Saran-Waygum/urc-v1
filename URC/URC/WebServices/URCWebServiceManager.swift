//
//  URCWebServiceManager.swift
//  URC
//
//  Created by Saran Mahadevan on 29/11/16.
//  Copyright © 2016 Waygum. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class URCWebServiceManager {
    
    let prefs = UserDefaults.standard
    
    func loadDataFromService (url: String, parameters: [String: String]!, headers: [String: String]!, success: @escaping (_ response: Array<NSDictionary>,_ resHeaders: NSDictionary?) ->Void, failure :@escaping (_ error: NSError?, _ code: Int) ->Void){
        
        print("api call is")
        print(url)
        
        var newHeaders = headers
        
        let token = prefs.string(forKey: "COM_WAYGUM_URC_APP_IDENTITY_ACCESS_TOKEN") == nil ? "" : prefs.string(forKey: "COM_WAYGUM_URC_APP_IDENTITY_ACCESS_TOKEN")!
        
        if(newHeaders != nil){
            newHeaders?["Authorization"] = "Bearer \(token)"
        }else{
            newHeaders = [
                "Authorization": "Bearer \(token)"
            ]
        }
        
        //let req :Alamofire.Request
        
        
        let req = Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: newHeaders)
        
        req.validate()
        req.responseJSON { response in
            var responseHeaders : NSDictionary = [:]
            //            if let JSON = response.result.value {
            //                print("JSON: \(JSON)")
            //            }
            if(response.response?.allHeaderFields != nil ){
                responseHeaders = (response.response?.allHeaderFields)! as NSDictionary
            }
            if(response.result.isSuccess){
                let responseValue = response.result.value
                //                if((responseValue?.isKindOfClass(NSDictionary)) == true){
                if responseValue is NSDictionary
                {
                    success(Array(arrayLiteral: responseValue as! NSDictionary),responseHeaders)
                }else{
                    success(response.result.value as! Array<NSDictionary>,responseHeaders)
                }
                
            }
            else{
                let errorCode : Int = response.response?.statusCode == nil ? 0 : (response.response?.statusCode)!
                failure(response.result.error as NSError?, errorCode)
            }
        }
    }
    
    func postDataToService (url: String, parameters: [String: AnyObject]!, headers: [String: String]!, success: @escaping (_ response: Array<NSDictionary>) ->Void, failure :@escaping (_ error: NSError?, _ code: Int, _ failureReason : String?) ->Void){
        
        let userDefaults = UserDefaults.standard
        var newHeaders = headers
        
        let token = userDefaults.string(forKey: "COM_WAYGUM_URC_APP_IDENTITY_ACCESS_TOKEN") == nil ? "" : userDefaults.string(forKey: "COM_WAYGUM_URC_APP_IDENTITY_ACCESS_TOKEN")!
        if(newHeaders != nil){
            
            newHeaders?["Authorization"] = "Bearer \(token)"
            newHeaders?["Content-Type"] = "application/json"
            
        }else{
            newHeaders = [
                "Authorization": "Bearer \(token)",
                "Content-Type": "application/json"
            ]
        }
        
        let req =  Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: newHeaders)
        //req.validate()
        req.responseJSON { response in
            
            //            if let JSON = response.result.value {
            //                print("JSON: \(JSON)")
            //            }
            
            if((response.response?.statusCode == Constants.StatusCodes.Ok.rawValue || response.response?.statusCode == Constants.StatusCodes.Created.rawValue || response.response?.statusCode == Constants.StatusCodes.Accepted.rawValue) && response.result.isSuccess){
                let responseValue = response.result.value
                if responseValue is NSDictionary{
                    
                    success(Array(arrayLiteral: responseValue as! NSDictionary))
                }else{
                    success(response.result.value as! Array<NSDictionary>)
                }
            }
            else{
                let errorCode : Int = response.response?.statusCode == nil ? 0 : (response.response?.statusCode)!
                var errorMessage : String = ""
                if let errorJson : NSDictionary = response.result.value as? NSDictionary{
                    if let eMessage : String = errorJson["message"] as? String{
                        errorMessage = eMessage
                    }
                }
                
                failure(response.result.error as NSError?, errorCode, errorMessage)
            }
        }
    }
    
    func putDataToService (url: String, parameters: [String: AnyObject]!, headers: [String: String]!, success: (_ response: Array<NSDictionary>) ->Void, failure :(_ error: NSError?, _ code: Int) ->Void){
        
        
        //        var newHeaders = headers
        //
        //        let token = KeychainWrapper.stringForKey("COM_WAYGUM_URC_APP_IDENTITY_ACCESS_TOKEN") == nil ? "" : KeychainWrapper.stringForKey("COM_WAYGUM_URC_APP_IDENTITY_ACCESS_TOKEN")!
        //
        //        if(newHeaders != nil){
        //            newHeaders.updateValue("Bearer \(token)", forKey: "Authorization")
        //            newHeaders.updateValue("application/json", forKey: "Content-Type")
        //        }else{
        //            newHeaders = [
        //                "Authorization": "Bearer \(token)",
        //                "Content-Type": "application/json"
        //            ]
        //        }
        //
        //        let req =  Alamofire.request(.PUT, url,parameters:parameters , headers: newHeaders, encoding: .JSON)
        //        req.validate()
        //        req.responseJSON { response in
        //
        //            //            if let JSON = response.result.value {
        //            //                print("JSON: \(JSON)")
        //            //            }
        //
        //            if(response.result.isSuccess){
        //                let responseValue = response.result.value
        //                if((responseValue?.isKindOfClass(NSDictionary)) == true){
        //                    success(response: Array(arrayLiteral: responseValue as! NSDictionary))
        //                }else{
        //                    success(response: response.result.value as! Array<NSDictionary>)
        //                }
        //            }
        //            else{
        //                let errorCode : Int = response.response?.statusCode == nil ? 0 : (response.response?.statusCode)!
        //                failure(error: response.result.error, code: errorCode)
        //            }
        //        }
    }
    
    func patchDataToService (url: String, parameters: AnyObject!, headers: [String: String]!, success: (_ response: Array<NSDictionary>) ->Void, failure :(_ error: NSError?, _ code: Int) ->Void){
        
        
        //        var newHeaders = headers
        //        let token = KeychainWrapper.stringForKey("COM_WAYGUM_URC_APP_IDENTITY_ACCESS_TOKEN") == nil ? "" : KeychainWrapper.stringForKey("COM_WAYGUM_URC_APP_IDENTITY_ACCESS_TOKEN")!
        //
        //        if(newHeaders != nil){
        //            newHeaders.updateValue("Bearer \(token)", forKey: "Authorization")
        //            newHeaders.updateValue("application/json", forKey: "Content-Type")
        //        }else{
        //            newHeaders = [
        //                "Authorization": "Bearer \(token)",
        //                "Content-Type": "application/json"
        //            ]
        //        }
        //
        //        var req: Alamofire.Request!
        //
        //        //The parameter passed for patch can be an array or dictionary hence checking for the type and if it is an array we need to define the request and send it to alamofire as alamofire does not handle array object as parameters.
        //
        //        if parameters is NSArray{
        //            let param = parameters as! Array<Dictionary<String,AnyObject>>
        //            let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        //            request.HTTPMethod = "PATCH"
        //            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        //            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //            request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(param, options: [])
        //            req =  Alamofire.request(request)
        //
        //        }else{
        //            let param : Dictionary<String,AnyObject> = parameters as! Dictionary<String,AnyObject>
        //            req =  Alamofire.request(.PATCH, url,parameters:param , headers: newHeaders, encoding: .JSON)
        //
        //        }
        //
        //        req.validate()
        //        req.responseJSON { response in
        //
        //            //            if let JSON = response.result.value {
        //            //                print("JSON: \(JSON)")
        //            //            }
        //
        //            if(response.result.isSuccess){
        //                let responseValue = response.result.value
        //                if((responseValue?.isKindOfClass(NSDictionary)) == true){
        //                    success(response: Array(arrayLiteral: responseValue as! NSDictionary))
        //                }else{
        //                    success(response: response.result.value as! Array<NSDictionary>)
        //                }
        //            }
        //            else{
        //                let errorCode : Int = response.response?.statusCode == nil ? 0 : (response.response?.statusCode)!
        //                failure(error: response.result.error, code: errorCode)
        //            }
        //        }
        
    }
    
    
    func loginService(url: String, parameters: [String: String]!, headers: [String: String]!, success: @escaping (_ response: NSDictionary) ->Void, failure :@escaping (_ error: NSError?, _ code: Int) ->Void){
        
        let req =  Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        
        
        
        req.validate()
        req.responseJSON { response in
            
            //            if let JSON = response.result.value {
            //                print("JSON: \(JSON)")
            //            }
            
            if(response.result.isSuccess){
                success(response.result.value as! NSDictionary)
            }
            else {
                let errorCode : Int = response.response?.statusCode == nil ? 0 : (response.response?.statusCode)!
                failure(response.result.error as NSError?, errorCode)
            }
        }
    }
    
    
    func logoutFromService(url: String, parameters: [String: String]!, headers: [String: String]!, success: @escaping (_ response: String) ->Void, failure :@escaping (_ error: NSError?, _ code: Int) ->Void){
        
        
        let req =  Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
                
                req.responseJSON { response in
                    
                    //            if let JSON = response.result.value {
                    //                print("JSON: \(JSON)")
                    //            }
                    
                    
                    if(response.response?.statusCode == Constants.StatusCodes.Ok.rawValue){
                        success("success")
                    }
                    else{
                        let errorCode : Int = response.response?.statusCode == nil ? 0 : (response.response?.statusCode)!
                        failure(response.result.error as NSError?, errorCode)
                    }
                }
    }
    
}
