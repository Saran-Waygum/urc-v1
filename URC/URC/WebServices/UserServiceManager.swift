//
//  UserServiceManager.swift
//  URC
//
//  Created by Roopa Raman on 10/03/16.
//  Copyright © 2016 waygum. All rights reserved.
//

import UIKit

class UserServiceManager: URCWebServiceManager {
    
    var createEditUserUrl : String = Constants.getCreateEditUserURL()
    
    
    
    internal func createUser(parameters:User, success : @escaping (_ response : User?) ->Void, failure: @escaping (_ error: NSError?, _ statusCode: Int,_ failureReason: String) -> Void ){
        let param : Dictionary<String,AnyObject> = constructReqParameterForSaveUser(user: parameters,isCreate: true)
        postDataToService(url: createEditUserUrl, parameters: param, headers: nil, success: { (response) -> Void in
            let responseUser : User! = self.mapUserSubjectDetails(userObj: response.first as! Dictionary<String,AnyObject>)
            success(responseUser)
        }, failure: {
            (error,code,failureReason) in
            failure(error, code, failureReason!)
        })
    }
    
    internal func editUser(parameters:User, success : @escaping (_ response : User?) ->Void, failure: @escaping (_ error: NSError?, _ statusCode: Int) -> Void ){
        let param : Dictionary<String,AnyObject> = constructReqParameterForSaveUser(user: parameters,isCreate: false)
        putDataToService(url: createEditUserUrl, parameters: param, headers: nil, success: { (response) -> Void in
            let responseUser : User! = self.mapUserSubjectDetails(userObj: response.first as! Dictionary<String,AnyObject>)
            success(responseUser)
        }, failure: {
            (error,code) in
            failure(error, code)
        })
    }
    
    
    func constructReqParameterForSaveUser(user: User, isCreate: Bool) -> Dictionary<String,AnyObject>{
        //        var userInfo : [String : AnyObject] = [:]
        var param : [String : AnyObject] = [:]
        
        param = [
            "identitySubjectLoginId":user.loginId as AnyObject,
            "userSubjectFirstName": user.firstName as AnyObject,
            "userSubjectLastName" : user.lastName as AnyObject,
            "userSubjectWorkEmail" : user.emailId as AnyObject,
            "userSubjectMobilePhone" : user.phone as AnyObject,
            "identitySubjectExtraInfo" : "" as AnyObject,
            //            "orgId" : user.identityAccess.orgId,
        ]
        // We should not send the password field for edit user
        if !user.password.isEmpty {
            param["password"] = user.password as AnyObject?
        }
        
        let extraInfo : Dictionary<String,AnyObject>  = [
            "SMSCarrierID" : "\(user.extraInfoObj.smsCarrierId)" as AnyObject,
        ]
        
        var jsonData:NSData = NSData()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: extraInfo, options:[]) as NSData
        } catch  {
            print(error)
        }
        let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
        
        param["identitySubjectExtraInfo"] = jsonString as AnyObject?
        
        //for creating a user, id will not exist so it should not be sent
        //        if(!isCreate){
        //            param["id"] = user.subjectId
        //        }
        
        return param
    }
    
    func mapUserSubjectDetails(userObj:Dictionary<String,AnyObject>) -> User{
        var firstName : String = ""
        var lastName : String = ""
        var mobilePhone : String = ""
        var email : String = ""
        var loginId : String = ""
        var uuid : String = ""
        var id : Int = 0
        var orgId : Int = 0
        var roles : Array<String> = []
        var extraInfo : Dictionary<String,AnyObject> = [:]
        var smsCarrierId : Int = 0
        var profileImageUrl : String = ""
        
        //        var isSuperAdmin : Bool = false
        //        var isUserAdmin : Bool = false
        if let obj = userObj["identitySubjectDTO"] {
            firstName =  (obj["userSubjectFirstName"] as? String) == nil ? "" : obj["userSubjectFirstName"] as! String
            lastName =  (obj["userSubjectLastName"] as? String) == nil ? "" : obj["userSubjectLastName"] as! String
            mobilePhone =  (obj["userSubjectMobilePhone"] as? String) == nil ? "" : obj["userSubjectMobilePhone"] as! String
            email =  (obj["userSubjectWorkEmail"] as? String) == nil ? "" : obj["userSubjectWorkEmail"] as! String
            loginId =  (obj["identitySubjectLoginId"] as? String) == nil ? "" : obj["identitySubjectLoginId"] as! String
            uuid =  (obj["identitySubjectUUId"] as? String) == nil ? "" : obj["identitySubjectUUId"] as! String
            id =  (obj["id"] as? Int)  == nil ? 0 : obj["id"] as! Int
            profileImageUrl = (obj["userSubjectImageUrl"] as? String) == nil ? "" : obj["userSubjectImageUrl"] as! String
            
            if let info = obj["identitySubjectExtraInfo"] as? String{
                if (!info.isEmpty ){
                    let data = info.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                    
                    do {
                        extraInfo = try (JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject])
                    } catch {
                        print("Failed to load: \(error)")
                    }
                    
                    if let smsCId = extraInfo["SMSCarrierID"] as? String{
                        smsCarrierId = Int(smsCId) != nil ? Int(smsCId)! : 0
                    }
                    
                }
            }
            
        }
        
        if let accessibleOrgs = userObj["identitySubjectGroupOrgIds"] {
            let accOrgId = (accessibleOrgs.firstObject as? Int == nil) ? 0 : accessibleOrgs.firstObject as! Int
            if(accOrgId != 0){
                if let accessObjArray = userObj["identityAccessRoles"] as? Array<Dictionary<String,AnyObject>>{
                    
                    for orgAccessObj in accessObjArray {
                        let currentorgId = (orgAccessObj["orgId"] as? Int == nil) ? 0: orgAccessObj["orgId"] as! Int
                        
                        if(currentorgId == accOrgId){
                            orgId = currentorgId
                            if let orgAccessRoles = orgAccessObj["orgAccessRoles"] as? [String] {
                                roles = orgAccessRoles
                            }
                        }
                    }
                }
            }
        }
        
        let userDefaults = UserDefaults.standard
        
        userDefaults.set(firstName, forKey: "firstName")
        
        userDefaults.set(lastName, forKey: "lastName")
        
        userDefaults.set(email, forKey: "email")
        
        userDefaults.set(profileImageUrl, forKey: "profileImageUrl")
        
        
        
        let user = User(firstName: firstName, lastName: lastName, emailId: email, phone: mobilePhone, subjectId: id, subjectUUID: uuid, identityAccess: UserIdentityAccess(orgId: orgId, roles: roles), loginId: loginId, password: "", extraInfoObj: UserExtraInfo(smsCarrierId: smsCarrierId))
        user.profileImageUrl = profileImageUrl
        return user
        
    }
    
}
